/*!****************************************************************************
 * @file
 * @author       Alexander Hogen
 *
 * @brief These functions generate SysmteVerilog compatible
 * ripple-carry adders.
 *
 *****************************************************************************/

#ifndef SV_ADDER_RIPPLE_CARRY_H
#define SV_ADDER_RIPPLE_CARRY_H

#include <string>
using std::string;
using std::to_string;

#include "sv_adder1b.h"

/*!****************************************************************************
 * @brief Generates SystemVerilog code to instantiate
 *        a ripple-carry adder.
 *
 * Note: You must call the svAdderRippleDef()
 * function to first create a SystemVerilog definition
 * for the ripple-carry adder. This definition must
 * be placed before the instantiation in the '.sv'
 * file.
 *
 * @param inst_name is the desired unique instance
 *        name/identifier
 *
 * @param in_a_name is the name of the first input
 *        of the adder. This input must be at
 *        least "bit_width" bits wide.
 *
 * @param inb_name is the name of the second input
 *        of the adder. This input must be at
 *        least "bit_width" bits wide.
 *
 * @param out_sum_name is the name of the output
 *        wire. It will be "bit_width" + 1 bits
 *        wide.
 *
 * @param bit_width is the number of bits to be
 *        added to each other. Because of this, it
 *        defines the number of 1-bit adders to
 *        be instantiated internaly and also
 *        defines how wide the output will be.
 *
 * @returns A string containing the instantiation
 *         SystemVerilog code. This string can
 *         be written directly to a '.sv' file.
 *
 *****************************************************************************/
string svAdderRippleInst(const string &inst_name,
                         const string &in_a_name,
                         const string &in_b_name,
                         const string &out_sum_name,
                         unsigned int bit_width);

/*!****************************************************************************
 * @brief Generates SystemVerilog code to define
 *        a ripple-carry adder.
 *
 * Note: You must call the svAdderRippleDef()
 * function to first create a SystemVerilog definition
 * for the ripple-carry adder. This definition must
 * be placed before the instantiation in the '.sv'
 * file.
 *
 * @param bit_width is the number of bits to be
 *        added to each other. Because of this, it
 *        defines the number of 1-bit adders to
 *        be instantiated internaly and also
 *        defines how wide the output will be.
 *
 * @returns A string containing the definition
 *         SystemVerilog code. This string can
 *         be written directly to a '.sv' file.
 *
 *****************************************************************************/
string svAdderRippleDef(unsigned int bit_width);

#endif // SV_ADDER_RIPPLE_CARRY_H
