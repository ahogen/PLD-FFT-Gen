/*!****************************************************************************
 * Project Name: tp-logger
 * @file
 * @author       Alexander Hogen
 * @date         2017-02-22
 *
 * @brief Tests the functionality of the Logger class.
 *
 * Tests both file and terminal logging capabilities with the
 * logging thresholds set to various levels.
 *****************************************************************************/
#include "../lib/logger.h"

#include <iostream>
using std::cout;
using std::endl;

void LogWithDefaults();
void LogWithTerminalOnly();
void LogToBothCustom1();
void LogWithCmdArgs(int &argc, char *argv[]);

int main(int argc, char *argv[])
{
  int return_status = 0;

  try
  {
    LogWithDefaults();
    LogWithTerminalOnly();
    LogToBothCustom1();
    LogWithCmdArgs(argc, argv);
  }
  catch (const std::runtime_error &e)
  {
    cout << "STD-RUNTIME-ERR-CAUGHT: " << e.what() << endl;
    return_status = -1;
  }

  return (return_status);
}

// Test Logger with default constructor
void LogWithDefaults()
{
  // Starting Doxygen snippet
  //! [Use defaults]
  Logger myLog;

  myLog.Debug(to_string(1) + " message");    // File: N Term: N
  myLog.Info(to_string(2) + " message");     // File: N Term: Y
  myLog.Warning(to_string(3) + " message");  // File: Y Term: Y
  myLog.Error(to_string(4) + " message");    // File: Y Term: Y
  myLog.Critical(to_string(5) + " message"); // File: Y Term: Y

  //! [Use defaults]
}

// Test Logger with only terminal logging enabled
void LogWithTerminalOnly()
{
  // Starting Doxygen snippet
  //! [Custom terminal only]
  Logger myLog(true, LOG_DEBUG, false);

  myLog.Debug(to_string(1) + " message");    // File: N Term: Y
  myLog.Info(to_string(2) + " message");     // File: N Term: Y
  myLog.Warning(to_string(3) + " message");  // File: N Term: Y
  myLog.Error(to_string(4) + " message");    // File: N Term: Y
  myLog.Critical(to_string(5) + " message"); // File: N Term: Y
                                             //! [Custom terminal only]
}

// Test Logger with both file and terminal outputs enabled.
// Custom log thresholds are defined for each.
void LogToBothCustom1()
{
  // Starting Doxygen snippet
  //![Custom terminal and file]
  Logger myLog(true, LOG_WARNING, true, LOG_DEBUG, "./logs/verbose.log");

  myLog.Debug(to_string(1) + " message");    // File: Y Term: N
  myLog.Info(to_string(2) + " message");     // File: Y Term: N
  myLog.Warning(to_string(3) + " message");  // File: Y Term: Y
  myLog.Error(to_string(4) + " message");    // File: Y Term: Y
  myLog.Critical(to_string(5) + " message"); // File: Y Term: Y
                                             //![Custom terminal and file]
}

// Test Logger being configured with any command line
// arguments passed in.
void LogWithCmdArgs(int &argc, char *argv[])
{
  //! [Cmd arg config]
  // Like this, using command arg parsing constructor...
  Logger myLog(argc, argv);

  // ... or like this, after instantiation using
  // the Logger::CmdArgConfig() method
  myLog.CmdArgConfig(argc, argv);

  myLog.Debug(to_string(1) + " message");
  myLog.Info(to_string(2) + " message");
  myLog.Warning(to_string(3) + " message");
  myLog.Error(to_string(4) + " message");
  myLog.Critical(to_string(5) + " message");
  //! [Cmd arg config]
}
