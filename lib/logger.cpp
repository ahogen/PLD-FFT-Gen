/*!****************************************************************************
 * @file
 * @author Alexander Hogen
 * @date   2017-02-22
 *
 * @brief Contains the logger class method
 *        implementations.
 *
 * Method descriptions are also provided here.
 *
 * <b>NOTE:</b> This file should be in the same
 * directory as the logger.h file.
 *****************************************************************************/

#include "logger.h"

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include <fstream>
using std::ios;
using std::ofstream;

// To insert a timestamp in log messages
#include <time.h>

#include <stdexcept>
using std::runtime_error;

/*!****************************************************************************
* @brief Constructor to initialize Logger and
*        specify terminal and file logging
*        behavior.
*
* Initializes all data members to their empty
* or default values. Argument values passed in
* will be copied to their respective private
* data member.
*
* All parameters are optional, so this constructor
* may also be used as the default constructor.
*
* @param term_enable is a bool and specifies
*        if displaying messages to the terminal
*        is enabled (true) or disabled (false)
*
* @param term_threshold is of type TLogLevel and
*        specifies the lowest log priority to
*        print to the terminal
*
* @param file_enable is a bool and specifies if
*        writing messages to a log file is
*        enabled (true) or disabled (false)
*
* @param file_threshold is of type TLogLevel and
*        specifies the lowest log priority to
*        write to the log file
*
* @param file_path_out is a char*. This is the
*        full path and name of the log file
*        which will be written to.
*        See the description of the
*        Logger::SetFilePath() method for other
*        usage instructions.
*
******************************************************************************/
Logger::Logger(
    bool term_enable,
    TLogLevel term_threshold,
    bool file_enable,
    TLogLevel file_threshold,
    const char *file_path_out) :

                                 term_en(term_enable),
                                 term_lvl(term_threshold),
                                 file_en(file_enable),
                                 file_lvl(file_threshold),
                                 file_path("./default.log")
{
  SetFilePath(file_path_out);
}

/*!****************************************************************************
* @brief Constructor to initialize Logger with
*        command line arguments.
*
* Initializes all data members to their empty
* or default values. Argument values are parsed
* and acted on by Logger::CmdArgConfig().
*
* See https://www.cs.ucsb.edu/~pconrad/cs16/topics/cmdlineargs/
* for a quick refresher on command line
* arguments if necessary.
*
* See Logger::CmdArgConfig() for parsing information.
*
* @param argc stands for "argument count" and
*        is an integer
*
* @param argv stands for "argument vector" and
*        is a one-dimentional array of strings.
*        It is a char* type.
*
* @throw std::runtime_error if Logger::CmdArgConfig()
*        returns 'false.'
*
******************************************************************************/
Logger::Logger(const int &argc, char *argv[]) : term_en(true),
                                                term_lvl(LOG_WARNING),
                                                file_en(false),
                                                file_lvl(LOG_ERROR),
                                                file_path("./default.log")
{
  if (!CmdArgConfig(argc, argv))
  {
    throw runtime_error("Bad Logger command-line argument");
  }
}

/*!****************************************************************************
* @brief Destructor which simply marks the end
*        of the logging session in all log
*        destinations.
*
* If the program using a Logger object is
* executed multiple times, it may be very
* helpful to mark the end of an execution so
* that the next log (if there is one) is clearly
* separate. That is why when this object is
* destroyed, we will display and print a log
* message to mark the end of the logging session.
*
******************************************************************************/
Logger::~Logger()
{

  string msg("-----------------  LOG BREAK  -----------------\n\n");

  if (term_en)
  {
    Log2Terminal(LOG_INFO, LogTypeStr(LOG_INFO), msg);
  }

  if (file_en)
  {
    Log2File(LogTypeStr(LOG_INFO), msg);
  }
}

/*!****************************************************************************
 * @brief Filters log messages and sends them to
 *        log destinations only if their level is
 *        low enough.
 *
 * This method decides whether or not a log
 * message should be printed in a log destination
 * such as the terminal or a log file depending
 * on the thresholds previously set for each
 * log destination type either with the constructor
 * when the object was created or with
 * @ref Logger::SetTerminalThreshold() or
 * @ref Logger::SetFileThreshold()
 *
 * If a log message should be displayed or
 * written, this method will call the function
 * which will format and display the log entry.
 *
 * @param level is a TLogLevel enumerated type,
 *        defining the log message severity
 *        level
 *
 * @param msg is a constant string which contains
 *        the text message to be included in the
 *        log entry
 *
 *****************************************************************************/
void Logger::Log(const TLogLevel &level, const string &msg) const
{
  // Get the severity as a string of this log message
  const char *level_string(LogTypeStr(level));

  // If the msg level is high enough to be displayed in the terminal...
  if (term_en && (level >= term_lvl))
  {
    // Print it to the terminal!
    Log2Terminal(level, level_string, msg);
  }

  // If the msg level is high enough to be written to the log file...
  if (file_en && (level >= file_lvl))
  {
    // Write it to the log file!
    Log2File(level_string, msg);
  }
}

/*!****************************************************************************
* @brief Writes the log message msg to the target
*        log file.
*
* @param lvl_str is a char* representation of the
*        TLogLevel level. See logger::log_type_str.
*
* @param msg is a constant string which contains
*        the text message to be included in the
*        log entry.
*
* @throw std::runtime_error if target file or
*        directory cannot be accessed
******************************************************************************/
void Logger::Log2File(const char *lvl_str, const string &msg) const
{
  // To store local time
  time_t rawtime;
  struct tm *timeinfo;
  char time_str[30];

  // Get the local time to timestamp the log
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  // Format the time into a nice string
  strftime(time_str, 30, "%Y-%m-%d %H:%M:%S", timeinfo);

  // Attempt to open for writing in append mode
  ofstream f(file_path.c_str(), ios::out | ios::app);

  if (f.is_open())
  {
    // Write time stamp
    f << time_str << " - ";
    f.flush();

    // Write log type
    f << lvl_str << ": ";
    f.flush();

    // Write log message
    f << msg << "\n";
    f.flush();

    // Flush and close the log file
    f.close();
  }
  else
  {
    // Throw a runtime exception
    // We should *not* continue running if we can't log messages.
    throw runtime_error("logger::Log2File Could not access target log file location \"" + file_path + "\"");
  }
}

/*!****************************************************************************
* @brief Writes the log message msg to the
*        console / terminal window.
*
* @param lvl_str is a
*
* @param msg is a constant string which contains
*        the text message to be included in the
*        log entry.
*
******************************************************************************/
void Logger::Log2Terminal(const TLogLevel log_severity,
                          const char *lvl_str,
                          const string &msg) const
{
  time_t rawtime;
  struct tm *timeinfo;
  char time_str[30];

  // Get the local time to timestamp the log
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(time_str, 30, "%Y-%m-%d %H:%M:%S", timeinfo);

  // If a log_t::LOG_WARNING or less severe, print to normal cout
  if (log_severity <= LOG_WARNING)
  {
    // Write log message
    cout << time_str << " - ";
    cout << lvl_str << ": ";
    cout << msg << endl;
  }
  // If log_severity is more severe than LOG_WARNING
  // print to the error stream
  else
  {
    // Write log message
    cerr << time_str << " - ";
    cerr << lvl_str << ": ";
    cerr << msg << endl;
  }
}

/*!****************************************************************************
* @brief Holds all of the constant character
*        strings corresponding to the log level
*        types.
*
* To retrieve the character string of a given
* TLogLevel log level, pass in the log level and
* a pointer will be returned which contains
* the corresponding character string.
*
* @param log_type must be a TLogLevel log severity
*        level
*
* @returns character constant pointer (char const*)
*         which is a null-terminated string
*
******************************************************************************/
char const *Logger::LogTypeStr(TLogLevel log_level) const
{
  char const *cstr(nullptr);

  switch (log_level)
  {
  case LOG_CRITICAL:
    cstr = "[CRITICAL]";
    break;

  case LOG_ERROR:
    cstr = "[ERROR]   ";
    break;

  case LOG_WARNING:
    cstr = "[WARNING] ";
    break;

  case LOG_INFO:
    cstr = "[INFO]    ";
    break;

  case LOG_DEBUG:
    cstr = "[DEBUG]   ";
    break;

  default:
    cstr = "[ ??? ]   ";
  }

  return (cstr);
}

/*!****************************************************************************
* @brief  Overwrites the old log level threshold
*         for terminal logging with the new
*         TLogLevel passed in.
*
* @param  new_lvl is a TLogLevel type and is the
*         new log level threshold
*
* @returns true if setting the new TLogLevel was
*         successful
******************************************************************************/
bool Logger::SetTerminalThreshold(TLogLevel new_lvl)
{
  bool success(false);

  if ((new_lvl < LOG_LVL_COUNT) && (new_lvl >= 0))
  {
    term_lvl = new_lvl;
    success = true;
  }
  else
  {
    Error("Logger::SetTerminalThreshold() received invalid log level type");
  }

  return (success);
}

/*!****************************************************************************
* @brief  Overwrites the old log level threshold
*         for file logging with the new
*         TLogLevel passed in.
*
* @param  new_lvl is a TLogLevel type and is the
*         new log level threshold
*
* @returns true if setting the new TLogLevel was
*         successful
******************************************************************************/
bool Logger::SetFileThreshold(TLogLevel new_lvl)
{
  bool success(false);

  if (new_lvl < LOG_LVL_COUNT && new_lvl >= 0)
  {
    file_lvl = new_lvl;
    success = true;
  }
  else
  {
    Error("Logger::SetFileThreshold() received invalid log level type");
  }

  return (success);
}

/*!****************************************************************************
* @brief Set new log file output destination
*
* Tests if the file path is valid and then sets
* the internal filepath variable to the string
* which was passed in if the test was successful.
*
* Since logging can be a very important
* component of an application's operation, we
* must first verify that logs can even be made
* in the first place. If they can't, we have to
* warn and possibly throw an exception as soon as
* possible, which is what we do in this method.
*
* This method changes the target output directory
* to the specified path and then checks to see if
* a file can be created and opened there. If a
* file cannot be created/opened, then a LOG_CRITICAL
* level message will be printed to the terminal
* (if logger::term_en == true) and then an
* runtime exception will be thrown.
*
* If a blank file exists at the location pointed
* to, it will be removed at the end of this function.
* Most of the time, the blank file was created
* while opening it in write mode while testing
* to see if it has the proper access permissions.
* It's normal behavior for ofstream to create
* a file if it doesn't exist already. Because
* the use code may update the desired log file
* after, say, the constructor called this function,
* there is a possibility that a blank file will
* be left behind after execution. To avoid that
* this function checks if the file is blank or
* not. If it is blank/empty, it is deleted.
*
* <b>Usage example:</b>
*
* To change the output log directory
* to a LOG subdirectory of the current
* directory, pass "./LOG/my_log_file.log" to
* this method.
*
* @param  target_file is a std::string passed
*         by reference
*
* @returns 'true' if the new log file address is
*         valid and accessible
*
* @throw  std::runtime_error if target file or
*         directory cannot be accessed
*
******************************************************************************/
bool Logger::SetFilePath(const string &target_file)
{
  bool success(false);

  // Attempt to open for writing in append mode
  ofstream f(target_file.c_str(), ios::out | ios::app);

  // If the file opened successfully without errors
  if (f.is_open() && f.good())
  {
    char tmp_char = 0;

    // Close the file in output/write mode
    f.close();

    // Open file in input/read mode
    std::ifstream f(target_file.c_str(), ios::in);

    // Dummy read
    f.get(tmp_char);

    // Get position
    tmp_char = f.tellg();

    f.close();

    // If the file was empty, delete it
    if (tmp_char == -1)
    {
      std::remove(target_file.c_str());
    }

    // Change the log file destination to the
    // file path that was just tested
    file_path = target_file;

    // Change the return status
    success = true;
  }
  else
  {
    // If the file could not be accessed, don't
    // allow any subsequent logs to attempt to
    // write to it (such as the Critical() log
    // we're going to fire next)
    file_en = false;

    // Even though file logs won't work, we can
    // still attempt to display a message via the
    // terminal window (if it is enabled).
    Critical("Logger::SetOutputDir could not open \"" + target_file + "\"");
    Critical("File logging disabled to prevent repeating this error");

    // Throw a runtime exception
    // We should *not* continue running if we can't log messages.
    throw runtime_error("Can\'t open " + target_file);
  }

  return (success);
}

/*!****************************************************************************
* @brief Interprets and acts on command line
*        arguments.
*
* Finds any valid command line options in argv
* and performs the corresponding actions of that
* option to the Logger object. None of the
* command line arguments are removed from argv
* after they are interpreted. This is useful if
* say, the '-h' (help) option is used by other
* external code to print out help options or
* messages to a terminal window, both this method
* and the external code can interpret and act
* on the '-h' option.
*
* NOTE: If an option (not an argument) is
* incorrect (misspelled or otherwise invalid) it
* will simply be ignored. There will be no error
* indicated. For all this function knows, it is
* a parameter meant for some other code, so no
* use complaining about it. Only errors for
* invalid arguments (i.e. expected 'yes' or
* 'no' but got something else) are indicated.
*
* See https://www.cs.ucsb.edu/~pconrad/cs16/topics/cmdlineargs/
* for a quick refresher on command line
* arguments if necessary.
*
* <ul>
* <li><b>Option:</b> A parameter preceded with single
* or double dash ('-' or '--') such as '-h' or
* '--help' </li>
*
* <li><b>Argument:</b> A secondary parameter, typically
* a string, provided or assigned to a given
* option. It is not preceded by any characters.
* May be surrounded with double quotes, especially
* if there are spaces in the string. </li>
* </ul>
*
* When changing the log file destination, use the
* '-fp' <b>option</b> followed by a file path
* <b>argument</b>.
*
* \verbatim $ [my-executable] -fp "./logs/my-log.log" \endverbatim
*
* Current options supported, as printed by the
* '-h' or '--help' option:
*
* @snippet lib/logger.cpp Logger cmd line help
*
* @param argc stands for "argument count" and
*        is an integer
*
* @param argv stands for "argument vector" and
*        is a one-dimensional array of strings.
*        It is a char* type.
*
* @returns 'true' if there were no issues decoding
*         arguments (not options)
*
******************************************************************************/
// \todo Document this method
// \todo Test this parsing on a Linux machine
bool Logger::CmdArgConfig(const int &argc, char *argv[])
{
  // This value is the returned status
  bool no_arg_error = true;

  int index = 1;      // Current place in the "argv" array
  unsigned int a = 0; // Temporary number
  unsigned int b = 0; // Temporary number
  string str = "";    // Temporary string

  // Extract executable name from argv[0] path
  str = argv[0];             // Copy char* into string
  a = str.find_last_of('/'); // Get position of last slash in filepath

  if (a == std::string::npos)
  { // If forward slashes aren't in path,
    // look for Windows-like backslashes instead
    a = str.find_last_of("\\");
  }

  str = str.substr(a + 1); // Extract executable name (with extension)
  // NOTE: It may be tempting to save the extraction
  // after the extension has been found. However
  // if the executable is on a Linux/UNIX machine,
  // does not have an extension, and is in a hidden
  // directory, then 'a' will be greater than 'b' which
  // is bad. Instead, we want to strip off all of the
  // directory names first, including the hidden dir
  // so that the output is as you would expect.

  b = str.find_last_of('.'); // Find position of extension suffix
  str = str.substr(0, b);    // Extract executable name without extension

  SetFilePath("./" + str + ".log"); // Verify we can write to this file

  // Parse, decode, and assign remaining config arguments
  while (index < argc)
  {
    str = argv[index];

    //----------------------
    // No argument options
    //----------------------
    // VERBOSE option
    if ((str == "-v") || (str == "--verbose"))
    {
      term_en = true;
      term_lvl = LOG_INFO;
      file_en = true;
      file_lvl = LOG_INFO;
    }

    // WARN option
    else if ((str == "-w") || (str == "--warn"))
    {
      term_en = true;
      term_lvl = LOG_WARNING;
      file_en = true;
      file_lvl = LOG_WARNING;
    }

    // SILENT option
    else if ((str == "--silent") || (str == "--no-log"))
    {
      term_en = false;
      file_en = false;
    }

    // HELP
    else if ((str == "-h") || (str == "--help"))
    {
      cout.flush();
      //! [Logger cmd line help]
      cout << "Logger.h -- Alexander Hogen <code.ahogen@outlook.com>" << endl;
      cout << "Logging options: " << endl;
      cout << "  -v  --verbose           term=on, tt=info, f=on, ft=info" << endl;
      cout << "  -w  --warn              term=on, tt=warn, f=on, ft=warn " << endl;
      cout << "      --silent            term=off,         f=off" << endl;
      cout << "  -tl --terminal-logging  [on / off]                        default = on" << endl;
      cout << "  -tt --term-threshold    [info / warn / error / critical]  default = warn" << endl;
      cout << "  -fl --file-logging      [on / off]                        default = off" << endl;
      cout << "  -ft --file-threshold    [info / warn / error / critical]  default = error" << endl;
      cout << "  -fp --file-path         e.g. \"./logs/messages.log\"      default = ./<executable-name>.log" << endl;
      cout << "  -h  --help              Prints this message" << endl;
      //! [Logger cmd line help]
      cout << endl;
      cout << "  EXAMPLE: <program-name> -fl on -ft error " << endl;
      cout << endl;
      cout << "  See Logger.h documentation for more details." << endl;
      cout << endl;
    }

    //-----------------------
    // Options with arguments
    //-----------------------

    // Make sure there is a second argument before
    // attempting to parse
    if (++index < argc)
    {

      // Terminal logging switch
      if ((str == "-tl") || (str == "--terminal-logging"))
      {

        str = argv[index];

        if (str == "on")
        {
          term_en = true;
        }
        else if (str == "off")
        {
          term_en = false;
        }
        else
        {
          no_arg_error = false;
          index--;
        }
      }
      // Terminal threshold setting
      else if ((str == "-tt") || (str == "--term-threshold"))
      {

        str = argv[index];

        if (str == "info")
        {
          term_lvl = LOG_INFO;
        }
        else if (str == "warn")
        {
          term_lvl = LOG_WARNING;
        }
        else if (str == "error")
        {
          term_lvl = LOG_ERROR;
        }
        else if (str == "critical")
        {
          term_lvl = LOG_CRITICAL;
        }
        else
        {
          no_arg_error = false;
          index--;
        }
      }
      // File logging switch
      else if ((str == "-fl") || (str == "--file-logging"))
      {

        str = argv[index];

        if (str == "on")
        {
          file_en = true;
        }
        else if (str == "off")
        {
          file_en = false;
        }
        else
        {
          no_arg_error = false;
          index--;
        }
      }
      // Log file threshold setting
      else if ((str == "-ft") || (str == "--file-threshold"))
      {

        str = argv[index];

        if (str == "info")
        {
          file_lvl = LOG_INFO;
        }
        else if (str == "warn")
        {
          file_lvl = LOG_WARNING;
        }
        else if (str == "error")
        {
          file_lvl = LOG_ERROR;
        }
        else if (str == "critical")
        {
          file_lvl = LOG_CRITICAL;
        }
        else
        {
          no_arg_error = false;
          index--;
        }
      }
      // Log file path
      else if ((str == "-fp") || (str == "--file-path"))
      {
        no_arg_error &= SetFilePath(argv[index]);
      }
    }

    // Increment to the next option in the list
    index++;
  }

  return (no_arg_error);
}
