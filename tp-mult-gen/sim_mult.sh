#!/bin/bash
###############################################################################
# Author: Alex Hogen (code.ahogen@outlook.com)
#
# Compile and simulate a Verilog testbench with Icarus Verilog.
#
# getopts parsing example from https://stackoverflow.com/a/14203146/3638827
###############################################################################

# Verilog simulation source files and configuration
BUILD_DIR='./icarus_build/'
SOURCES='
./mult.v
./mult_tb.v'
SIM_NAME=simulation_tb
DUMP_FILE="./sim_dump.sim"
COMPILER_ARGS=' '


OPTIND=1 # If getops used previously, reset it for this run

# Initialize option switches
cmdopt_gtkwave=0
cmdopt_simple_sim_print=0

function show_help() {
    help_str="""
Compile and simulate a Verilog testbench with Icarus Verilog.

Usage: sim_script.sh [-g] [-s] [-h]

-g   After compiling and simulating, launch waveform viewer (gtkwave)
-s   Simple sim print. Turns off ANSI codes (colors and formatting)
     in the Verilog testbench. Useful when saving output to text files.
-h   Show this message
"""

    echo "$help_str"
}

# Parse cmdline args
while getopts "hsg" opt; do
    case "$opt" in
    h)
        show_help;
        exit 0;
        ;;
    s)  cmdopt_simple_sim_print=1
        ;;
    g)  cmdopt_gtkwave=1
        ;;
    esac
done
shift $((OPTIND-1))
[ "$1" = "--" ] && shift

# Make build directory
if [ ! -d ${BUILD_DIR} ]; then
    mkdir ${BUILD_DIR}
fi
# Remove old sim files
rm -f ${DUMP_FILE}
rm -f ${BUILD_DIR}/${SIM_NAME}

# Enable ANSI escape codes in testbench
if [ "$cmdopt_simple_sim_print" -eq "0" ]; then
    COMPILER_ARGS=" -DUSE_ANSI_PRINT_CODES $COMPILER_ARGS"
fi

echo "Compiling (iverilog) " ${SOURCES}
iverilog -g2001 -Wall $COMPILER_ARGS -o ${BUILD_DIR}/${SIM_NAME} ${SOURCES}
EC_COMPILER=$?

if [ ${EC_COMPILER} -ne 0 ]; then
    exit ${EC_COMPILER}
fi

echo "Running simulation (vpp) "
vvp -N -l sim_icarus.log ${BUILD_DIR}/${SIM_NAME}
EC_SIM=$?

if [ ${EC_SIM} -ne 0 ]; then
    exit ${EC_SIM}
fi

# If enabled, run waveform viewer
if [ "$cmdopt_gtkwave" -eq "1" ]; then
    echo "Launching waveform viewer (gtkwave)"
    gtkwave ${DUMP_FILE} &
fi


exit $EC_SIM