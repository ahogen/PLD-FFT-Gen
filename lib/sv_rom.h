/******************************************************************************
 * Class:        twiddle
 *
 * Author:       Alexander Hogen
 * Date Created: 09/05/2016
 * Modified:     09/20/2016
 * Filename:     twiddle.h
 *
 * Constructors:
 *
 *    svROM ( string name, string initialization_filepath, string
 *            output_directory, int address_offset )
 *
 *         Constructor for using a plain-text initialization file.
 *         Optional output directory or address offset specification.
 *         Initializes all other data members to default empty values.
 *
 *     svROM ( string name, int values[], int num_vals, string
 *             output_directory, int address_offset )
 *
 *         Constructor for using an integer array for ROM data.
 *         Optional output directory or address offset specification.
 *         Initializes all other data members to default empty values.
 *
 * Destructors:
 *
 *	 ~svROM()
 *
 *         Destructor which deallocates the "rom_dat" dynamically allocated
 *         array apon object destruction.
 *
 * Public methods:
 *
 *	 bool ImportFile()
 *
 *         Locates and imports numeric strings from the initialization file.
 *         Compatible with both CSV and plain-text (one num per line) files.
 *         In the case of a CSV file, it will step through the data row by
 *         row.
 *
 *         While importing, it analyzes the data to find the number of bits
 *         required to represent the largest element and tracks the number
 *         of elements imported to the int array.
 *
 *         Any issues opening or importing the file are reported by
 *         returning FALSE.
 *
 *    bool Export()
 *
 *         Creates or overwrites a "r_name".sv file in the specified dir.
 *         The file complies with SystemVerilog HDL standards. The int
 *         data in "rom_dat" is converted to a hexadecimel representation.
 *
 *         Any issues opening or creating the file are reported by
 *         returning FALSE.
 *
 *    void Print()
 *
 *         Prints all the elements in "rom_dat" to the terminal window.
 *         Useful for checking to see which numbers were imported and
 *         in what order, before they are exported to a SV file.
 *
 *         No data members are modified as a result of this method.
 *
 *    int GetSizeBytes()
 *
 *         Returns the resulting ROM size in bytes, based on the bit width
 *         and number of elements in the ROM. Useful for obtaining an
 *         estimate of how large the ROM will be when implimented in an
 *         FPGA.
 *
 *         No data members are modified as a result of this method.
 *
 *****************************************************************************/

#ifndef SV_ROM_H
#define SV_ROM_H

#include <string>
using std::string;

/*!****************************************************************************
 * @brief When defined, log message calls will be included by
 *        the preprocessor.
 *
 * All log messages in this class are enclosed by ifs so by
 * simply commenting out this line (causing it to be not defined)
 * all log messages will be removed from this code.
 *
 *****************************************************************************/
#define ENABLE_LOG_SV_ROM 1 //!< Set to 0 to remove all \
                            //!< calls to the Logging class.

#if ENABLE_LOG_SV_ROM
#include "logger.h"
#endif

class svROM
{
public:
  svROM(const string &name,
        const string &initialize_file,
        const string &output_directory = ".",
        const int &addr_offset = 0,
        Logger *log_obj = nullptr);

  svROM(const string &name,
        const int values[],
        const int &num_vals,
        const string &output_directory = ".",
        const int &addr_offset = 0,
        Logger *log_obj = nullptr);

  ~svROM();

  // Read and analyze initialization file
  bool ImportFile();

  // Generate SystemVerilog ROM file
  bool Export();

  // Print initialization data to terminal
  void Print() const;

  // Get size of ROM block in bytes
  int GetSizeBytes() const;

  // Set and Get initialization file address

  // Set and Get output file directory

  // Set and Get starting address offset

private:
  // ROM members
  int r_length;
  int r_bitwidth;
  int r_addr_width;
  int r_addr_offset;
  int *rom_dat;

  string r_name;

  string r_init_file;
  string r_out_dir;
  bool imported;

  Logger *m_log; //!< Pointer to a logger class.

  void AddToArray(int new_value);
  string IntToHexStr(int num) const;
};

#endif // SV_ROM_H
