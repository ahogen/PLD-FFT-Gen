/*!****************************************************************************
 * @file
 * @brief  svMult_BW class definitions
 *
 * @author Alexander Hogen
 *
 * Definitions for all the ctors, dtor, and
 * methods of the svMult_BW class, with the
 * exception of the adder export functions which
 * are defined in a separate ".cpp" file.
 *
 * \todo There are no registeres every N adders. May not pass real-world tests
 *  at high speeds.
 *
 * References:
 *
 * [1] H. Eriksson, P. Larsson-Edefors, M. Sheeran,
 *     M. Sjalander, D. Johansson, and M. Scholin, "Multiplier
 *     Reduction Tree with Logarithmic Logic Depth and Regular
 *     Connectivity," in IEEE International Symposium on
 *     Circuits and Systems, May 2006.
 *     Available: http://bit.ly/2dvN2u3
 *
 * [2] M. Sjalander and P. Larsson-Edefors, "The Case for
 *     HPM-Based Baugh-Wooley Multipliers," Department of
 *     Computer Science and Engineering, Chalmers University
 *     of Technology, Tech. Rep. 08-8, March 2008.
 *     Available: http://bit.ly/2dt7DPP
 *
 *****************************************************************************/

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ios;
using std::ofstream;

#include <time.h> // To insert a timestamp in the generated
                  // SV ROM file.

//#include <cmath>

#include "sv_mult_bw.h"
#include "sv_adder_ripple_carry.h"

/*!****************************************************************************
 * @brief Constructor to initialize with a name
 *        and bit width.
 *
 * All data members initialized to their empty
 * or default values. The name of the multiplier
 * is set to the string passed in as the first
 * argument. The multiplier bit width is set
 * to the value of the second argument.
 *
 * @param name as a string
 * @param bitwidth as an integer
 *
 *****************************************************************************/
svMult_BW::svMult_BW(string n, int b, string o)
    : m_name("mult"), m_out_dir(o), m_bitwidth(4),
      pp_ra(nullptr), adder_ra(nullptr), adder_cnt(0)
{
  // Make sure string has characters and that the first
  // character *is* a letter, not a number or symbol.
  if (n != "" && isalpha(n.at(0)))
  {
    m_name = n;
  }

  // Make sure the bit width is at least 4 and
  // is an even number.
  if (b >= 4)
  {
    if (b % 2)
    {
      // b was odd. Change it to the next greater
      // even number
      m_bitwidth = b + 1;
    }
    else
    {
      // b was even. Simply copy it over.
      m_bitwidth = b;
    }
  }

  // Now we will dynamically allocate the space
  // needed for the partial product (PP) array
  pp_ra = new string *[m_bitwidth];
  for (int i = 0; i < m_bitwidth; i++)
  {
    pp_ra[i] = new string[2 * m_bitwidth];

    for (int j = 0; j < 2 * m_bitwidth; j++)
    {
      pp_ra[i][j] = "";
    }
  }
}

/*!****************************************************************************
 * @brief Destructor
 *
 * Deallocates the memory allocated for the
 * adder_ra and pp_ra.
 *****************************************************************************/
svMult_BW::~svMult_BW()
{
  // Deallocate (free) memory used for adder list
  delete[] adder_ra;

  // Deallocate memory used for partial product
  // string 2D array
  for (int i = 0; i < m_bitwidth; i++)
    delete[] pp_ra[i];

  delete[] pp_ra;
}

/*!****************************************************************************
 * @brief Generates and exports a HPM BW
 *        multiplier
 *
 *  Calls Generate() to create HPM reduction tree.
 *  then calls all other Export* methods to
 *  create all needed adder SV modules. Finally,
 *  calls the ExportMultTop() which creates the
 *  top-level multiplier SV module.
 *
 * @param none
 * @returns Boolean true if no errors occurred while executing
 *
 * @bug No error checking implemented.
 *
 *****************************************************************************/
bool svMult_BW::Export()
{
  bool success(true);

  success = success & Generate();

  success = success & ExportMultTop();

  return (success);
}

/*!****************************************************************************
 * @brief Generates and exports a HPM BW
 *        multiplier
 *
 *  Builds the HPM reduction tree from scratch, following the
 *  processes described in [1]. Then it writes all adder
 *  instantiation statements to the desired SystemVerilog file.
 *  It will also create half, full, and full1-adder modules, if
 *  they do not already exist.
 *
 * @param none
 * @returns Boolean true if no errors occurred while executing
 *
 *****************************************************************************/
bool svMult_BW::Generate()
{
  bool success(true);

  svAdder1B t_add;

  int largest_col = 0;
  int col_left = 0;
  int col_right = 0;
  int add_lvl = 0;

  // Create array of partial products
  // Also, flatten into a pyramid
  for (int i = 0; i < m_bitwidth; i++)
  {
    int max_col_depth = 0;

    for (int j = i; j < m_bitwidth + i; j++)
    {
      int row_offset = 0;

      // Find the first empty element in the current column
      // starting from the bottom (row 0)
      while (pp_ra[row_offset][j] != "")
      {
        row_offset++;
      }

      // Place the next partial product string in the empty
      // string array element, just found in the while loop
      // above.
      pp_ra[row_offset][j] = "p" + to_string(m_bitwidth - i - 1);
      pp_ra[row_offset][j] += "_" + to_string(m_bitwidth + i - j - 1);

      // Keep track of the largest (or "deepest") column
      if (max_col_depth < row_offset)
      {
        max_col_depth = row_offset;
        largest_col = j;
      }
    }
  }

  // Set the left and right column pointers
  col_right = largest_col;
  col_left = largest_col - 1;

  // Set adder level to total number of adder levels which will be
  // generated. See Table 2 in reference [2] listed at the top
  // of this file.
  add_lvl = m_bitwidth - 2;

  // Create first half-adder
  t_add.SetName("HA" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));
  t_add.SetType(HA);
  t_add.SetInA(pp_ra[0][col_right]);
  t_add.SetInB(pp_ra[2][col_right]);
  t_add.SetOutSum("s" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));
  t_add.SetOutCarry("c" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));

  // Add to adder array
  AddAdder(t_add);
  // Remove PPs from pp_ra and flatten pp_ra
  RemovePPbyCol(col_right, 1, 2);
  RemovePPbyCol(col_right, 1, 0);
  // Append the adder outputs to the pp_ra
  AppendAdderOutputs(t_add, col_right);

  //
  // Create second adder (the modified full adder)
  //
  t_add.SetName("FAone" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_left));
  t_add.SetType(FA1);
  t_add.SetInA(pp_ra[0][col_left]);
  t_add.SetInB(pp_ra[2][col_left]);
  t_add.SetOutSum("s" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_left));
  t_add.SetOutCarry("c" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_left));

  // Add to adder array
  AddAdder(t_add);
  // Remove PPs from pp_ra and flatten pp_ra
  RemovePPbyCol(col_left, 1, 2);
  RemovePPbyCol(col_left, 1, 0);
  // Append the adder outputs to the pp_ra
  AppendAdderOutputs(t_add, col_left);

  // Decrement the current adder level number
  add_lvl--;

  while ((m_bitwidth - NumEmptyPPRows()) > 2)
  {
    int col_current(0);
    col_left--;
    col_right++;

    // Create outer half adder
    t_add.SetName("HA" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));
    t_add.SetType(HA);
    t_add.SetInA(pp_ra[0][col_right]);
    t_add.SetInB(pp_ra[2][col_right]);
    t_add.SetOutSum("s" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));
    t_add.SetOutCarry("c" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_right));

    // Add to adder array
    AddAdder(t_add);
    // Remove PPs from pp_ra and flatten pp_ra
    RemovePPbyCol(col_right, 1, 2);
    RemovePPbyCol(col_right, 1, 0);
    // Append the adder outputs to the pp_ra
    AppendAdderOutputs(t_add, col_right);

    col_current = col_right - 1;

    while (col_current >= col_left)
    {
      // Create full adders
      t_add.SetName("FA" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_current));
      t_add.SetType(FA);
      t_add.SetInA(pp_ra[0][col_current]);
      t_add.SetInB(pp_ra[1][col_current]);
      t_add.SetInCarry(pp_ra[2][col_current]);
      t_add.SetOutSum("s" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_current));
      t_add.SetOutCarry("c" + to_string(add_lvl) + "_" + to_string((2 * m_bitwidth) - 2 - col_current));

      // Add to adder array
      AddAdder(t_add);
      // Remove PPs from pp_ra and flatten pp_ra
      RemovePPbyCol(col_current, 3);
      // Append the adder outputs to the pp_ra
      AppendAdderOutputs(t_add, col_current);

      col_current--;
    }

    add_lvl--;
  }

  return (success);
}

/*!****************************************************************************
 * @brief Creates top-level SV multiplier module
 *
 *  Creates file of name m_name + "_top.sv" which
 *  instantiates and interconnects the sub-level
 *  adders.
 *
 *  Generate() must have been called before
 *  calling this function.
 *
 * @param none
 * @returns Boolean true if no errors occurred while executing
 *
 * @bug No error checking implemented.
 *
 *****************************************************************************/
bool svMult_BW::ExportMultTop()
{
  bool success(false);
  time_t rawtime;
  struct tm *timeinfo;

  unsigned int fadder_bitwidth = (m_bitwidth * 2) - 2;

  ///////////////////////////////////////////////////////
  // Create full filepath string
  string full_output_name(m_out_dir + "/" + m_name + ".v");

#ifdef DEBUG_MULT_BW
  cout << "-- INFO: Attempting to open \"" << full_output_name;
  cout << "\" for writing"
       << " : ";
#endif // DEBUG_MULT_BW

  // Attempt to open the target ouput file
  ofstream f(full_output_name, ios::out | ios::trunc);

  if (f.is_open())
  {

#ifdef DEBUG_MULT_BW
    cout << "OPENED" << endl;
#endif // DEBUG_MULT_BW

    f << "///////////////////////////////////////////////////////////////////////////////////// \n";
    f << "// \n";
    f << "// Baugh-Wooley multiplier with High Performance Multiplier (HPM) reduction tree   \n";
    f << "// generator  \n";
    f << "// \n";
    f << "// Author:    Alexander Hogen (https://github.com/ahogen/)\n";
    f << "// Filename:  " << m_name << ".sv \n";

    // Get the local time to timestamp the output file
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    f << "// Generated: " << asctime(timeinfo);
    f << "// \n";
    f << "// Description: \n";
    f << "// \n";
    f << "//     This is a(n) " << m_bitwidth << " x " << m_bitwidth << " bit,";
    f << " single-cycle multiplier based off the work and research \n";
    f << "//     by H. Eriksson, P. Larsson-Edefors, M. Sheeran, M. Sjalander, D. Johansson, and \n";
    f << "//     M. Scholin. \n";
    f << "// \n";
    f << "// References: \n";
    f << "// \n";
    f << "//     [1] H. Eriksson, P. Larsson-Edefors, M. Sheeran, M. Sjalander, D. Johansson,\n";
    f << "//         and M. Scholin, \"Multiplier Reduction Tree with Logarithmic Logic Depth\n";
    f << "//         and Regular Connectivity,\" in IEEE International Symposium on Circuits\n";
    f << "//         and Systems, May 2006. Available: http://bit.ly/2dvN2u3 \n";
    f << "// \n";
    f << "//     [2] M. Sjalander and P. Larsson-Edefors, \"The Case for HPM-Based Baugh-Wooley \n";
    f << "//         Multipliers,\" Department of Computer Science and Engineering, Chalmers  \n";
    f << "//         University of Technology, Tech. Rep. 08-8, March 2008.\n";
    f << "//         Available: http://bit.ly/2dt7DPP \n";
    f << "// \n";
    f << "// DISCLAIMER: \n";
    f << "// \n";
    f << "// This code, in any raw, compiled, simulated, or synthesized form, is provided to \n";
    f << "// you and/or the end-user \"AS IS\" without any warranties or support. \n";
    f << "// \n";
    f << "// There is no warranty for the code, to the extent permitted by applicable law. \n";
    f << "// Except when otherwise stated in writing the copyright holders and/or other parties \n";
    f << "// provide the code \"AS IS\" without warranty of any kind, either expressed or \n";
    f << "// implied, including, but not limited to, the implied warranties of merchantability \n";
    f << "// and fitness for a particular purpose. The entire risk as to the quality and \n";
    f << "// performance of the code is with you. Should the code prove defective, you assume \n";
    f << "// the cost of all necessary servicing, repair, or correction. \n";
    f << "// \n";
    f << "// In no event unless required by applicable law or agreed to in writing will any \n";
    f << "// copyright holder, or any other party who modifies and/or conveys the code as \n";
    f << "// permitted above, be liable to you for damages, including any general, special, \n";
    f << "// incidental, or consequential damages arising out of the use or inability to use \n";
    f << "// the code (including but not limited to loss of data or data being rendered \n";
    f << "// inaccurate or losses sustained by you or third parties or a failure of the code \n";
    f << "// to operate with any other programs), even if such holder or other party has been \n";
    f << "// advised of the possibility of such damages.\n";
    f << "// \n";
    f << "// By using this code in any way, you agree to the above statements.\n";
    f << "///////////////////////////////////////////////////////////////////////////////////// \n\n";

    f << "`timescale 1ns / 1ns";

    f << "\n\n";

    /*************************************************
     * Generate definitions of small adder modules
     ************************************************/
    f << AdderDefStr(HA);
    f << AdderDefStr(FA);
    f << AdderDefStr(FA1);

    // Define the final adder
    f << "// Definition of final adder architecture \n";
    f << svAdderRippleDef(fadder_bitwidth);
    f << "\n\n";

    f << "module " << m_name << " (\n";
    f << "\tinput wire [" << m_bitwidth - 1 << ":0] a, \n";
    f << "\tinput wire [" << m_bitwidth - 1 << ":0] b, \n";
    f << "\toutput wire [" << (2 * m_bitwidth) - 1 << ":0] out \n";
    f << "); \n\n";

    /*************************************************
     * Create partial product wires
     ************************************************/
    f << "// Partial product wires\n";
    for (int i = 0; i < m_bitwidth; i++)
    {
      for (int j = i; j < m_bitwidth + i; j++)
      {
        f << "wire p" << to_string(i) << "_" << to_string(j - i) << ";\n";
      }
    }

    f << "\n\n";

    /*************************************************
     * Create all partial-product logic
     ************************************************/
    f << "// Partial product term generation\n";

    for (int i = 0; i < m_bitwidth; i++)
    {
      for (int j = 0; j < m_bitwidth; j++)
      {
        f << "assign p" << i << "_" << j << " = ";

        // If MSB of partial product row except MSB of last row
        // OR any bit (other than MSB) of last row
        if ((j == m_bitwidth - 1 && i != m_bitwidth - 1) ||
            (i == m_bitwidth - 1 && j != m_bitwidth - 1))
        {
          // out = nand( A, B )
          f << " ~( b[" << i << "] & a[" << j << "] );\n";
        }
        else
        {
          // out = and( A, B )
          f << "  ( b[" << i << "] & a[" << j << "] );\n";
        }
      }
    }

    f << "\n\n";

    /*************************************************
     * Instantiate 1-bit adders in HPM tree
     ************************************************/

    f << "// Output wires of 1-bit adders in HPM tree\n";
    for (int i = 0; i < adder_cnt; i++)
    {
      f << "wire " << adder_ra[i].GetOutSum() << ";\n";
      f << "wire " << adder_ra[i].GetOutCarry() << ";\n";
    }

    f << "// Instantiate 1-bit adders in HPM tree\n";
    for (int i = 0; i < adder_cnt; i++)
    {
      f << adder_ra[i].InstantiateStr();
      //f << adder_ra[i].InlineDataflow();
    }

    /*************************************************
     * Instantiate final adder
     ************************************************/
    // Create input and output wires for the final adder
    f << "wire [" << (fadder_bitwidth - 1) << ":0] fadd_a;\n";
    f << "wire [" << (fadder_bitwidth - 1) << ":0] fadd_b;\n";
    f << "wire [" << fadder_bitwidth << ":0] fadd_sum;\n";

    // Group the two remaining rows of the partial product array
    // into two wire buses to be fed into the final adder

    // REMEMBER: Start at column LSB + 1 and not the LSB column
    // because the LSB column has only one element (p0_0) and
    // will fall through to the final output of the multiplier. It
    // never needs to pass through the final adder

    for (unsigned int c = 0; c < fadder_bitwidth; c++)
    {
      f << "assign fadd_a[" << c << "] = " << pp_ra[0][fadder_bitwidth - c - 1] << ";\n";
      f << "assign fadd_b[" << c << "] = " << pp_ra[1][fadder_bitwidth - c - 1] << ";\n";

      // Remove the strings from the pp_ra which were just assigned
      // to the wires.
      RemovePPbyCol(fadder_bitwidth - c - 1, 2);
    }

    // Instantiate the final adder
    f << "// Instantiation of final adder \n";
    f << svAdderRippleInst("final_adder", "fadd_a", "fadd_b", "fadd_sum", fadder_bitwidth);
    f << "\n\n";

    /*************************************************
     * Collect and assign final multiplier outputs
     ************************************************/
    f << "\n";

    f << "// Invert the MSB before assigning it to the output \n";
    f << "assign out[" << (m_bitwidth * 2) - 1 << "] = ~fadd_sum[" << fadder_bitwidth << "];\n";
    f << "assign out[" << (m_bitwidth * 2) - 2 << ":1] = fadd_sum[" << fadder_bitwidth - 1 << ":0];\n";
    f << "// Assign the LSB of the partial product array directly to the output \n";
    f << "assign out[0] = " << pp_ra[0][fadder_bitwidth] << ";\n\n";

    RemovePPbyCol(fadder_bitwidth, 1);

    f << "endmodule \n\n";

    f.close();

    success = true;
  }
  else
  {
    success = false;
    cout << "ERROR: Could not open file for writing.\n";
    cout << "    \"" << full_output_name << "\"\n";
    cout << "    Does path exist?" << endl;
  }


  return (success);
}

/*!****************************************************************************
 * @brief Add an adder structure to the adder
 *        array.
 *
 * Handles all dynamic memory allocation and
 * updates the counter which keeps track of how
 * many adder structures there are in the list.
 *
 * @param new_adder as a sAdder structure
 * @returns void
 *
 *****************************************************************************/
void svMult_BW::AddAdder(const svAdder1B &new_adder)
{
  svAdder1B *temp_array = new svAdder1B[adder_cnt + 1];

  // Copy old adders to new adder array
  for (int i = 0; i < adder_cnt; i++)
  {
    temp_array[i] = adder_ra[i];
  }

  // Append the new adder to the last element of
  // the new adder array
  temp_array[adder_cnt] = new_adder;

  // Deallocate old adder array
  if (adder_ra != nullptr)
    delete[] adder_ra;

  // Copy pointer of new adder array to th old
  // adder array pointer
  adder_ra = temp_array;

  // Increment adder array length counter
  adder_cnt++;
}

/*!****************************************************************************
 * @brief Remove elements of 2D partial-product
 *        array by column.
 *
 * Remove elements (set string to "") in the
 * specified column and shift all remaining
 * elements down to the baseline. By default, it
 * starts at row 0
 *
 * @param col_sel is the column to remove elements from
 *
 * @param rm_num is the specific number of elements
 *        to remove, starting from the row_base offset.
 *
 * @param row_base is an optional parameter (default = 0)
 *        to specify the row to start removing rm_num
 *        elements in the column col_sel
 *
 *****************************************************************************/
void svMult_BW::RemovePPbyCol(int col_sel,
                              int rm_num,
                              int row_base)
{
  // Iterate through all rows in the selected column
  // starting at row "row_base"
  for (int r = row_base; r < m_bitwidth; r++)
  {
    // If the row we are currently on plus the number of elements
    // we need to remove is less than the total number of rows...
    if (r + rm_num < m_bitwidth)
    {
      // ... then overwrite the elements we want to remove
      // with the data in the above elements, effectively
      // sliding the column downwards.
      pp_ra[r][col_sel] = pp_ra[r + rm_num][col_sel];
    }

    // Otherwise if there is not an element above the one we are
    // currently at which we can copy data down from, we will
    // simply set it to a blank string ("")
    else
    {
      pp_ra[r][col_sel] = "";
    }
  }
}

/*!****************************************************************************
 * @brief Append the outputs of the adder to the
 *        partial product array.
 *
 * Places the sum string at the bottom of column
 * "col" and places carry out at the bottom of
 * "col" + 1. If RemovePPbyCol() is not called
 * before calling this function, a memory access
 * fault will occur because there won't be room
 * in the array for the new string.
 *
 * @param a as an sAdder. This is the adder
 *        structure containing the sum and
 *        carry strings which will be appended
 *        to the partial product array.
 *
 * @param col as an integer indicating which
 *        column to add the sum string to.
 *
 *****************************************************************************/
void svMult_BW::AppendAdderOutputs(const svAdder1B &a, int col)
{
  int row_offset(0);

  // Find the first empty element in the current column
  // starting from the top (row 0)
  while (pp_ra[row_offset][col] != "")
  {
    row_offset++;
  }

  // Append the adder's sum to the array
  pp_ra[row_offset][col] = a.GetOutSum();

  // Move to the column on the left
  col--;

  // Find the first empty element in the current column
  // starting from the top (row 0)
  while (pp_ra[row_offset][col] != "")
  {
    row_offset++;
  }

  // Append the adder's carry to the array
  pp_ra[row_offset][col] = a.GetOutCarry();
}

/*!****************************************************************************
 * @brief Prints the partial product array to the
 *        terminal window.
 *
 * Prints the array in the inverted pyramid form
 * which is useful for visualizing the current
 * status of the partial product array.
 *
 *****************************************************************************/
void svMult_BW::PrintPPArray() const
{
  for (int i = m_bitwidth - 1; i >= 0; i--)
  {
    for (int j = 0; j < (m_bitwidth * 2); j++)
    {
      cout << ((pp_ra[i][j] == "") ? "    " : pp_ra[i][j]);
      cout << " ";
    }
    cout << "\n";
  }
}

// /*!****************************************************************************
//  * @brief Prints the adder list to the terminal
//  *        window.
//  *
//  * Prints a list of the adders and their properties
//  * to the terminal window.
//  *
//  *****************************************************************************/
//void svMult_BW::PrintAdderArray() const
//{
//    for (int i = 0; i < adder_cnt; i++)
//    {
//        switch (adder_array[i].type)
//        {
//            case FA:
//                cout << "FA";
//                break;
//            case HA:
//                cout << "HA";
//                break;
//            case FA1:
//                cout << "Full with const 1";
//                break;
//        }
//
//        cout << " :: Inputs: " << adder_array[i].a;
//        cout << ", " << adder_array[i].b;
//
//        if (adder_array[i].type == FA)
//            cout << ", " << adder_array[i].c_in;
//
//        cout << " Outputs: " << adder_array[i].sum;
//        cout << ", " << adder_array[i].c_out;
//        cout << "\n\n";
//    }
//}

/*!****************************************************************************
 * @brief Calculates the number of empty ("")
 *        rows in the 2D string array
 *
 * Counts the number of rows of string elements
 * which are empty ( equal to "" ), starting
 * from the most significant row index.
 *
 * @returns Number of empty rows in the partial
 *         product array
 *
 *****************************************************************************/
int svMult_BW::NumEmptyPPRows() const
{
  bool empty_f(true);
  int cnt(0);

  // Start at last row and search backwards
  for (int r = m_bitwidth - 1; (r >= 0) && (empty_f); r--)
  {
    for (int c = 0; (c < m_bitwidth * 2) && empty_f; c++)
    {
      empty_f = (pp_ra[r][c] == "") ? true : false;
    }

    if (empty_f)
      cnt++;
  }

  return (cnt);
}
