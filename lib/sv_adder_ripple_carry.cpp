/*!****************************************************************************
 * @file
 * @author       Alexander Hogen
 *
 * @brief These functions generate SystemVerilog compatible
 * ripple-carry adders.
 *
 *****************************************************************************/

#include "sv_adder_ripple_carry.h"

string svAdderRippleInst(const string &inst_name,
                         const string &in_a_name,
                         const string &in_b_name,
                         const string &out_sum_name,
                         unsigned int bit_width)
{
  string str("");

  str = "ripple_carry_adder " + inst_name + " (\n";
  str += "   .a    ( " + in_a_name + "[";
  str += to_string(bit_width - 1);
  str += ":0] ),\n";
  str += "   .b    ( " + in_b_name + "[";
  str += to_string(bit_width - 1);
  str += ":0] ),\n";
  str += "   .out  ( " + out_sum_name + "[";
  str += to_string(bit_width);
  str += ":0] ) \n";
  str += " );\n";

  return (str);
}

string svAdderRippleDef(unsigned int bit_width)
{
  string str("");

  str += "module ripple_carry_adder ( \n";
  str += "\n";
  str += "\tinput wire [";
  str += to_string(bit_width - 1);
  str += ":0] a,\n";
  str += "\tinput wire [";
  str += to_string(bit_width - 1);
  str += ":0] b,\n";
  str += "\toutput wire [";
  str += to_string(bit_width);
  str += ":0] out\n";
  str += ");\n";
  str += "\n";
  //str += "/*****************************************************************************\n";
  //str += "*                            Constant Declarations                           *\n";
  //str += "*****************************************************************************/\n";
  str += "parameter BW = ";
  str += to_string(bit_width);
  str += " -1;\n";
  str += "\n\n";
  //str += "/*****************************************************************************\n";
  //str += "*                  Internal Wires and Register Declarations                  *\n";
  //str += "*****************************************************************************/\n";
  str += "wire [BW:0] carry;\n";
  str += "wire [BW:0] sum;\n";
  str += "\n";
  //str += "/*****************************************************************************\n";
  //str += "*                             Combinational logic                            *\n";
  //str += "*****************************************************************************/\n";
  str += "assign out = { carry[BW], sum[BW:0] };\n";
  str += "\n\n";
  str += "// Instantiate a single half adder module, for the first adder in the chain.\n";
  str += "half_adder HA (\n";
  str += "    .a    ( a[0] ),\n";
  str += "    .b    ( b[0] ),\n";
  str += "    .sum  ( sum[0] ),\n";
  str += "    .cout ( carry[0] )\n";
  str += "    );\n";
  str += "\n\n";
  str += "// Use a generate block to automatically create full adders to perform the rest\n";
  str += "// of the additions.\n";
  str += "genvar i;\n";
  str += "generate \n";
  str += "\n";
  str += "    for ( i = 1; i <= BW; i = i + 1) begin: FullAdderGenerate\n";
  str += "\n";
  str += "        full_adder FA (\n";
  str += "            .a    ( a[i] ),\n";
  str += "            .b    ( b[i] ),\n";
  str += "            .cin  ( carry[i-1] ),\n";
  str += "            .sum  ( sum[i] ),\n";
  str += "            .cout ( carry[i] )\n";
  str += "            );\n";
  str += "\n";
  str += "end \n";
  str += "\n";
  str += "endgenerate \n";
  str += "\n";
  str += "endmodule \n";

  return (str);
}
