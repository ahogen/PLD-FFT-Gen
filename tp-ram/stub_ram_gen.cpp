/*!****************************************************************************
 * @file
 * @brief Test the svRAM class
 *
 * @author Alexander Hogen
 * @date   01/06/2017
 *
 * Tests the svRAM class by creating a few System
 * Verilog RAM modules of various sizes.
 *****************************************************************************/

#include <iostream>
using std::cout;
using std::endl;

#define ENABLE_LOG_MAIN

#include "../lib/logger.h"
#include "../lib/sv_ram.h"

int main()
{
  logger log("tp-ram-execution", true, LOG_INFO, true, LOG_WARNING);

  // Remove previous log file (if the file name is same as provided above)
  log.clean();

#ifdef ENABLE_LOG_MAIN
  log.info("Started SystemVerilog RAM module generation test...");
#endif // ENABLE_LOG_MAIN

  svRAM test0("ram_512B", 8, 512, &log);
  test0.Export("out");

  svRAM test1("ram_2kB", 16, 1024, &log);
  test1.Export("out");

  svRAM test2("ram_16kB", 32, 4096, &log);
  test2.Export("out");

#ifdef ENABLE_LOG_MAIN
  log.info("Exiting the RAM generation test...");
#endif // ENABLE_LOG_MAIN

  return 0;
}
