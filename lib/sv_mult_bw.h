/*!****************************************************************************
 * @file
 * @author       Alexander Hogen
 * @brief Class which generates a Baugh-Wooley
 *        SystemVerilog compatible multiplier.
 *
 * This class generates a Baugh-Wooley multiplier
 * with a High Performance Multiplier (HPM)
 * reduction tree. Simply instantiate an object
 * with the desired multiplier bit width and
 * optionally specify an output directory. Then
 * call the Generate() method and a SystemVerilog
 * multiplier will be generated.
 *
 *****************************************************************************/

#ifndef SVMULT_BW_H
#define SVMULT_BW_H

#include "sv_adder1b.h"

#include <string>
using std::string;

/*!****************************************************************************
 * @brief   Class which generates a Baugh-Wooley
 *          SystemVerilog compatible multiplier.
 *
 * This class generates a Baugh-Wooley multiplier
 * with a High Performance Multiplier (HPM)
 * reduction tree. Simply instantiate an object
 * with the desired multiplier bit width and
 * optionally specify an output directory. Then
 * call the Generate() method and a SystemVerilog
 * multiplier will be generated.
 *
 *****************************************************************************/
class svMult_BW
{

public:
  svMult_BW(string name, int bitwidth, string out_dir = ".");
  ~svMult_BW();

  bool Export();

protected:
private:
  string m_name;    //!< The name of the multiplier. Used when creating the SV module.
  string m_out_dir; //!< The directory in which the SystemVerilog file(s) will be created
  int m_bitwidth;   //!< Defines how wide the multiplier is
  string **pp_ra;   //!< 2D array of strings with dimensions (m_bitwidth) by (2*m_bitwidth)

  svAdder1B *adder_ra; //!< A dynamically allocated array of adder structures
  int adder_cnt;       //!< The number of adders in adder_list

  bool Generate();

  //bool ExportAdderHalf();
  //bool ExportAdderFull();
  //bool ExportAdderFullOne();
  //bool ExportAdderKoggeStone();

  bool ExportMultTop();

  void AddAdder(const svAdder1B &new_adder);
  void RemovePPbyCol(int col_sel, int rm_num, int row_base = 0);
  void AppendAdderOutputs(const svAdder1B &t_adder, int col);
  void PrintPPArray() const;
  //void PrintAdderArray() const;
  int NumEmptyPPRows() const;
  //string Int2String( int num ) const;
};

#endif // SVMULT_BW_H
