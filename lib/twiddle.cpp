/******************************************************************************
 * Author:              Alexander Hogen
 * Date Created:        09/10/2016
 * Modified:            09/20/2016
 * Filename:            twiddle.cpp
 * Parent header file:  twiddle.h
 *
 * Overview:
 *
 *    This file contains the declarations of all methods
 *    in the 'twiddle' class.
 *****************************************************************************/

#include "twiddle.h"

#include <cmath>
#define PI 3.14159265358979323846
using std::ceil;
using std::cos;
using std::log2;
using std::pow;
using std::round;
using std::sin;

/******************************************************************************
 * Purpose: Constructor
 *
 * Entry:   Object instantiated with (int, int)
 *
 * Exit:    All data members are initialized. "t_length" and
 *          "t_bit_width" are set to "fft_length" and
 *          "fft_bit_width" respectively, if they are >= 2.
 *****************************************************************************/
twiddle::twiddle(const int &fft_length, const int &fft_bit_width)
    : r_vals(nullptr), i_vals(nullptr), t_length(0), t_bit_width(0)
{
#ifdef DEBUG_TWIDDLE
  cout << "ENTERED: Twiddle ctor" << endl;
#endif // DEBUG_TWIDDLE

  if (fft_length >= 2)
    t_length = fft_length;

  if (fft_bit_width >= 2)
    t_bit_width = fft_bit_width;

#ifdef DEBUG_TWIDDLE
  cout << "EXITING: Twiddle ctor" << endl;
#endif // DEBUG_TWIDDLE
}

/******************************************************************************
 * Purpose: Destructor. Safely deallocates "r_vals" and
 *          "i_vals" memory blocks.
 *
 * Entry:   Destruction of the object or "delete" command.
 *
 * Exit:    Both int pointer members set to nullptr
 *
 *****************************************************************************/
twiddle::~twiddle()
{
#ifdef DEBUG_TWIDDLE
  cout << "INFO: Twiddle dtor has been fired" << endl;
#endif // DEBUG_TWIDDLE

  if (r_vals != nullptr)
  {
    delete[] r_vals;
    r_vals = nullptr;
  }

  if (i_vals != nullptr)
  {
    delete[] i_vals;
    i_vals = nullptr;
  }
}

/******************************************************************************
 * Purpose: Generate signed integer real and imaginary
 *          twiddle factors for use in an FFT of length
 *          "t_length" and width "t_bit_width".
 *
 * Entry:   Any twiddle object or friend class/function
 *
 * Exit:    "r_vals" and "i_vals" filled with (t_length/2)
 *          base-10 integer twiddle factors
 *****************************************************************************/
bool twiddle::Generate()
{
  bool success(false);
  int num_twiddle_factors = (t_length / 2);
  int address_bw = ceil(log2(num_twiddle_factors));
  int t_max_val(0);
  float t_scale(0.0);

#ifdef DEBUG_TWIDDLE
  cout << "\nENTERED: twiddle Generate() " << endl;
#endif // DEBUG_TWIDDLE

  // Calculate the largest number that an unsigned number with
  // t_bit_width number number of bits can hold
  for (int i = 0; i < t_bit_width; i++)
  {
    t_max_val = t_max_val + pow(2, i);
  }

#ifdef DEBUG_TWIDDLE
  cout << "-- INFO: Largest unsigned " << t_bit_width;
  cout << " bit number is " << t_max_val << endl;
#endif // DEBUG_TWIDDLE

  // Set the twiddle factor scale (?)
  t_scale = pow(2, t_bit_width - 1) - 1;

  // Clean out arrays if there is existing data, possibly
  // from a previous execution of "Generate()"
  if (r_vals != nullptr)
  {
#ifdef DEBUG_TWIDDLE
    cout << "-- INFO: Deallocating real value array" << endl;
#endif // DEBUG_TWIDDLE

    delete[] r_vals;
  }

  if (i_vals != nullptr)
  {
#ifdef DEBUG_TWIDDLE
    cout << "-- INFO: Deallocating imag value array" << endl;
#endif // DEBUG_TWIDDLE

    delete[] i_vals;
  }

  // Get memory blocks for new arrays
  r_vals = new int[num_twiddle_factors];
  i_vals = new int[num_twiddle_factors];

  // Verify that memory was successfully allocated
  if (r_vals != nullptr && i_vals != nullptr)
  {
    // Generate twiddle factors!
    for (int k = 0; k < num_twiddle_factors; k++)
    {
      r_vals[k] = round(cos(2 * PI * k / t_length) * t_scale);
      i_vals[k] = round(sin(2 * PI * k / t_length) * t_scale);
    }

    success = true;
  }

#ifdef DEBUG_TWIDDLE
  cout << "EXITING: twiddle Generate() with status: ";
  cout << (success ? "SUCCESS \n" : "FAIL <----------<<  \n") << endl;
#endif // DEBUG_TWIDDLE

  return (success);
}

/******************************************************************************
 * Purpose: Create two svROM objects with calculated twiddle
 *          factors and call them to generate System Verilog
 *          ROM modules.
 *
 * Entry:   Any twiddle object or friend class/function
 *
 * Exit:    Two SystemVerilog modules ('.sv') in "out_dir"
 *          directory created/overwritten using the data in
 *          real and imaginary arrays.
 *
 *          No data members are directly modified. Calls
 *          Generate() which does modify data members.
 *****************************************************************************/
bool twiddle::Export(const string &out_dir)
{
  bool success(false);

#ifdef DEBUG_TWIDDLE
  cout << "ENTERED: twiddle Export()" << endl;
#endif // DEBUG_TWIDDLE

  if (this->Generate())
  {

#ifdef DEBUG_TWIDDLE
    cout << "-- Instantiate svROM objects" << endl;
#endif // DEBUG_TWIDDLE
    svROM real_rom("twiddle_rom_real", r_vals, t_length / 2, out_dir);
    svROM imag_rom("twiddle_rom_imag", i_vals, t_length / 2, out_dir);

    if (real_rom.Export() && imag_rom.Export())
    {
      success = true;
    }
  }

#ifdef DEBUG_TWIDDLE
  cout << "EXITING: twiddle Export() with status: ";
  cout << (success ? "SUCCESS \n" : "FAIL <----------<< \n") << endl;
#endif // DEBUG_TWIDDLE

  return (success);
}
