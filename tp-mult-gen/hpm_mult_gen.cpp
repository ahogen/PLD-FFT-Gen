#include <limits.h>
#include <sys/param.h>
#include <stdlib.h>    // realpath

#include <cstring>

#include <string>
using std::string;
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;


#include "sv_mult_bw.h"


#define DXSTR(s) DSTR(s)
#define DSTR(s) #s

#define DEFAULT_BITWIDTH     8
#define DEFAULT_IEEESTD      1364
#define DEFAULT_FILENAME    "mult_" DXSTR(DEFAULT_BITWIDTH) "b"

const char * program_version = "pld-mult-gen-cli 0.9-rc1";
const char * program_bug_address = "<code.ahogen@outlook.com>";

static char doc[] =
"##############################################################################\n"
"PLD Multiplier Generation Utility\n"
"\n"
"Author:    Alex Hogen (code.ahogen@outlook.com, https://github.com/ahogen)\n"
"Date:      " __DATE__ "\n"
"License:   MIT\n"
"Copyright (C) 2019 Alexander Hogen\n"
"\n"
"This is a utility to generate code for an FPGA which implements multiplication\n"
"of binary numbers. The multiplier is of the Bough-Wooley architecture with \n"
"the High Performance Multiplier (HPM) reduction tree by Sjalander et al. \n"
"\n"
"A Sjalander, one of the authors, has a generator utility of his own, which\n"
"has many more options and is probably better written than this utility. It \n"
"generates code with VHDL syntax.\n"
"Check it out here: http://sjalander.com/research/multiplier \n"
"\n"
"I built this tool because I've been using Verilog, not VHDL, and wanted to \n"
"see how this multiplier worked. So it was for my education more than anything.\n"
"\n"
" [1] H. Eriksson, P. Larsson-Edefors, M. Sheeran, \n"
"     M. Sjalander, D. Johansson, and M. Scholin, \"Multiplier \n"
"     Reduction Tree with Logarithmic Logic Depth and Regular \n"
"     Connectivity,\" in IEEE International Symposium on \n"
"     Circuits and Systems, May 2006. \n"
"     Available: http://bit.ly/2dvN2u3 \n"
" \n"
" [2] M. Sjalander and P. Larsson-Edefors, \"The Case for \n"
"     HPM-Based Baugh-Wooley Multipliers,\" Department of \n"
"     Computer Science and Engineering, Chalmers University \n"
"     of Technology, Tech. Rep. 08-8, March 2008. \n"
"     Available: http://bit.ly/2dt7DPP \n"
"\n"
"\n"
"*****************************************************************************\n"
"USAGE: hpm_mult_gen [-b <input bitwidth>] [-v] [-h] [-o <out filename>]\n"
//"                    [--std=ieee1364|ieee1800]  \n"
"\n\n"
"PARAMETERS:\n"
"   -b, --bitwidth                 Bitwidth of the inputs to the multiplier.\n"
"                                  Output bitwidth will be double this size.\n"
"                                  [default = " DXSTR(DEFAULT_BITWIDTH) "]\n"
"\n"
"   -o, --output                   Output filename. Extension is automatically\n"
"                                  appended to the string provided. \n"
"                                  [default = \"" DEFAULT_FILENAME "\"]\n"
"\n"
// "   --ieeestd=1364|1800            Output code in IEEE 1364 (Verilog-2001) or\n"
// "                                  1800 (SystemVerilog) syntax.\n"
// "                                  [default = " DXSTR(DEFAULT_IEEESTD) "]\n"
// "\n"
"   -h, --help                     Display this message\n"
"   -v, --verbose                  Enable verbose console output.\n"
"##############################################################################";


int main (int argc, char **argv)
{
    // Options
    bool verbose = false;
    uint32_t bitwidth = DEFAULT_BITWIDTH;
    uint16_t syntax_std = DEFAULT_IEEESTD;
    string out_fname = "";
    string out_fdir = ".";

    // Was going to use GNU's argp, but it's not avaliable in MinGW/TDM-GCC
    // so parsing args manually for now. Might look into another library later.
    for (int i = 1; i < argc; i++)
    {
        if ((strcmp(argv[i], "-v") == 0) || (strcmp(argv[i], "--verbose") == 0))
        {
            verbose = true;
        }
        else if ((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "--help") == 0))
        {
            cout << doc << endl;
            exit(0);
        }
        else if ((strcmp(argv[i], "-b") == 0) || (strcmp(argv[i], "--bitwidth") == 0))
        {
            if ((argc >= (i + 1)) && (argv[i+1] != NULL))
            {
                i += 1;
                bitwidth = strtoul(argv[i], NULL, 10);
            }
            else
            {
                cerr << "ERROR: Missing parameter" << endl;
                return(-1);
            }
        }
        else if ((strcmp(argv[i], "-o") == 0) || (strcmp(argv[i], "--output") == 0))
        {
            if ((argc >= (i + 1)) && (argv[i+1] != NULL))
            {
                i += 1;
                out_fname = argv[i];

                // Split into directory path and file name
                size_t basename_split_pos = out_fname.find_last_of("/\\");
                if (basename_split_pos != string::npos)
                {
                    // No trailing slash in dir name. No slash at front of fname.
                    out_fdir = out_fname.substr(0, basename_split_pos);
                    out_fname = out_fname.substr(basename_split_pos+1);
                }
            }
            else
            {
                cerr << "ERROR: Missing parameter" << endl;
                return(-1);
            }
        }
        // else if (strcmp(argv[i], "--ieeestd=1364") == 0)
        // {
        //     syntax_std = 1364;
        // }
        // else if (strcmp(argv[i], "--ieeestd=1800") == 0)
        // {
        //     syntax_std = 1800;
        // }
        else
        {
            cerr << "ERROR: Unrecognized argument: " << argv[i] << endl;
            cout << "Try \"--help\" for assistance";
            //cout << doc;
            exit(-2);
        }

    }

    if (out_fname == "")
    {
        out_fname = "mult_";
        out_fname += std::to_string(bitwidth);
        out_fname += "b";
    }

    if (verbose)
    {
        cout << "CONFIGURATION: \n";
        cout << "   Verbose:          " << verbose << endl;
        cout << "   Bitwidth:         " << bitwidth << endl;
        cout << "   Syntax Standard:  IEEE " << syntax_std << endl;
        cout << "   Output filename:  \"" << out_fdir + "/" + out_fname << ".[v/sv]\"" << endl;
    }

    svMult_BW mult1(out_fname, bitwidth, out_fdir);
    cout << "Generating..." << endl;
    return(!mult1.Export()); // Returns "false" if failed. Invert to return non-zero
}