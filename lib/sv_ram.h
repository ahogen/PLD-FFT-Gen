// File-level comments for Doxygen go here

#ifndef SV_RAM_H
#define SV_RAM_H

#include <string>
using std::string;

#include "logger.h"

/*!****************************************************************************
* @brief When defined, log message calls will be included by
*        the preprocessor.
*
* All log messages in this class are enclosed by ifdefs so by
* simply commenting out this line (causeing it to be not defined)
* all log messages will be removed from this code.
*
******************************************************************************/
#define ENABLE_LOG_SV_RAM //!< Remove/comment out this line to remove all \
                          //!< calls to the logging class.

// Class descriptive comments for Doxygen go here

class svRAM
{

public:
  svRAM();
  svRAM(const char *name, const unsigned int width, const unsigned int length, logger *log_obj = nullptr);

  // Let the data members' destructors take care of themselves.
  // No need to write a destructor for this class.
  // ~svRAM();

  void SetName(const string &name);
  void SetLength(const unsigned int length);
  void SetWidth(const unsigned int width);

  string GetName() const;
  unsigned int GetLength() const;
  unsigned int GetWidth() const;

  bool Export(const char *out_dir = ".") const;

protected:
private:
  string m_name;             //!< The name of this RAM module. Must be a unique identifier
  unsigned int m_bitwidth;   //!< The bit width of each element in the RAM array
  unsigned int m_length;     //!< The number of elements in the RAM array
  unsigned int m_addr_width; //!< The bit width of the address field

  logger *m_log; //!< Pointer to a logger class.

  void UpdateAddrWidth();
};

#endif // SV_RAM_H
