/*!****************************************************************************
* @file
* @author Alexander Hogen
* @date   2017-02-22
*
* @brief Logger class header file
*
* The Logger class as well as the associated
* log level enum TLogLevel are declared in this
* file.
*
******************************************************************************/

// \todo Build command-line argument parsing
// \todo Document all methods and members with Doxygen style comments

#ifndef LOGGER_H
#define LOGGER_H

#include <string>
using std::string;
using std::to_string; // Included for the user to convert
                      // numbers to strings in a log message

#include <cstdio>
using std::remove; // Used by logger::clean()

/*!****************************************************************************
* @brief  Defines log message severity levels
*
* This enum defines all the possible levels of
* logging, ranging from greatest severity
* LogLevel::LOG_CRITICAL to least severe
* LogLevel::LOG_DEBUG. The actual enumeration
* numbering is ordered such that the more
* severe log level has a higher numerical value.
*
* In the Logger::Log() method filters messages
* based on this scale.
*
* LOG_LVL_COUNT is simply provided to allow
* the code to determine how many levels are
* defined by this struct. LOG_LVL_COUNT itself
* should not be used as a valid logging level.
* Instead, you will know that (LOG_LVL_COUNT - 1)
* is the largest (numerically) or most severe
* log level.
*
******************************************************************************/
enum TLogLevel
{
  LOG_DEBUG = 0, //!< Debug: Nitty-gritty, nerd-level messages
  LOG_INFO,      //!< Info: No danger. Just (hopefully helpful) information
  LOG_WARNING,   //!< Warning: Not good, but we should be fine
  LOG_ERROR,     //!< Error: Pretty bad, but we can continue running
  LOG_CRITICAL,  //!< Critical: execution-breaking problems
  LOG_LVL_COUNT  //!< Don't use. Only used in code to determine the number of logging levels defined
};

/*!****************************************************************************
* @brief Class which handles the printing or
* exporting of log messages of various severities.
*
* This class provides an interface to filter the
* displaying of informative messages. Currently
* two logging methods are supported:
*
* <ol>
*    <li> Displaying messages via a terminal window </li>
*    <li> Appending messages to a log file </li>
* </ol>
*
* For log levels, please see the description
* for the @ref TLogLevel enumerated type.
*
* Almost everything you need to get started using
* this class is performed at construction, so
* see @ref Logger::Logger() for some more info.
*
* Take note of the default parameters of the
* constructor, shown below.
* @snippet lib/logger.h logger-defaults
*
* <b>Example 1:</b> Use default configuration
*
* @snippet tp-logger/stub-logging.cpp Use defaults
*
* <b>Example 2:</b> Log everything to the terminal.
* No file logging.
*
* @snippet tp-logger/stub-logging.cpp Custom terminal only
*
* <b>Example 3:</b> Messages with LOG_WARNING
* severity or worse are printed to the terminal.
* Everything but LOG_DEBUG messages written to
* "verbose.log" file.
*
* @snippet tp-logger/stub-logging.cpp Custom terminal and file
*
* <b>Example 4:</b> Configured with command line
* arguments. See @ref Logger::CmdArgConfig().
*
* @snippet tp-logger/stub-logging.cpp Cmd arg config
******************************************************************************/
class Logger
{

public:
  //! [logger-defaults]
  Logger(bool log_to_terminal = true,
         TLogLevel term_log_level = LOG_INFO,
         bool log_to_file = true,
         TLogLevel file_log_level = LOG_WARNING,
         const char *file_path = "./default.log");
  //! [logger-defaults]

  // Uses command-line arguments
  Logger(const int &argc, char *argv[]);

  ~Logger();

  // All the log-generating methods
  void Log(const TLogLevel &level, const string &msg) const;

  //! Method for @ref TLogLevel::LOG_CRITICAL level messages
  inline void Critical(const string &msg) const { Log(LOG_CRITICAL, msg); };

  //! Method for @ref TLogLevel::LOG_ERROR level messages
  inline void Error(const string &msg) const { Log(LOG_ERROR, msg); };

  //! Method for @ref TLogLevel::LOG_WARNING level messages
  inline void Warning(const string &msg) const { Log(LOG_WARNING, msg); };

  //! Method for @ref TLogLevel::LOG_INFO level messages
  inline void Info(const string &msg) const { Log(LOG_INFO, msg); };

  //! Method for @ref TLogLevel::LOG_DEBUG level messages
  inline void Debug(const string &msg) const { Log(LOG_DEBUG, msg); };

  /*!**************************************************************************
  * @brief Tool to remove a previous log file
  *
  * Will attempt to delete a file at
  * Logger::file_path if called. Make sure you
  * know what the file is currently pointing to
  * before calling this function!
  *
  * See @ref Logger::GetFilePath()
  ****************************************************************************/
  // Call this to remove any log file created by this class
  inline void Clean() const { std::remove(file_path.c_str()); };

  /*!**************************************************************************
  * @brief Enable or disable terminal logging
  * @param en is a bool. Pass in 'true' to enable
  *        logging to the terminal.
  ****************************************************************************/
  void SetTerminalEnable(bool en) { term_en = en; };
  bool SetTerminalThreshold(TLogLevel new_level);

  /*!**************************************************************************
  * @brief Enable or disable file logging
  * @param en is a bool. Pass in 'true' to enable
  *        logging to the file.
  ****************************************************************************/
  void SetFileEnable(bool en) { file_en = en; };
  bool SetFileThreshold(TLogLevel new_level);
  bool SetFilePath(const string &target_file);

  /*!**************************************************************************
  * @brief Get current file path string
  * @returns Full path to current log file, as set
  *         by constructor, command line arguments,
  *         or user code.
  ****************************************************************************/
  string GetFilePath() { return (file_path); };

  bool CmdArgConfig(const int &argc, char *argv[]);

private:
  //--------------------
  // Data members
  //--------------------
  bool term_en;       //!< Enables (when true) displaying messages to the terminal window
  TLogLevel term_lvl; //!< Sets the log threshold for terminal messages

  bool file_en;       //!< Enables (when true) writing log messages to a log file. Note: Must set log_name
  TLogLevel file_lvl; //!< Sets the log threshold for log file messages
  string file_path;   //!< The full path to the target log file.

  //--------------------
  // Methods
  //--------------------
  //! Format and print log message to terminal/console window
  void Log2Terminal(const TLogLevel log_severity, const char *lvl_str, const string &msg) const;

  //! Format and print log message to log file, specified in Logger::file_path
  void Log2File(const char *lvl_str, const string &msg) const;

  //! Get the string version of the log severity type
  char const *LogTypeStr(TLogLevel log_type) const;
};

#endif // LOGGER_H
