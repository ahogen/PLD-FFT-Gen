onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label m'and -radix decimal /mult_tb/mand
add wave -noupdate -label m'ier -radix decimal /mult_tb/mier
add wave -noupdate -label {product DUT} -radix decimal /mult_tb/product
add wave -noupdate -label {product actual} -radix decimal /mult_tb/t_product
add wave -noupdate -divider Counters
add wave -noupdate -label test_counter -radix decimal /mult_tb/test_counter
add wave -noupdate -label error_counter -radix unsigned /mult_tb/error_cnt
add wave -noupdate -label pass_counter -radix unsigned /mult_tb/pass_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {19478442 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4 ns}
