///////////////////////////////////////////////////////////////////////////////
//
// Author:  Alexander Hogen
// Created: 10/8/2016
// Updated: 10/15/2016
// Purpose: Verify the combinational logic of a multiplier module with a
//          specified bit width. You must change the parameter below to match
//          the multiplier's bit width for the test to function correctly.
//
///////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ns
`default_nettype none

`define VERBOSE_OUTPUT
`define SIM_DUMP_OUTPUT
`define PRINT_FAIL_SIMTIME
`define FINISH_WHEN_COMPLETE
`define COLUMN1 5
`define COLUMN2 30
`define COLUMN3 40

// This might be best to define in your tool's build flags. In our case, add
// this to the Icarus compiler flags in the bash script.
`ifdef USE_ANSI_PRINT_CODES
    `define CURSOR_2_COL(X) $write("\033[%0dG", X)
    `define DISPLAY_PASS(X) $write("\033[32m[PASS] ")
    `define DISPLAY_WARN(X) $write("\033[33m[WARN] ")

    `ifdef PRINT_FAIL_SIMTIME
        `define DISPLAY_FAIL(X) $write("\033[31;1m[FAILED] @ %0tt ", $time)
    `else
        `define DISPLAY_FAIL(X) $write("\033[31;1m[FAILED] ")
    `endif
    `define DISPLAY_2_DEFAULT(X) $write("\033[0m")
`else
    `define CURSOR_2_COL(X) $write("  ")
    `define DISPLAY_PASS(X) $write("[PASS] ")
    `define DISPLAY_WARN(X) $write("[WARNING] ")

    `ifdef PRINT_FAIL_SIMTIME
        `define DISPLAY_FAIL(X) $write("[FAILED] @ %0tt ", $time)
    `else
        `define DISPLAY_FAIL(X) $write("[FAILED] ")
    `endif
    `define DISPLAY_2_DEFAULT(X) $write("")
`endif

module mult_tb ();

/**********************************************************
* SETUP PARAMETERS
**********************************************************/
// Set to equal the bit width of the DUT mutliplier
parameter BIT_WIDTH = 16;

/**********************************************************
* TESTBENCH SIGNALS
**********************************************************/
// (Subtract 1 from bit width to simplify definitions below)
localparam BW = BIT_WIDTH -1;

reg signed [BW:0] mand;
reg signed [BW:0] mier;
wire [(BIT_WIDTH * 2) - 1 : 0] product;
reg signed [(BIT_WIDTH * 2) - 1 : 0] DUT_product;

/********************************************
* Test Bench Configuration
********************************************/
//localparam DUT_EXPECTED_LATENCY = 1;
//localparam CLOCK_PERIOD         = 2;

//reg            clk;
//reg            rst_n;

integer _error_count = 0;
integer _pass_count = 0;

// `ifdef SIM_DUMP_OUTPUT
//     initial begin
//         $dumpfile("sim_dump.sim");
//         $dumpvars(0, mult_tb);
//     end
// `endif

// always
// begin
//     clk <= 0;
//     #(CLOCK_PERIOD/2)
//     clk <= 1;
//     #(CLOCK_PERIOD/2)
//     clk <= 0;
// end

/**********************************************************
* INSTANTIATE DUT(s)
**********************************************************/
// multiplier DUT (
    // .X   ( mand[BW:0] ),
    // .Y   ( mier[BW:0] ),
    // .P ( product[(BIT_WIDTH * 2) - 1 : 0] )
// );

mult DUT (
    .a   ( mand[BW:0] ),
    .b   ( mier[BW:0] ),
    .out ( product[(BIT_WIDTH * 2) - 1 : 0] )
);


/**********************************************************
* TESTBENCH LOGIC
**********************************************************/
reg signed [(BIT_WIDTH * 2) - 1 : 0] t_product;

reg [BW:0]  MAX_VAL;

integer report;

// INITIAL BLOCk
initial begin
	$display( "Initializing... " );

	mand = 0;
	mier = 0;
	DUT_product = 0;
	t_product = 0;
	_error_count = 0;
	_pass_count = 0;
	MAX_VAL = 'h0;
	MAX_VAL = ~MAX_VAL;

	//$urandom(SEED);

	// Any other initialization code here

	$display( "Starting testbench... " );

	end


initial begin

	while ( (mand != MAX_VAL) || (mier != MAX_VAL) )
	begin

		{mand, mier} = {mand, mier} + 1;

		// Short pause
		#1;

		// Calculate actual product
		t_product = mand * mier;

		// Copy product coming out of DUT into the signed buffer
		DUT_product = product;

		// Short pause
		#1;

		$write( "%0d x %0d = %0d ? .... ", mand, mier, DUT_product);
		`CURSOR_2_COL(`COLUMN3);

		// Test output
		if (t_product != DUT_product) begin
			`DISPLAY_FAIL();
			_error_count = _error_count + 1;
			end

		else begin
			`DISPLAY_PASS();
			_pass_count = _pass_count + 1;
			end

		$write("\n"); `DISPLAY_2_DEFAULT();
	end


	$display( "===== RESULTS =====" );
	$display( "Tested a %0dx%0d multiplier", BIT_WIDTH, BIT_WIDTH);
	$display( "Multiplication tests passed: %0d", _pass_count );
	$display( "Multiplication tests failed: %0d", _error_count );
	$display( "Success rate: %0d%%", ((_pass_count) / (_pass_count + _error_count))*100 );

	report = $fopen("mult_tb_REPORT.txt","w"); // a = Writing to end of file (append)
	$fdisplay( report, "Tested a %0dx%0d multiplier", BIT_WIDTH, BIT_WIDTH);
	$fdisplay( report, "Multiplication tests passed: %0d", _pass_count );
	$fdisplay( report, "Multiplication tests failed: %0d", _error_count);
	$fdisplay( report, "Success rate: %0d%%", ((_pass_count) / (_pass_count + _error_count))*100 );
	$fdisplay( report, "=================================================");
	$fclose(report);

	$write("\n");
	if (_error_count == 0)
		begin
			`ifdef FINISH_WHEN_COMPLETE
				// This completes the simulation, in some tools this will
				// close the simulation results.
				$finish;
			`else
				// This returns control to the simulation engine
				$exit;
			`endif
		end
	else
		begin
			`DISPLAY_FAIL();
			$write("Testbench completed. Error count: %0d", _error_count);
			`DISPLAY_2_DEFAULT();
			// Run vvp with -N option to return non-zero with $stop
			$stop;
		end
end

endmodule

`default_nettype wire
