`timescale 1ns / 1ps
///////////////////////////////////////////////////////////
//
// Author:  Alexander Hogen
// Created: 10/9/2016
// Updated: 10/9/2016
// Purpose: Verify the combinational logic of a half-adder
//          module.
//
///////////////////////////////////////////////////////////
module half_adder_tb ();


/**********************************************************
*                    SETUP PARAMETERS
**********************************************************/

/**********************************************************
*                    TESTBENCH SIGNALS
**********************************************************/
logic input_a;
logic input_b;
wire output_sum; 
wire output_carry;

logic expected_sum;
logic expected_carry;

integer fail_count;
integer test_count;
integer report;

/**********************************************************
*                    INSTANTIATE DUT(s)
**********************************************************/

half_adder DUT (
	.a    ( input_a ),
	.b    ( input_b ),
	.sum  ( output_sum ),
	.cout ( output_carry )
);

/**********************************************************
*                     TESTBENCH LOGIC
**********************************************************/



// INITIAL BLOCk
initial begin
	$display( "Initializing... " );
	
	input_a = 0;
	input_b = 0;
	expected_sum = 0;
	expected_carry = 0;
	fail_count = 0;
	test_count = 0;

	// Any other initialization code here
	
	$display( "Write header to output report");
	
	report = $fopen("half_adder_tb_REPORT.txt","w"); // w = For writing
	
	$fdisplay( report, "--------------------------------------------");
	$fdisplay( report, "| Testbench results for module *half_adder* ");
	$fdisplay( report, "--------------------------------------------");
	$fdisplay( report, "" );
	
	$display( "Starting testbench... " );

	end




// Logic checking loop
integer i, j;
initial 
	begin

	for (i=0; i<=1; i=i+1) 
		begin

		for (j=0; j<=1; j=j+1) 
			begin

			input_a = i;
			input_b = j;

			#1;
			
			// A lookup table of the expected outputs depending on the signals
			// currently present at the DUT inputs
			if (input_a == 0 && input_b == 0)
				begin
				expected_sum   = 0;
				expected_carry = 0;
				end

			else if (input_a == 0 && input_b == 1)
				begin
				expected_sum   = 1;
				expected_carry = 0;
				end

			else if (input_a == 1 && input_b == 0) 
				begin
				expected_sum   = 1;
				expected_carry = 0;
				end

			else if (input_a == 1 && input_b == 1) 
				begin
				expected_sum   = 0;
				expected_carry = 1;
				end

			#1;

			// Compare expected to actual output and report discrepancies
			if ( (expected_sum != output_sum) || (expected_carry != output_carry) ) 
				begin
				// Increment fault counter
				fail_count = fail_count + 1;

				// Write error to screen
				$display( "ERROR: DUT output not expected at time %t", $time );
				$display( "Input A: %d", input_a );
				$display( "Input B: %d", input_b );
				$display( "Expected sum: %d. Actual sum: %d ", expected_sum, output_sum);
				$display( "Expected carry: %d. Actual carry: %d ", expected_carry, output_carry);

				// Write error to report file
				$fdisplay( report, "ERROR: DUT output not expected at time %t", $time );
				$fdisplay( report, "Input A: %d", input_a );
				$fdisplay( report, "Input B: %d", input_b );
				$fdisplay( report, "Expected sum: %d. Actual sum: %d ", expected_sum, output_sum);
				$fdisplay( report, "Expected carry: %d. Actual carry: %d ", expected_carry, output_carry);
				end
				
			test_count = test_count + 1;
			
			end
			
		end

	$display( "--------------------------------------------" );
	$display( "|                 RESULTS                   " );
	$display( "--------------------------------------------" );
	$display( "%d tests performed with %d errors", test_count, fail_count );
	
	$fdisplay( report, "--------------------------------------------" );
	$fdisplay( report, "|                 RESULTS                   " );
	$fdisplay( report, "--------------------------------------------" );
	$fdisplay( report, "%d tests performed", test_count );
	$fdisplay( report, "%d errors", fail_count );


	#1;

	$fclose(report);

	end

endmodule
