/******************************************************************************
 * Author:                  Alexander Hogen
 * Date Created:            08/18/2016
 * Last Modification Date:  09/18/2016
 * Project Name:            rom-creator
 * Filename:                main.cpp
 *
 * Overview:
 *       This program generates a SystemVerilog compatible
 *       ROM module. The ROM is initialized with the contents
 *       of a provided '.dat' file or with the contents of an
 *       array passed to the svROM object.
 *
 * Input:
 *       Gets input from user via the keyboard. Reads, checks,
 *       and imports a text file with ROM initialization data.
 *
 * Output:
 *       Displays prompts to user in terminal window. Creates
 *       (or overwrites) a '.sv' file in a user-specified
 *       directory.
 *
 *****************************************************************************/
#define ENABLE_LOG_MAIN

#include "../lib/logger.h"
#include "../lib/sv_rom.h"

#include <iostream>
using std::cout;
using std::endl;

int main()
{
  Logger log(true, LOG_INFO, true, LOG_DEBUG);

  // ROM with initialization file specified, same dir, no address offset
  svROM rom1("test_rom1", "rom.dat", ".", 0, &log);

  // ROM with initialization, out dir, and address offset specified
  //svROM rom2("test_rom2", "rom.dat", "./out", 256, &log);

  // Generate an arbitrary array of integers
  int ra[100] = {0};

  for (int i = 0; i < 100; i++)
  {
    ra[i] = i + i;
  }

  // Create ROM with the array as the ROM data
  //svROM rom3("test_rom3", ra, 100, "", 0, &log);
  //svROM romtest("name", ra, 100, ".", 0, &log);

  // ROM with array data as well as specified output dir and
  // address offset
  //svROM rom4("test_rom4", ra, 100, "out", 128, &log);

  // Export all ROM modules
  if (rom1.Export() //&&
      //rom2.Export() &&
      //rom3.Export() &&
      //rom4.Export()
  )
  {
    cout << "SUCCESS!" << endl;
  }
  else
  {
    cout << "FAIL!!" << endl;
  }

  return (0);
}
