/*!****************************************************************************
 * @file
 * @author   Alexander Hogen
 * @date     10/06/2016
 * @brief    Definitions of AdderDefStr() and methods of svAdder1B
 *
 *****************************************************************************/

#include "sv_adder1b.h"

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ios;
using std::ofstream;

//#include <cmath>

string AdderDefStr(TAdd type)
{
  string str("");

  str = "module ";

  // SV module name
  switch (type)
  {
  case FA:
    str += NAME_FULL_ADD;
    break;

  case HA:
    str += NAME_HALF_ADD;
    break;

  case FA1:
    str += NAME_FULL_ONE_ADD;
    break;

  default:
    str += "UNKNOWN_MODULE!!";
  }

  str += " ( a, b ";
  if (type == FA)
  {
    str += ", cin";
  }
  str += ", sum, cout );\n";

  str += "\tinput wire a; \n";
  str += "\tinput wire b; \n";
  if (type == FA)
  {
    str += "\tinput wire cin; \n";
  }
  str += "\toutput wire sum; \n";
  str += "\toutput wire cout;\n";

  //str += "\talways\n\t\tbegin \n";
  //str += "\t\tbegin \n";

  switch (type)
  {
  case FA:
    // str += "\t\t\tsum  = a ^ (b ^ cin); \n";
    // str += "\t\t\tcout = (a & b) | (a & cin) | (b & cin); \n";
    str += "\tassign sum  = a ^ (b ^ cin); \n";
    str += "\tassign cout = (a & b) | (a & cin) | (b & cin); \n";
    break;

  case HA:
    // str += "\t\t\tsum  = (a ^ b); \n";
    // str += "\t\t\tcout = (a & b); \n";
    str += "\tassign sum  = (a ^ b); \n";
    str += "\tassign cout = (a & b); \n";
    break;

  case FA1:
    // str += "\t\t\tsum  = a ^ (~b); \n";
    // str += "\t\t\tcout = (a & b) | a | b; \n";
    str += "\tassign sum  = a ^ (~b); \n";
    str += "\tassign cout = (a & b) | a | b; \n";
    break;

  default:
    str += "// UNKNOWN ADDER ARCHITECTURE";
  }

  //str += "\t\tend \n";
  str += "endmodule\n\n";

  return (str);
}

svAdder1B::svAdder1B() : m_name("adder_inst"),
                         m_type(FA),
                         m_a("none"),
                         m_b("none"),
                         m_c_in("none"),
                         m_sum("none"),
                         m_c_out("none")
{
}

svAdder1B::svAdder1B(
    const char *n,
    const TAdd type,
    const char *a,
    const char *b,
    const char *ci,
    const char *s,
    const char *co)
    : m_name("adder_inst"),
      m_type(type),
      m_a(a),
      m_b(b),
      m_c_in(ci),
      m_sum(s),
      m_c_out(co)
{
  // Make sure name string has characters and that the first
  // character *is* a letter, not a number or symbol.
  if (svStrCk(n))
  {
    m_name = n;
  }
}

string svAdder1B::InlineDataflow()
{
  string str("");

  switch (m_type)
  {
  // Inline dataflow logic for a full adder
  case FA:
    str = "// In-line FULL adder \n";
    str += "assign " + m_sum + " = " + m_a + " ^ (" + m_b + " ^ " + m_c_in + "); \n";
    str += "assign " + m_c_out + " = (" + m_a + " & " + m_b + ") | (" + m_a + " & " + m_c_in + ") | (" + m_b + " & " + m_c_in + "); \n";
    break;

  // Inline dataflow logic for a half-adder
  case HA:
    str = "// In-line HALF adder \n";
    str += "assign " + m_sum + " = (" + m_a + " ^ " + m_b + "); \n";
    str += "assign " + m_c_out + " = (" + m_a + " & " + m_b + "); \n";
    break;

  // Inline dataflow logic for a full adder with a constant 1 at the carry in input
  case FA1:
    str = "// In-line full adder with constant ONE at carry input\n";
    str += "assign " + m_sum + " = " + m_a + " ^ (~" + m_b + "); // A xor (not B) \n";
    str += "assign " + m_c_out + " = (" + m_a + " & " + m_b + ") | " + m_a + " | " + m_b + "; \n";
    break;

  default:
    str = "// ERROR: UNKNOWN ADDER ARCHITECTURE!!";
  }

  str += "\n";

  return (str);
}

string svAdder1B::InstantiateStr()
{
  string str("");

  // SV module name
  switch (m_type)
  {
  case FA:
    str = NAME_FULL_ADD;
    break;

  case HA:
    str = NAME_HALF_ADD;
    break;

  case FA1:
    str = NAME_FULL_ONE_ADD;
    break;

  default:
    str = "UNKNOWN_MODULE!!";
  }

  str += " " + m_name + " (\n\t";
  str += "// Inputs \n\t";
  str += ".a\t\t( " + m_a + " ), \n\t";
  str += ".b\t\t( " + m_b + " ), \n\t";

  if (m_type == FA)
  {
    str += ".cin\t\t( " + m_c_in + " ), \n\t";
  }
  str += "// Outputs \n\t";
  str += ".sum\t\t( " + m_sum + " ), \n\t";
  str += ".cout\t\t( " + m_c_out + " ) \n\t";
  str += ");\n\n";

  return (str);
}

bool svAdder1B::svStrCk(const string &s)
{
  if ((s != "") && isalpha(s.at(0)))
  {
    return (true);
  }
  else
  {
    return (false);
  }
}
