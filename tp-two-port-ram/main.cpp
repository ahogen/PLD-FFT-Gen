/******************************************************************************
 * Author:                  Alexander Hogen
 * Date Created:            09/18/2016
 * Last Modification Date:  09/18/2016
 * Project Name:            two-port-ram
 *
 * Overview:
 *       This program generates a SystemVerilog compatible
 *       RAM module.
 *
 * Input:
 *       Gets keyboard input from user via the terminal
 *       window.
 *
 * Output:
 *       Displays prompts to user in terminal window. Creates
 *       (or overwrites) a SystemVerilog RAM module in the
 *       user-specified output directory and with the specified
 *       module name.
 *
 *****************************************************************************/

#include <iostream>
using std::cout;
using std::endl;

#include "../common-src/sv_ram.h"

int main()
{
  svRAM ram1;
}
