`timescale 1ns / 1ns

///////////////////////////////////////////////////////////
//
// Author:  Alexander Hogen
// Created: 10/8/2016
// Updated: 10/15/2016
// Purpose: Verify the combinational logic of a multiplier
//          module with a specified bit width. You must
//          change the parameter below to match the
//          multiplier's bit width for the test to function
//          correctly.
//
///////////////////////////////////////////////////////////

module mult_tb ();

/**********************************************************
*                    SETUP PARAMETERS
**********************************************************/
// Set to equal the bit width of the DUT mutliplier
parameter BIT_WIDTH = 16;

/**********************************************************
*                    TESTBENCH SIGNALS
**********************************************************/
// (Subtract 1 from bit width to simplify definitions below)
localparam BW = BIT_WIDTH -1;



logic signed [BW:0] mand;
logic signed [BW:0] mier;
wire [(BIT_WIDTH * 2) - 1 : 0] product;
logic signed [(BIT_WIDTH * 2) - 1 : 0] DUT_product;

/**********************************************************
*                    INSTANTIATE DUT(s)
**********************************************************/
// multiplier DUT (
	// .X   ( mand[BW:0] ), 
	// .Y   ( mier[BW:0] ), 
	// .P ( product[(BIT_WIDTH * 2) - 1 : 0] )
// ); 

mult DUT (
	.a   ( mand[BW:0] ), 
	.b   ( mier[BW:0] ), 
	.out ( product[(BIT_WIDTH * 2) - 1 : 0] )
); 

/**********************************************************
*                     TESTBENCH LOGIC
**********************************************************/
logic signed [(BIT_WIDTH * 2) - 1 : 0] t_product;
logic [ (2 * BIT_WIDTH)-1 :0] error_cnt;
logic [ (2 * BIT_WIDTH)-1 :0] pass_cnt;

logic [BW:0]  MAX_VAL;

integer report;

// INITIAL BLOCk
initial begin
	$display( "Initializing... " );
	
	mand = 0;
	mier = 0;
	DUT_product = 0;
	t_product = 0;
	error_cnt = 0;
	pass_cnt = 0;
	MAX_VAL = 'h0;
	MAX_VAL = ~MAX_VAL;
	
	//$urandom(SEED);

	// Any other initialization code here
	
	$display( "Starting testbench... " );

	end


initial begin

	while ( (mand != MAX_VAL) || (mier != MAX_VAL) )
	begin
	
		{mand, mier} = {mand, mier} + 1;
		
		// Short pause
		#1;
		
		// Calculate actual product
		t_product = mand * mier;
				
		// Copy product coming out of DUT into the signed buffer
		DUT_product = product;
		
		// Short pause
		#1;

		$write( "%0d x %0d = %0d ? ", mand, mier, DUT_product);
		
		// Test output
		if (t_product != DUT_product) begin
		
			$display( ".... FALSE!");
			error_cnt = error_cnt + 1;
			end
			
		else begin
			$display( ".... true " );
			pass_cnt = pass_cnt + 1;
			end
	end
	

	$display( "===== RESULTS =====" );
	$display( "Tested a %0dx%0d multiplier", BIT_WIDTH, BIT_WIDTH);
	$display( "Multiplication tests passed: %0d", pass_cnt );
	$display( "Multiplication tests failed: %0d", error_cnt );
	$display( "Success rate: %0d%%", ((pass_cnt) / (pass_cnt + error_cnt))*100 );
	
	report = $fopen("mult_tb_REPORT.txt","w"); // a = Writing to end of file (append)
	$fdisplay( report, "Tested a %0dx%0d multiplier", BIT_WIDTH, BIT_WIDTH);
	$fdisplay( report, "Multiplication tests passed: %0d", pass_cnt );
	$fdisplay( report, "Multiplication tests failed: %0d", error_cnt);
	$fdisplay( report, "Success rate: %0d%%", ((pass_cnt) / (pass_cnt + error_cnt))*100 );
	$fdisplay( report, "=================================================");
	$fclose(report);
end

endmodule 