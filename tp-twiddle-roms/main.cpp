/*!****************************************************************************
 * @file
 * @author  Alexander Hogen
 *
 * @brief Genreate two Verilog ROM modules with twiddle factors
 *
 * This program generates two SystemVerilog compatible
 *       ROM modules, embedded with the real and imaginary
 *       twiddle factors required for a specified width and
 *       depth of an FFT core.
 *
 * Input:
 *       Gets keyboard input from user via the terminal window.
 *
 * Output:
 *       Displays prompts to user in terminal window. Creates
 *       (or overwrites) a 'twiddle_rom_real.sv' and
 *       'twiddle_rom_imag.sv" file in the specified directory.
 *
 *****************************************************************************/

#include <iostream>
using std::cout;
using std::endl;

#include "../lib/twiddle.h"

int main()
{
  cout << "Twiddle factors for FFT of length 64 and width 8:\t";
  twiddle t1(64, 8);
  cout << (t1.Export(".") ? "Success!" : "FAIL!!") << endl;

  cout << "Twiddle factors for FFT of length 256 and width 12:\t";
  twiddle t2(256, 12);
  cout << (t2.Export(".") ? "Success!" : "FAIL!!") << endl;

  cout << "Twiddle factors for FFT of length 1024 and width 16:\t";
  twiddle t3(1024, 16);
  cout << (t3.Export(".") ? "Success!" : "FAIL!!") << endl;

  cout << "Twiddle factors for FFT of length 4096 and width 30:\t";
  twiddle t4(4096, 30);
  cout << (t4.Export(".") ? "Success!" : "FAIL!!") << endl;

  return (0);
}
