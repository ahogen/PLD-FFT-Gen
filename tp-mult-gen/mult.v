/////////////////////////////////////////////////////////////////////////////////////
//
// Baugh-Wooley multiplier with High Performance Multiplier (HPM) reduction tree
// generator
//
// Author:    Alexander Hogen (https://bitbucket.org/ahogen/)
// Filename:  mult2.sv
// Generated: Sun Feb 17 10:36:27 2019
//
// Description:
//
//     This is a(n) 16 x 16 bit, single-cycle multiplier based off the work and research
//     by H. Eriksson, P. Larsson-Edefors, M. Sheeran, M. Sjalander, D. Johansson, and
//     M. Scholin.
//
// References:
//
//     [1] H. Eriksson, P. Larsson-Edefors, M. Sheeran, M. Sjalander, D. Johansson,
//         and M. Scholin, "Multiplier Reduction Tree with Logarithmic Logic Depth
//         and Regular Connectivity," in IEEE International Symposium on Circuits
//         and Systems, May 2006. Available: http://bit.ly/2dvN2u3
//
//     [2] M. Sjalander and P. Larsson-Edefors, "The Case for HPM-Based Baugh-Wooley
//         Multipliers," Department of Computer Science and Engineering, Chalmers
//         University of Technology, Tech. Rep. 08-8, March 2008.
//         Available: http://bit.ly/2dt7DPP
//
// DISCLAIMER:
//
// This code, in any raw, compiled, simulated, or synthesized form, is provided to
// you and/or the end-user "AS IS" without any warranties or support.
//
// There is no warranty for the code, to the extent permitted by applicable law.
// Except when otherwise stated in writing the copyright holders and/or other parties
// provide the code "AS IS" without warranty of any kind, either expressed or
// implied, including, but not limited to, the implied warranties of merchantability
// and fitness for a particular purpose. The entire risk as to the quality and
// performance of the code is with you. Should the code prove defective, you assume
// the cost of all necessary servicing, repair, or correction.
//
// In no event unless required by applicable law or agreed to in writing will any
// copyright holder, or any other party who modifies and/or conveys the code as
// permitted above, be liable to you for damages, including any general, special,
// incidental, or consequential damages arising out of the use or inability to use
// the code (including but not limited to loss of data or data being rendered
// inaccurate or losses sustained by you or third parties or a failure of the code
// to operate with any other programs), even if such holder or other party has been
// advised of the possibility of such damages.
//
// By using this code in any way, you agree to the above statements.
/////////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ns

module half_adder ( a, b , sum, cout );
	input wire a;
	input wire b;
	output wire sum;
	output wire cout;
	assign sum  = (a ^ b);
	assign cout = (a & b);
endmodule

module full_adder ( a, b , cin, sum, cout );
	input wire a;
	input wire b;
	input wire cin;
	output wire sum;
	output wire cout;
	assign sum  = a ^ (b ^ cin);
	assign cout = (a & b) | (a & cin) | (b & cin);
endmodule

module full_one_adder ( a, b , sum, cout );
	input wire a;
	input wire b;
	output wire sum;
	output wire cout;
	assign sum  = a ^ (~b);
	assign cout = (a & b) | a | b;
endmodule

// Definition of final adder architecture
module ripple_carry_adder (

	input wire [29:0] a,
	input wire [29:0] b,
	output wire [30:0] out
);

parameter BW = 30 -1;


wire [BW:0] carry;
wire [BW:0] sum;

assign out = { carry[BW], sum[BW:0] };


// Instantiate a single half adder module, for the first adder in the chain.
half_adder HA (
    .a    ( a[0] ),
    .b    ( b[0] ),
    .sum  ( sum[0] ),
    .cout ( carry[0] )
    );


// Use a generate block to automatically create full adders to perform the rest
// of the additions.
genvar i;
generate

    for ( i = 1; i <= BW; i = i + 1) begin: FullAdderGenerate

        full_adder FA (
            .a    ( a[i] ),
            .b    ( b[i] ),
            .cin  ( carry[i-1] ),
            .sum  ( sum[i] ),
            .cout ( carry[i] )
            );

end

endgenerate

endmodule


module mult (
	input wire [15:0] a,
	input wire [15:0] b,
	output wire [31:0] out
);

// Partial product wires
wire p0_0;
wire p0_1;
wire p0_2;
wire p0_3;
wire p0_4;
wire p0_5;
wire p0_6;
wire p0_7;
wire p0_8;
wire p0_9;
wire p0_10;
wire p0_11;
wire p0_12;
wire p0_13;
wire p0_14;
wire p0_15;
wire p1_0;
wire p1_1;
wire p1_2;
wire p1_3;
wire p1_4;
wire p1_5;
wire p1_6;
wire p1_7;
wire p1_8;
wire p1_9;
wire p1_10;
wire p1_11;
wire p1_12;
wire p1_13;
wire p1_14;
wire p1_15;
wire p2_0;
wire p2_1;
wire p2_2;
wire p2_3;
wire p2_4;
wire p2_5;
wire p2_6;
wire p2_7;
wire p2_8;
wire p2_9;
wire p2_10;
wire p2_11;
wire p2_12;
wire p2_13;
wire p2_14;
wire p2_15;
wire p3_0;
wire p3_1;
wire p3_2;
wire p3_3;
wire p3_4;
wire p3_5;
wire p3_6;
wire p3_7;
wire p3_8;
wire p3_9;
wire p3_10;
wire p3_11;
wire p3_12;
wire p3_13;
wire p3_14;
wire p3_15;
wire p4_0;
wire p4_1;
wire p4_2;
wire p4_3;
wire p4_4;
wire p4_5;
wire p4_6;
wire p4_7;
wire p4_8;
wire p4_9;
wire p4_10;
wire p4_11;
wire p4_12;
wire p4_13;
wire p4_14;
wire p4_15;
wire p5_0;
wire p5_1;
wire p5_2;
wire p5_3;
wire p5_4;
wire p5_5;
wire p5_6;
wire p5_7;
wire p5_8;
wire p5_9;
wire p5_10;
wire p5_11;
wire p5_12;
wire p5_13;
wire p5_14;
wire p5_15;
wire p6_0;
wire p6_1;
wire p6_2;
wire p6_3;
wire p6_4;
wire p6_5;
wire p6_6;
wire p6_7;
wire p6_8;
wire p6_9;
wire p6_10;
wire p6_11;
wire p6_12;
wire p6_13;
wire p6_14;
wire p6_15;
wire p7_0;
wire p7_1;
wire p7_2;
wire p7_3;
wire p7_4;
wire p7_5;
wire p7_6;
wire p7_7;
wire p7_8;
wire p7_9;
wire p7_10;
wire p7_11;
wire p7_12;
wire p7_13;
wire p7_14;
wire p7_15;
wire p8_0;
wire p8_1;
wire p8_2;
wire p8_3;
wire p8_4;
wire p8_5;
wire p8_6;
wire p8_7;
wire p8_8;
wire p8_9;
wire p8_10;
wire p8_11;
wire p8_12;
wire p8_13;
wire p8_14;
wire p8_15;
wire p9_0;
wire p9_1;
wire p9_2;
wire p9_3;
wire p9_4;
wire p9_5;
wire p9_6;
wire p9_7;
wire p9_8;
wire p9_9;
wire p9_10;
wire p9_11;
wire p9_12;
wire p9_13;
wire p9_14;
wire p9_15;
wire p10_0;
wire p10_1;
wire p10_2;
wire p10_3;
wire p10_4;
wire p10_5;
wire p10_6;
wire p10_7;
wire p10_8;
wire p10_9;
wire p10_10;
wire p10_11;
wire p10_12;
wire p10_13;
wire p10_14;
wire p10_15;
wire p11_0;
wire p11_1;
wire p11_2;
wire p11_3;
wire p11_4;
wire p11_5;
wire p11_6;
wire p11_7;
wire p11_8;
wire p11_9;
wire p11_10;
wire p11_11;
wire p11_12;
wire p11_13;
wire p11_14;
wire p11_15;
wire p12_0;
wire p12_1;
wire p12_2;
wire p12_3;
wire p12_4;
wire p12_5;
wire p12_6;
wire p12_7;
wire p12_8;
wire p12_9;
wire p12_10;
wire p12_11;
wire p12_12;
wire p12_13;
wire p12_14;
wire p12_15;
wire p13_0;
wire p13_1;
wire p13_2;
wire p13_3;
wire p13_4;
wire p13_5;
wire p13_6;
wire p13_7;
wire p13_8;
wire p13_9;
wire p13_10;
wire p13_11;
wire p13_12;
wire p13_13;
wire p13_14;
wire p13_15;
wire p14_0;
wire p14_1;
wire p14_2;
wire p14_3;
wire p14_4;
wire p14_5;
wire p14_6;
wire p14_7;
wire p14_8;
wire p14_9;
wire p14_10;
wire p14_11;
wire p14_12;
wire p14_13;
wire p14_14;
wire p14_15;
wire p15_0;
wire p15_1;
wire p15_2;
wire p15_3;
wire p15_4;
wire p15_5;
wire p15_6;
wire p15_7;
wire p15_8;
wire p15_9;
wire p15_10;
wire p15_11;
wire p15_12;
wire p15_13;
wire p15_14;
wire p15_15;


// Partial product term generation
assign p0_0 =   ( b[0] & a[0] );
assign p0_1 =   ( b[0] & a[1] );
assign p0_2 =   ( b[0] & a[2] );
assign p0_3 =   ( b[0] & a[3] );
assign p0_4 =   ( b[0] & a[4] );
assign p0_5 =   ( b[0] & a[5] );
assign p0_6 =   ( b[0] & a[6] );
assign p0_7 =   ( b[0] & a[7] );
assign p0_8 =   ( b[0] & a[8] );
assign p0_9 =   ( b[0] & a[9] );
assign p0_10 =   ( b[0] & a[10] );
assign p0_11 =   ( b[0] & a[11] );
assign p0_12 =   ( b[0] & a[12] );
assign p0_13 =   ( b[0] & a[13] );
assign p0_14 =   ( b[0] & a[14] );
assign p0_15 =  ~( b[0] & a[15] );
assign p1_0 =   ( b[1] & a[0] );
assign p1_1 =   ( b[1] & a[1] );
assign p1_2 =   ( b[1] & a[2] );
assign p1_3 =   ( b[1] & a[3] );
assign p1_4 =   ( b[1] & a[4] );
assign p1_5 =   ( b[1] & a[5] );
assign p1_6 =   ( b[1] & a[6] );
assign p1_7 =   ( b[1] & a[7] );
assign p1_8 =   ( b[1] & a[8] );
assign p1_9 =   ( b[1] & a[9] );
assign p1_10 =   ( b[1] & a[10] );
assign p1_11 =   ( b[1] & a[11] );
assign p1_12 =   ( b[1] & a[12] );
assign p1_13 =   ( b[1] & a[13] );
assign p1_14 =   ( b[1] & a[14] );
assign p1_15 =  ~( b[1] & a[15] );
assign p2_0 =   ( b[2] & a[0] );
assign p2_1 =   ( b[2] & a[1] );
assign p2_2 =   ( b[2] & a[2] );
assign p2_3 =   ( b[2] & a[3] );
assign p2_4 =   ( b[2] & a[4] );
assign p2_5 =   ( b[2] & a[5] );
assign p2_6 =   ( b[2] & a[6] );
assign p2_7 =   ( b[2] & a[7] );
assign p2_8 =   ( b[2] & a[8] );
assign p2_9 =   ( b[2] & a[9] );
assign p2_10 =   ( b[2] & a[10] );
assign p2_11 =   ( b[2] & a[11] );
assign p2_12 =   ( b[2] & a[12] );
assign p2_13 =   ( b[2] & a[13] );
assign p2_14 =   ( b[2] & a[14] );
assign p2_15 =  ~( b[2] & a[15] );
assign p3_0 =   ( b[3] & a[0] );
assign p3_1 =   ( b[3] & a[1] );
assign p3_2 =   ( b[3] & a[2] );
assign p3_3 =   ( b[3] & a[3] );
assign p3_4 =   ( b[3] & a[4] );
assign p3_5 =   ( b[3] & a[5] );
assign p3_6 =   ( b[3] & a[6] );
assign p3_7 =   ( b[3] & a[7] );
assign p3_8 =   ( b[3] & a[8] );
assign p3_9 =   ( b[3] & a[9] );
assign p3_10 =   ( b[3] & a[10] );
assign p3_11 =   ( b[3] & a[11] );
assign p3_12 =   ( b[3] & a[12] );
assign p3_13 =   ( b[3] & a[13] );
assign p3_14 =   ( b[3] & a[14] );
assign p3_15 =  ~( b[3] & a[15] );
assign p4_0 =   ( b[4] & a[0] );
assign p4_1 =   ( b[4] & a[1] );
assign p4_2 =   ( b[4] & a[2] );
assign p4_3 =   ( b[4] & a[3] );
assign p4_4 =   ( b[4] & a[4] );
assign p4_5 =   ( b[4] & a[5] );
assign p4_6 =   ( b[4] & a[6] );
assign p4_7 =   ( b[4] & a[7] );
assign p4_8 =   ( b[4] & a[8] );
assign p4_9 =   ( b[4] & a[9] );
assign p4_10 =   ( b[4] & a[10] );
assign p4_11 =   ( b[4] & a[11] );
assign p4_12 =   ( b[4] & a[12] );
assign p4_13 =   ( b[4] & a[13] );
assign p4_14 =   ( b[4] & a[14] );
assign p4_15 =  ~( b[4] & a[15] );
assign p5_0 =   ( b[5] & a[0] );
assign p5_1 =   ( b[5] & a[1] );
assign p5_2 =   ( b[5] & a[2] );
assign p5_3 =   ( b[5] & a[3] );
assign p5_4 =   ( b[5] & a[4] );
assign p5_5 =   ( b[5] & a[5] );
assign p5_6 =   ( b[5] & a[6] );
assign p5_7 =   ( b[5] & a[7] );
assign p5_8 =   ( b[5] & a[8] );
assign p5_9 =   ( b[5] & a[9] );
assign p5_10 =   ( b[5] & a[10] );
assign p5_11 =   ( b[5] & a[11] );
assign p5_12 =   ( b[5] & a[12] );
assign p5_13 =   ( b[5] & a[13] );
assign p5_14 =   ( b[5] & a[14] );
assign p5_15 =  ~( b[5] & a[15] );
assign p6_0 =   ( b[6] & a[0] );
assign p6_1 =   ( b[6] & a[1] );
assign p6_2 =   ( b[6] & a[2] );
assign p6_3 =   ( b[6] & a[3] );
assign p6_4 =   ( b[6] & a[4] );
assign p6_5 =   ( b[6] & a[5] );
assign p6_6 =   ( b[6] & a[6] );
assign p6_7 =   ( b[6] & a[7] );
assign p6_8 =   ( b[6] & a[8] );
assign p6_9 =   ( b[6] & a[9] );
assign p6_10 =   ( b[6] & a[10] );
assign p6_11 =   ( b[6] & a[11] );
assign p6_12 =   ( b[6] & a[12] );
assign p6_13 =   ( b[6] & a[13] );
assign p6_14 =   ( b[6] & a[14] );
assign p6_15 =  ~( b[6] & a[15] );
assign p7_0 =   ( b[7] & a[0] );
assign p7_1 =   ( b[7] & a[1] );
assign p7_2 =   ( b[7] & a[2] );
assign p7_3 =   ( b[7] & a[3] );
assign p7_4 =   ( b[7] & a[4] );
assign p7_5 =   ( b[7] & a[5] );
assign p7_6 =   ( b[7] & a[6] );
assign p7_7 =   ( b[7] & a[7] );
assign p7_8 =   ( b[7] & a[8] );
assign p7_9 =   ( b[7] & a[9] );
assign p7_10 =   ( b[7] & a[10] );
assign p7_11 =   ( b[7] & a[11] );
assign p7_12 =   ( b[7] & a[12] );
assign p7_13 =   ( b[7] & a[13] );
assign p7_14 =   ( b[7] & a[14] );
assign p7_15 =  ~( b[7] & a[15] );
assign p8_0 =   ( b[8] & a[0] );
assign p8_1 =   ( b[8] & a[1] );
assign p8_2 =   ( b[8] & a[2] );
assign p8_3 =   ( b[8] & a[3] );
assign p8_4 =   ( b[8] & a[4] );
assign p8_5 =   ( b[8] & a[5] );
assign p8_6 =   ( b[8] & a[6] );
assign p8_7 =   ( b[8] & a[7] );
assign p8_8 =   ( b[8] & a[8] );
assign p8_9 =   ( b[8] & a[9] );
assign p8_10 =   ( b[8] & a[10] );
assign p8_11 =   ( b[8] & a[11] );
assign p8_12 =   ( b[8] & a[12] );
assign p8_13 =   ( b[8] & a[13] );
assign p8_14 =   ( b[8] & a[14] );
assign p8_15 =  ~( b[8] & a[15] );
assign p9_0 =   ( b[9] & a[0] );
assign p9_1 =   ( b[9] & a[1] );
assign p9_2 =   ( b[9] & a[2] );
assign p9_3 =   ( b[9] & a[3] );
assign p9_4 =   ( b[9] & a[4] );
assign p9_5 =   ( b[9] & a[5] );
assign p9_6 =   ( b[9] & a[6] );
assign p9_7 =   ( b[9] & a[7] );
assign p9_8 =   ( b[9] & a[8] );
assign p9_9 =   ( b[9] & a[9] );
assign p9_10 =   ( b[9] & a[10] );
assign p9_11 =   ( b[9] & a[11] );
assign p9_12 =   ( b[9] & a[12] );
assign p9_13 =   ( b[9] & a[13] );
assign p9_14 =   ( b[9] & a[14] );
assign p9_15 =  ~( b[9] & a[15] );
assign p10_0 =   ( b[10] & a[0] );
assign p10_1 =   ( b[10] & a[1] );
assign p10_2 =   ( b[10] & a[2] );
assign p10_3 =   ( b[10] & a[3] );
assign p10_4 =   ( b[10] & a[4] );
assign p10_5 =   ( b[10] & a[5] );
assign p10_6 =   ( b[10] & a[6] );
assign p10_7 =   ( b[10] & a[7] );
assign p10_8 =   ( b[10] & a[8] );
assign p10_9 =   ( b[10] & a[9] );
assign p10_10 =   ( b[10] & a[10] );
assign p10_11 =   ( b[10] & a[11] );
assign p10_12 =   ( b[10] & a[12] );
assign p10_13 =   ( b[10] & a[13] );
assign p10_14 =   ( b[10] & a[14] );
assign p10_15 =  ~( b[10] & a[15] );
assign p11_0 =   ( b[11] & a[0] );
assign p11_1 =   ( b[11] & a[1] );
assign p11_2 =   ( b[11] & a[2] );
assign p11_3 =   ( b[11] & a[3] );
assign p11_4 =   ( b[11] & a[4] );
assign p11_5 =   ( b[11] & a[5] );
assign p11_6 =   ( b[11] & a[6] );
assign p11_7 =   ( b[11] & a[7] );
assign p11_8 =   ( b[11] & a[8] );
assign p11_9 =   ( b[11] & a[9] );
assign p11_10 =   ( b[11] & a[10] );
assign p11_11 =   ( b[11] & a[11] );
assign p11_12 =   ( b[11] & a[12] );
assign p11_13 =   ( b[11] & a[13] );
assign p11_14 =   ( b[11] & a[14] );
assign p11_15 =  ~( b[11] & a[15] );
assign p12_0 =   ( b[12] & a[0] );
assign p12_1 =   ( b[12] & a[1] );
assign p12_2 =   ( b[12] & a[2] );
assign p12_3 =   ( b[12] & a[3] );
assign p12_4 =   ( b[12] & a[4] );
assign p12_5 =   ( b[12] & a[5] );
assign p12_6 =   ( b[12] & a[6] );
assign p12_7 =   ( b[12] & a[7] );
assign p12_8 =   ( b[12] & a[8] );
assign p12_9 =   ( b[12] & a[9] );
assign p12_10 =   ( b[12] & a[10] );
assign p12_11 =   ( b[12] & a[11] );
assign p12_12 =   ( b[12] & a[12] );
assign p12_13 =   ( b[12] & a[13] );
assign p12_14 =   ( b[12] & a[14] );
assign p12_15 =  ~( b[12] & a[15] );
assign p13_0 =   ( b[13] & a[0] );
assign p13_1 =   ( b[13] & a[1] );
assign p13_2 =   ( b[13] & a[2] );
assign p13_3 =   ( b[13] & a[3] );
assign p13_4 =   ( b[13] & a[4] );
assign p13_5 =   ( b[13] & a[5] );
assign p13_6 =   ( b[13] & a[6] );
assign p13_7 =   ( b[13] & a[7] );
assign p13_8 =   ( b[13] & a[8] );
assign p13_9 =   ( b[13] & a[9] );
assign p13_10 =   ( b[13] & a[10] );
assign p13_11 =   ( b[13] & a[11] );
assign p13_12 =   ( b[13] & a[12] );
assign p13_13 =   ( b[13] & a[13] );
assign p13_14 =   ( b[13] & a[14] );
assign p13_15 =  ~( b[13] & a[15] );
assign p14_0 =   ( b[14] & a[0] );
assign p14_1 =   ( b[14] & a[1] );
assign p14_2 =   ( b[14] & a[2] );
assign p14_3 =   ( b[14] & a[3] );
assign p14_4 =   ( b[14] & a[4] );
assign p14_5 =   ( b[14] & a[5] );
assign p14_6 =   ( b[14] & a[6] );
assign p14_7 =   ( b[14] & a[7] );
assign p14_8 =   ( b[14] & a[8] );
assign p14_9 =   ( b[14] & a[9] );
assign p14_10 =   ( b[14] & a[10] );
assign p14_11 =   ( b[14] & a[11] );
assign p14_12 =   ( b[14] & a[12] );
assign p14_13 =   ( b[14] & a[13] );
assign p14_14 =   ( b[14] & a[14] );
assign p14_15 =  ~( b[14] & a[15] );
assign p15_0 =  ~( b[15] & a[0] );
assign p15_1 =  ~( b[15] & a[1] );
assign p15_2 =  ~( b[15] & a[2] );
assign p15_3 =  ~( b[15] & a[3] );
assign p15_4 =  ~( b[15] & a[4] );
assign p15_5 =  ~( b[15] & a[5] );
assign p15_6 =  ~( b[15] & a[6] );
assign p15_7 =  ~( b[15] & a[7] );
assign p15_8 =  ~( b[15] & a[8] );
assign p15_9 =  ~( b[15] & a[9] );
assign p15_10 =  ~( b[15] & a[10] );
assign p15_11 =  ~( b[15] & a[11] );
assign p15_12 =  ~( b[15] & a[12] );
assign p15_13 =  ~( b[15] & a[13] );
assign p15_14 =  ~( b[15] & a[14] );
assign p15_15 =   ( b[15] & a[15] );


// Output wires of 1-bit adders in HPM tree
wire s14_15;
wire c14_15;
wire s14_16;
wire c14_16;
wire s13_14;
wire c13_14;
wire s13_15;
wire c13_15;
wire s13_16;
wire c13_16;
wire s13_17;
wire c13_17;
wire s12_13;
wire c12_13;
wire s12_14;
wire c12_14;
wire s12_15;
wire c12_15;
wire s12_16;
wire c12_16;
wire s12_17;
wire c12_17;
wire s12_18;
wire c12_18;
wire s11_12;
wire c11_12;
wire s11_13;
wire c11_13;
wire s11_14;
wire c11_14;
wire s11_15;
wire c11_15;
wire s11_16;
wire c11_16;
wire s11_17;
wire c11_17;
wire s11_18;
wire c11_18;
wire s11_19;
wire c11_19;
wire s10_11;
wire c10_11;
wire s10_12;
wire c10_12;
wire s10_13;
wire c10_13;
wire s10_14;
wire c10_14;
wire s10_15;
wire c10_15;
wire s10_16;
wire c10_16;
wire s10_17;
wire c10_17;
wire s10_18;
wire c10_18;
wire s10_19;
wire c10_19;
wire s10_20;
wire c10_20;
wire s9_10;
wire c9_10;
wire s9_11;
wire c9_11;
wire s9_12;
wire c9_12;
wire s9_13;
wire c9_13;
wire s9_14;
wire c9_14;
wire s9_15;
wire c9_15;
wire s9_16;
wire c9_16;
wire s9_17;
wire c9_17;
wire s9_18;
wire c9_18;
wire s9_19;
wire c9_19;
wire s9_20;
wire c9_20;
wire s9_21;
wire c9_21;
wire s8_9;
wire c8_9;
wire s8_10;
wire c8_10;
wire s8_11;
wire c8_11;
wire s8_12;
wire c8_12;
wire s8_13;
wire c8_13;
wire s8_14;
wire c8_14;
wire s8_15;
wire c8_15;
wire s8_16;
wire c8_16;
wire s8_17;
wire c8_17;
wire s8_18;
wire c8_18;
wire s8_19;
wire c8_19;
wire s8_20;
wire c8_20;
wire s8_21;
wire c8_21;
wire s8_22;
wire c8_22;
wire s7_8;
wire c7_8;
wire s7_9;
wire c7_9;
wire s7_10;
wire c7_10;
wire s7_11;
wire c7_11;
wire s7_12;
wire c7_12;
wire s7_13;
wire c7_13;
wire s7_14;
wire c7_14;
wire s7_15;
wire c7_15;
wire s7_16;
wire c7_16;
wire s7_17;
wire c7_17;
wire s7_18;
wire c7_18;
wire s7_19;
wire c7_19;
wire s7_20;
wire c7_20;
wire s7_21;
wire c7_21;
wire s7_22;
wire c7_22;
wire s7_23;
wire c7_23;
wire s6_7;
wire c6_7;
wire s6_8;
wire c6_8;
wire s6_9;
wire c6_9;
wire s6_10;
wire c6_10;
wire s6_11;
wire c6_11;
wire s6_12;
wire c6_12;
wire s6_13;
wire c6_13;
wire s6_14;
wire c6_14;
wire s6_15;
wire c6_15;
wire s6_16;
wire c6_16;
wire s6_17;
wire c6_17;
wire s6_18;
wire c6_18;
wire s6_19;
wire c6_19;
wire s6_20;
wire c6_20;
wire s6_21;
wire c6_21;
wire s6_22;
wire c6_22;
wire s6_23;
wire c6_23;
wire s6_24;
wire c6_24;
wire s5_6;
wire c5_6;
wire s5_7;
wire c5_7;
wire s5_8;
wire c5_8;
wire s5_9;
wire c5_9;
wire s5_10;
wire c5_10;
wire s5_11;
wire c5_11;
wire s5_12;
wire c5_12;
wire s5_13;
wire c5_13;
wire s5_14;
wire c5_14;
wire s5_15;
wire c5_15;
wire s5_16;
wire c5_16;
wire s5_17;
wire c5_17;
wire s5_18;
wire c5_18;
wire s5_19;
wire c5_19;
wire s5_20;
wire c5_20;
wire s5_21;
wire c5_21;
wire s5_22;
wire c5_22;
wire s5_23;
wire c5_23;
wire s5_24;
wire c5_24;
wire s5_25;
wire c5_25;
wire s4_5;
wire c4_5;
wire s4_6;
wire c4_6;
wire s4_7;
wire c4_7;
wire s4_8;
wire c4_8;
wire s4_9;
wire c4_9;
wire s4_10;
wire c4_10;
wire s4_11;
wire c4_11;
wire s4_12;
wire c4_12;
wire s4_13;
wire c4_13;
wire s4_14;
wire c4_14;
wire s4_15;
wire c4_15;
wire s4_16;
wire c4_16;
wire s4_17;
wire c4_17;
wire s4_18;
wire c4_18;
wire s4_19;
wire c4_19;
wire s4_20;
wire c4_20;
wire s4_21;
wire c4_21;
wire s4_22;
wire c4_22;
wire s4_23;
wire c4_23;
wire s4_24;
wire c4_24;
wire s4_25;
wire c4_25;
wire s4_26;
wire c4_26;
wire s3_4;
wire c3_4;
wire s3_5;
wire c3_5;
wire s3_6;
wire c3_6;
wire s3_7;
wire c3_7;
wire s3_8;
wire c3_8;
wire s3_9;
wire c3_9;
wire s3_10;
wire c3_10;
wire s3_11;
wire c3_11;
wire s3_12;
wire c3_12;
wire s3_13;
wire c3_13;
wire s3_14;
wire c3_14;
wire s3_15;
wire c3_15;
wire s3_16;
wire c3_16;
wire s3_17;
wire c3_17;
wire s3_18;
wire c3_18;
wire s3_19;
wire c3_19;
wire s3_20;
wire c3_20;
wire s3_21;
wire c3_21;
wire s3_22;
wire c3_22;
wire s3_23;
wire c3_23;
wire s3_24;
wire c3_24;
wire s3_25;
wire c3_25;
wire s3_26;
wire c3_26;
wire s3_27;
wire c3_27;
wire s2_3;
wire c2_3;
wire s2_4;
wire c2_4;
wire s2_5;
wire c2_5;
wire s2_6;
wire c2_6;
wire s2_7;
wire c2_7;
wire s2_8;
wire c2_8;
wire s2_9;
wire c2_9;
wire s2_10;
wire c2_10;
wire s2_11;
wire c2_11;
wire s2_12;
wire c2_12;
wire s2_13;
wire c2_13;
wire s2_14;
wire c2_14;
wire s2_15;
wire c2_15;
wire s2_16;
wire c2_16;
wire s2_17;
wire c2_17;
wire s2_18;
wire c2_18;
wire s2_19;
wire c2_19;
wire s2_20;
wire c2_20;
wire s2_21;
wire c2_21;
wire s2_22;
wire c2_22;
wire s2_23;
wire c2_23;
wire s2_24;
wire c2_24;
wire s2_25;
wire c2_25;
wire s2_26;
wire c2_26;
wire s2_27;
wire c2_27;
wire s2_28;
wire c2_28;
wire s1_2;
wire c1_2;
wire s1_3;
wire c1_3;
wire s1_4;
wire c1_4;
wire s1_5;
wire c1_5;
wire s1_6;
wire c1_6;
wire s1_7;
wire c1_7;
wire s1_8;
wire c1_8;
wire s1_9;
wire c1_9;
wire s1_10;
wire c1_10;
wire s1_11;
wire c1_11;
wire s1_12;
wire c1_12;
wire s1_13;
wire c1_13;
wire s1_14;
wire c1_14;
wire s1_15;
wire c1_15;
wire s1_16;
wire c1_16;
wire s1_17;
wire c1_17;
wire s1_18;
wire c1_18;
wire s1_19;
wire c1_19;
wire s1_20;
wire c1_20;
wire s1_21;
wire c1_21;
wire s1_22;
wire c1_22;
wire s1_23;
wire c1_23;
wire s1_24;
wire c1_24;
wire s1_25;
wire c1_25;
wire s1_26;
wire c1_26;
wire s1_27;
wire c1_27;
wire s1_28;
wire c1_28;
wire s1_29;
wire c1_29;
// Instantiate 1-bit adders in HPM tree
half_adder HA14_15 (
	// Inputs
	.a		( p15_0 ),
	.b		( p13_2 ),
	// Outputs
	.sum		( s14_15 ),
	.cout		( c14_15 )
	);

full_one_adder FAone14_16 (
	// Inputs
	.a		( p15_1 ),
	.b		( p13_3 ),
	// Outputs
	.sum		( s14_16 ),
	.cout		( c14_16 )
	);

half_adder HA13_14 (
	// Inputs
	.a		( p14_0 ),
	.b		( p12_2 ),
	// Outputs
	.sum		( s13_14 ),
	.cout		( c13_14 )
	);

full_adder FA13_15 (
	// Inputs
	.a		( p14_1 ),
	.b		( p12_3 ),
	.cin		( p11_4 ),
	// Outputs
	.sum		( s13_15 ),
	.cout		( c13_15 )
	);

full_adder FA13_16 (
	// Inputs
	.a		( p14_2 ),
	.b		( p12_4 ),
	.cin		( p11_5 ),
	// Outputs
	.sum		( s13_16 ),
	.cout		( c13_16 )
	);

full_adder FA13_17 (
	// Inputs
	.a		( p15_2 ),
	.b		( p14_3 ),
	.cin		( p13_4 ),
	// Outputs
	.sum		( s13_17 ),
	.cout		( c13_17 )
	);

half_adder HA12_13 (
	// Inputs
	.a		( p13_0 ),
	.b		( p11_2 ),
	// Outputs
	.sum		( s12_13 ),
	.cout		( c12_13 )
	);

full_adder FA12_14 (
	// Inputs
	.a		( p13_1 ),
	.b		( p11_3 ),
	.cin		( p10_4 ),
	// Outputs
	.sum		( s12_14 ),
	.cout		( c12_14 )
	);

full_adder FA12_15 (
	// Inputs
	.a		( p10_5 ),
	.b		( p9_6 ),
	.cin		( p8_7 ),
	// Outputs
	.sum		( s12_15 ),
	.cout		( c12_15 )
	);

full_adder FA12_16 (
	// Inputs
	.a		( p10_6 ),
	.b		( p9_7 ),
	.cin		( p8_8 ),
	// Outputs
	.sum		( s12_16 ),
	.cout		( c12_16 )
	);

full_adder FA12_17 (
	// Inputs
	.a		( p12_5 ),
	.b		( p11_6 ),
	.cin		( p10_7 ),
	// Outputs
	.sum		( s12_17 ),
	.cout		( c12_17 )
	);

full_adder FA12_18 (
	// Inputs
	.a		( p15_3 ),
	.b		( p14_4 ),
	.cin		( p13_5 ),
	// Outputs
	.sum		( s12_18 ),
	.cout		( c12_18 )
	);

half_adder HA11_12 (
	// Inputs
	.a		( p12_0 ),
	.b		( p10_2 ),
	// Outputs
	.sum		( s11_12 ),
	.cout		( c11_12 )
	);

full_adder FA11_13 (
	// Inputs
	.a		( p12_1 ),
	.b		( p10_3 ),
	.cin		( p9_4 ),
	// Outputs
	.sum		( s11_13 ),
	.cout		( c11_13 )
	);

full_adder FA11_14 (
	// Inputs
	.a		( p9_5 ),
	.b		( p8_6 ),
	.cin		( p7_7 ),
	// Outputs
	.sum		( s11_14 ),
	.cout		( c11_14 )
	);

full_adder FA11_15 (
	// Inputs
	.a		( p7_8 ),
	.b		( p6_9 ),
	.cin		( p5_10 ),
	// Outputs
	.sum		( s11_15 ),
	.cout		( c11_15 )
	);

full_adder FA11_16 (
	// Inputs
	.a		( p7_9 ),
	.b		( p6_10 ),
	.cin		( p5_11 ),
	// Outputs
	.sum		( s11_16 ),
	.cout		( c11_16 )
	);

full_adder FA11_17 (
	// Inputs
	.a		( p9_8 ),
	.b		( p8_9 ),
	.cin		( p7_10 ),
	// Outputs
	.sum		( s11_17 ),
	.cout		( c11_17 )
	);

full_adder FA11_18 (
	// Inputs
	.a		( p12_6 ),
	.b		( p11_7 ),
	.cin		( p10_8 ),
	// Outputs
	.sum		( s11_18 ),
	.cout		( c11_18 )
	);

full_adder FA11_19 (
	// Inputs
	.a		( p15_4 ),
	.b		( p14_5 ),
	.cin		( p13_6 ),
	// Outputs
	.sum		( s11_19 ),
	.cout		( c11_19 )
	);

half_adder HA10_11 (
	// Inputs
	.a		( p11_0 ),
	.b		( p9_2 ),
	// Outputs
	.sum		( s10_11 ),
	.cout		( c10_11 )
	);

full_adder FA10_12 (
	// Inputs
	.a		( p11_1 ),
	.b		( p9_3 ),
	.cin		( p8_4 ),
	// Outputs
	.sum		( s10_12 ),
	.cout		( c10_12 )
	);

full_adder FA10_13 (
	// Inputs
	.a		( p8_5 ),
	.b		( p7_6 ),
	.cin		( p6_7 ),
	// Outputs
	.sum		( s10_13 ),
	.cout		( c10_13 )
	);

full_adder FA10_14 (
	// Inputs
	.a		( p6_8 ),
	.b		( p5_9 ),
	.cin		( p4_10 ),
	// Outputs
	.sum		( s10_14 ),
	.cout		( c10_14 )
	);

full_adder FA10_15 (
	// Inputs
	.a		( p4_11 ),
	.b		( p3_12 ),
	.cin		( p2_13 ),
	// Outputs
	.sum		( s10_15 ),
	.cout		( c10_15 )
	);

full_adder FA10_16 (
	// Inputs
	.a		( p4_12 ),
	.b		( p3_13 ),
	.cin		( p2_14 ),
	// Outputs
	.sum		( s10_16 ),
	.cout		( c10_16 )
	);

full_adder FA10_17 (
	// Inputs
	.a		( p6_11 ),
	.b		( p5_12 ),
	.cin		( p4_13 ),
	// Outputs
	.sum		( s10_17 ),
	.cout		( c10_17 )
	);

full_adder FA10_18 (
	// Inputs
	.a		( p9_9 ),
	.b		( p8_10 ),
	.cin		( p7_11 ),
	// Outputs
	.sum		( s10_18 ),
	.cout		( c10_18 )
	);

full_adder FA10_19 (
	// Inputs
	.a		( p12_7 ),
	.b		( p11_8 ),
	.cin		( p10_9 ),
	// Outputs
	.sum		( s10_19 ),
	.cout		( c10_19 )
	);

full_adder FA10_20 (
	// Inputs
	.a		( p15_5 ),
	.b		( p14_6 ),
	.cin		( p13_7 ),
	// Outputs
	.sum		( s10_20 ),
	.cout		( c10_20 )
	);

half_adder HA9_10 (
	// Inputs
	.a		( p10_0 ),
	.b		( p8_2 ),
	// Outputs
	.sum		( s9_10 ),
	.cout		( c9_10 )
	);

full_adder FA9_11 (
	// Inputs
	.a		( p10_1 ),
	.b		( p8_3 ),
	.cin		( p7_4 ),
	// Outputs
	.sum		( s9_11 ),
	.cout		( c9_11 )
	);

full_adder FA9_12 (
	// Inputs
	.a		( p7_5 ),
	.b		( p6_6 ),
	.cin		( p5_7 ),
	// Outputs
	.sum		( s9_12 ),
	.cout		( c9_12 )
	);

full_adder FA9_13 (
	// Inputs
	.a		( p5_8 ),
	.b		( p4_9 ),
	.cin		( p3_10 ),
	// Outputs
	.sum		( s9_13 ),
	.cout		( c9_13 )
	);

full_adder FA9_14 (
	// Inputs
	.a		( p3_11 ),
	.b		( p2_12 ),
	.cin		( p1_13 ),
	// Outputs
	.sum		( s9_14 ),
	.cout		( c9_14 )
	);

full_adder FA9_15 (
	// Inputs
	.a		( p1_14 ),
	.b		( p0_15 ),
	.cin		( s14_15 ),
	// Outputs
	.sum		( s9_15 ),
	.cout		( c9_15 )
	);

full_adder FA9_16 (
	// Inputs
	.a		( p1_15 ),
	.b		( c14_15 ),
	.cin		( s14_16 ),
	// Outputs
	.sum		( s9_16 ),
	.cout		( c9_16 )
	);

full_adder FA9_17 (
	// Inputs
	.a		( p3_14 ),
	.b		( p2_15 ),
	.cin		( c14_16 ),
	// Outputs
	.sum		( s9_17 ),
	.cout		( c9_17 )
	);

full_adder FA9_18 (
	// Inputs
	.a		( p6_12 ),
	.b		( p5_13 ),
	.cin		( p4_14 ),
	// Outputs
	.sum		( s9_18 ),
	.cout		( c9_18 )
	);

full_adder FA9_19 (
	// Inputs
	.a		( p9_10 ),
	.b		( p8_11 ),
	.cin		( p7_12 ),
	// Outputs
	.sum		( s9_19 ),
	.cout		( c9_19 )
	);

full_adder FA9_20 (
	// Inputs
	.a		( p12_8 ),
	.b		( p11_9 ),
	.cin		( p10_10 ),
	// Outputs
	.sum		( s9_20 ),
	.cout		( c9_20 )
	);

full_adder FA9_21 (
	// Inputs
	.a		( p15_6 ),
	.b		( p14_7 ),
	.cin		( p13_8 ),
	// Outputs
	.sum		( s9_21 ),
	.cout		( c9_21 )
	);

half_adder HA8_9 (
	// Inputs
	.a		( p9_0 ),
	.b		( p7_2 ),
	// Outputs
	.sum		( s8_9 ),
	.cout		( c8_9 )
	);

full_adder FA8_10 (
	// Inputs
	.a		( p9_1 ),
	.b		( p7_3 ),
	.cin		( p6_4 ),
	// Outputs
	.sum		( s8_10 ),
	.cout		( c8_10 )
	);

full_adder FA8_11 (
	// Inputs
	.a		( p6_5 ),
	.b		( p5_6 ),
	.cin		( p4_7 ),
	// Outputs
	.sum		( s8_11 ),
	.cout		( c8_11 )
	);

full_adder FA8_12 (
	// Inputs
	.a		( p4_8 ),
	.b		( p3_9 ),
	.cin		( p2_10 ),
	// Outputs
	.sum		( s8_12 ),
	.cout		( c8_12 )
	);

full_adder FA8_13 (
	// Inputs
	.a		( p2_11 ),
	.b		( p1_12 ),
	.cin		( p0_13 ),
	// Outputs
	.sum		( s8_13 ),
	.cout		( c8_13 )
	);

full_adder FA8_14 (
	// Inputs
	.a		( p0_14 ),
	.b		( s13_14 ),
	.cin		( c12_13 ),
	// Outputs
	.sum		( s8_14 ),
	.cout		( c8_14 )
	);

full_adder FA8_15 (
	// Inputs
	.a		( c13_14 ),
	.b		( s13_15 ),
	.cin		( c12_14 ),
	// Outputs
	.sum		( s8_15 ),
	.cout		( c8_15 )
	);

full_adder FA8_16 (
	// Inputs
	.a		( c13_15 ),
	.b		( s13_16 ),
	.cin		( c12_15 ),
	// Outputs
	.sum		( s8_16 ),
	.cout		( c8_16 )
	);

full_adder FA8_17 (
	// Inputs
	.a		( c13_16 ),
	.b		( s13_17 ),
	.cin		( c12_16 ),
	// Outputs
	.sum		( s8_17 ),
	.cout		( c8_17 )
	);

full_adder FA8_18 (
	// Inputs
	.a		( p3_15 ),
	.b		( c13_17 ),
	.cin		( c12_17 ),
	// Outputs
	.sum		( s8_18 ),
	.cout		( c8_18 )
	);

full_adder FA8_19 (
	// Inputs
	.a		( p6_13 ),
	.b		( p5_14 ),
	.cin		( p4_15 ),
	// Outputs
	.sum		( s8_19 ),
	.cout		( c8_19 )
	);

full_adder FA8_20 (
	// Inputs
	.a		( p9_11 ),
	.b		( p8_12 ),
	.cin		( p7_13 ),
	// Outputs
	.sum		( s8_20 ),
	.cout		( c8_20 )
	);

full_adder FA8_21 (
	// Inputs
	.a		( p12_9 ),
	.b		( p11_10 ),
	.cin		( p10_11 ),
	// Outputs
	.sum		( s8_21 ),
	.cout		( c8_21 )
	);

full_adder FA8_22 (
	// Inputs
	.a		( p15_7 ),
	.b		( p14_8 ),
	.cin		( p13_9 ),
	// Outputs
	.sum		( s8_22 ),
	.cout		( c8_22 )
	);

half_adder HA7_8 (
	// Inputs
	.a		( p8_0 ),
	.b		( p6_2 ),
	// Outputs
	.sum		( s7_8 ),
	.cout		( c7_8 )
	);

full_adder FA7_9 (
	// Inputs
	.a		( p8_1 ),
	.b		( p6_3 ),
	.cin		( p5_4 ),
	// Outputs
	.sum		( s7_9 ),
	.cout		( c7_9 )
	);

full_adder FA7_10 (
	// Inputs
	.a		( p5_5 ),
	.b		( p4_6 ),
	.cin		( p3_7 ),
	// Outputs
	.sum		( s7_10 ),
	.cout		( c7_10 )
	);

full_adder FA7_11 (
	// Inputs
	.a		( p3_8 ),
	.b		( p2_9 ),
	.cin		( p1_10 ),
	// Outputs
	.sum		( s7_11 ),
	.cout		( c7_11 )
	);

full_adder FA7_12 (
	// Inputs
	.a		( p1_11 ),
	.b		( p0_12 ),
	.cin		( s11_12 ),
	// Outputs
	.sum		( s7_12 ),
	.cout		( c7_12 )
	);

full_adder FA7_13 (
	// Inputs
	.a		( s12_13 ),
	.b		( c11_12 ),
	.cin		( s11_13 ),
	// Outputs
	.sum		( s7_13 ),
	.cout		( c7_13 )
	);

full_adder FA7_14 (
	// Inputs
	.a		( s12_14 ),
	.b		( c11_13 ),
	.cin		( s11_14 ),
	// Outputs
	.sum		( s7_14 ),
	.cout		( c7_14 )
	);

full_adder FA7_15 (
	// Inputs
	.a		( s12_15 ),
	.b		( c11_14 ),
	.cin		( s11_15 ),
	// Outputs
	.sum		( s7_15 ),
	.cout		( c7_15 )
	);

full_adder FA7_16 (
	// Inputs
	.a		( s12_16 ),
	.b		( c11_15 ),
	.cin		( s11_16 ),
	// Outputs
	.sum		( s7_16 ),
	.cout		( c7_16 )
	);

full_adder FA7_17 (
	// Inputs
	.a		( s12_17 ),
	.b		( c11_16 ),
	.cin		( s11_17 ),
	// Outputs
	.sum		( s7_17 ),
	.cout		( c7_17 )
	);

full_adder FA7_18 (
	// Inputs
	.a		( s12_18 ),
	.b		( c11_17 ),
	.cin		( s11_18 ),
	// Outputs
	.sum		( s7_18 ),
	.cout		( c7_18 )
	);

full_adder FA7_19 (
	// Inputs
	.a		( c12_18 ),
	.b		( c11_18 ),
	.cin		( s11_19 ),
	// Outputs
	.sum		( s7_19 ),
	.cout		( c7_19 )
	);

full_adder FA7_20 (
	// Inputs
	.a		( p6_14 ),
	.b		( p5_15 ),
	.cin		( c11_19 ),
	// Outputs
	.sum		( s7_20 ),
	.cout		( c7_20 )
	);

full_adder FA7_21 (
	// Inputs
	.a		( p9_12 ),
	.b		( p8_13 ),
	.cin		( p7_14 ),
	// Outputs
	.sum		( s7_21 ),
	.cout		( c7_21 )
	);

full_adder FA7_22 (
	// Inputs
	.a		( p12_10 ),
	.b		( p11_11 ),
	.cin		( p10_12 ),
	// Outputs
	.sum		( s7_22 ),
	.cout		( c7_22 )
	);

full_adder FA7_23 (
	// Inputs
	.a		( p15_8 ),
	.b		( p14_9 ),
	.cin		( p13_10 ),
	// Outputs
	.sum		( s7_23 ),
	.cout		( c7_23 )
	);

half_adder HA6_7 (
	// Inputs
	.a		( p7_0 ),
	.b		( p5_2 ),
	// Outputs
	.sum		( s6_7 ),
	.cout		( c6_7 )
	);

full_adder FA6_8 (
	// Inputs
	.a		( p7_1 ),
	.b		( p5_3 ),
	.cin		( p4_4 ),
	// Outputs
	.sum		( s6_8 ),
	.cout		( c6_8 )
	);

full_adder FA6_9 (
	// Inputs
	.a		( p4_5 ),
	.b		( p3_6 ),
	.cin		( p2_7 ),
	// Outputs
	.sum		( s6_9 ),
	.cout		( c6_9 )
	);

full_adder FA6_10 (
	// Inputs
	.a		( p2_8 ),
	.b		( p1_9 ),
	.cin		( p0_10 ),
	// Outputs
	.sum		( s6_10 ),
	.cout		( c6_10 )
	);

full_adder FA6_11 (
	// Inputs
	.a		( p0_11 ),
	.b		( s10_11 ),
	.cin		( c9_10 ),
	// Outputs
	.sum		( s6_11 ),
	.cout		( c6_11 )
	);

full_adder FA6_12 (
	// Inputs
	.a		( c10_11 ),
	.b		( s10_12 ),
	.cin		( c9_11 ),
	// Outputs
	.sum		( s6_12 ),
	.cout		( c6_12 )
	);

full_adder FA6_13 (
	// Inputs
	.a		( c10_12 ),
	.b		( s10_13 ),
	.cin		( c9_12 ),
	// Outputs
	.sum		( s6_13 ),
	.cout		( c6_13 )
	);

full_adder FA6_14 (
	// Inputs
	.a		( c10_13 ),
	.b		( s10_14 ),
	.cin		( c9_13 ),
	// Outputs
	.sum		( s6_14 ),
	.cout		( c6_14 )
	);

full_adder FA6_15 (
	// Inputs
	.a		( c10_14 ),
	.b		( s10_15 ),
	.cin		( c9_14 ),
	// Outputs
	.sum		( s6_15 ),
	.cout		( c6_15 )
	);

full_adder FA6_16 (
	// Inputs
	.a		( c10_15 ),
	.b		( s10_16 ),
	.cin		( c9_15 ),
	// Outputs
	.sum		( s6_16 ),
	.cout		( c6_16 )
	);

full_adder FA6_17 (
	// Inputs
	.a		( c10_16 ),
	.b		( s10_17 ),
	.cin		( c9_16 ),
	// Outputs
	.sum		( s6_17 ),
	.cout		( c6_17 )
	);

full_adder FA6_18 (
	// Inputs
	.a		( c10_17 ),
	.b		( s10_18 ),
	.cin		( c9_17 ),
	// Outputs
	.sum		( s6_18 ),
	.cout		( c6_18 )
	);

full_adder FA6_19 (
	// Inputs
	.a		( c10_18 ),
	.b		( s10_19 ),
	.cin		( c9_18 ),
	// Outputs
	.sum		( s6_19 ),
	.cout		( c6_19 )
	);

full_adder FA6_20 (
	// Inputs
	.a		( c10_19 ),
	.b		( s10_20 ),
	.cin		( c9_19 ),
	// Outputs
	.sum		( s6_20 ),
	.cout		( c6_20 )
	);

full_adder FA6_21 (
	// Inputs
	.a		( p6_15 ),
	.b		( c10_20 ),
	.cin		( c9_20 ),
	// Outputs
	.sum		( s6_21 ),
	.cout		( c6_21 )
	);

full_adder FA6_22 (
	// Inputs
	.a		( p9_13 ),
	.b		( p8_14 ),
	.cin		( p7_15 ),
	// Outputs
	.sum		( s6_22 ),
	.cout		( c6_22 )
	);

full_adder FA6_23 (
	// Inputs
	.a		( p12_11 ),
	.b		( p11_12 ),
	.cin		( p10_13 ),
	// Outputs
	.sum		( s6_23 ),
	.cout		( c6_23 )
	);

full_adder FA6_24 (
	// Inputs
	.a		( p15_9 ),
	.b		( p14_10 ),
	.cin		( p13_11 ),
	// Outputs
	.sum		( s6_24 ),
	.cout		( c6_24 )
	);

half_adder HA5_6 (
	// Inputs
	.a		( p6_0 ),
	.b		( p4_2 ),
	// Outputs
	.sum		( s5_6 ),
	.cout		( c5_6 )
	);

full_adder FA5_7 (
	// Inputs
	.a		( p6_1 ),
	.b		( p4_3 ),
	.cin		( p3_4 ),
	// Outputs
	.sum		( s5_7 ),
	.cout		( c5_7 )
	);

full_adder FA5_8 (
	// Inputs
	.a		( p3_5 ),
	.b		( p2_6 ),
	.cin		( p1_7 ),
	// Outputs
	.sum		( s5_8 ),
	.cout		( c5_8 )
	);

full_adder FA5_9 (
	// Inputs
	.a		( p1_8 ),
	.b		( p0_9 ),
	.cin		( s8_9 ),
	// Outputs
	.sum		( s5_9 ),
	.cout		( c5_9 )
	);

full_adder FA5_10 (
	// Inputs
	.a		( s9_10 ),
	.b		( c8_9 ),
	.cin		( s8_10 ),
	// Outputs
	.sum		( s5_10 ),
	.cout		( c5_10 )
	);

full_adder FA5_11 (
	// Inputs
	.a		( s9_11 ),
	.b		( c8_10 ),
	.cin		( s8_11 ),
	// Outputs
	.sum		( s5_11 ),
	.cout		( c5_11 )
	);

full_adder FA5_12 (
	// Inputs
	.a		( s9_12 ),
	.b		( c8_11 ),
	.cin		( s8_12 ),
	// Outputs
	.sum		( s5_12 ),
	.cout		( c5_12 )
	);

full_adder FA5_13 (
	// Inputs
	.a		( s9_13 ),
	.b		( c8_12 ),
	.cin		( s8_13 ),
	// Outputs
	.sum		( s5_13 ),
	.cout		( c5_13 )
	);

full_adder FA5_14 (
	// Inputs
	.a		( s9_14 ),
	.b		( c8_13 ),
	.cin		( s8_14 ),
	// Outputs
	.sum		( s5_14 ),
	.cout		( c5_14 )
	);

full_adder FA5_15 (
	// Inputs
	.a		( s9_15 ),
	.b		( c8_14 ),
	.cin		( s8_15 ),
	// Outputs
	.sum		( s5_15 ),
	.cout		( c5_15 )
	);

full_adder FA5_16 (
	// Inputs
	.a		( s9_16 ),
	.b		( c8_15 ),
	.cin		( s8_16 ),
	// Outputs
	.sum		( s5_16 ),
	.cout		( c5_16 )
	);

full_adder FA5_17 (
	// Inputs
	.a		( s9_17 ),
	.b		( c8_16 ),
	.cin		( s8_17 ),
	// Outputs
	.sum		( s5_17 ),
	.cout		( c5_17 )
	);

full_adder FA5_18 (
	// Inputs
	.a		( s9_18 ),
	.b		( c8_17 ),
	.cin		( s8_18 ),
	// Outputs
	.sum		( s5_18 ),
	.cout		( c5_18 )
	);

full_adder FA5_19 (
	// Inputs
	.a		( s9_19 ),
	.b		( c8_18 ),
	.cin		( s8_19 ),
	// Outputs
	.sum		( s5_19 ),
	.cout		( c5_19 )
	);

full_adder FA5_20 (
	// Inputs
	.a		( s9_20 ),
	.b		( c8_19 ),
	.cin		( s8_20 ),
	// Outputs
	.sum		( s5_20 ),
	.cout		( c5_20 )
	);

full_adder FA5_21 (
	// Inputs
	.a		( s9_21 ),
	.b		( c8_20 ),
	.cin		( s8_21 ),
	// Outputs
	.sum		( s5_21 ),
	.cout		( c5_21 )
	);

full_adder FA5_22 (
	// Inputs
	.a		( c9_21 ),
	.b		( c8_21 ),
	.cin		( s8_22 ),
	// Outputs
	.sum		( s5_22 ),
	.cout		( c5_22 )
	);

full_adder FA5_23 (
	// Inputs
	.a		( p9_14 ),
	.b		( p8_15 ),
	.cin		( c8_22 ),
	// Outputs
	.sum		( s5_23 ),
	.cout		( c5_23 )
	);

full_adder FA5_24 (
	// Inputs
	.a		( p12_12 ),
	.b		( p11_13 ),
	.cin		( p10_14 ),
	// Outputs
	.sum		( s5_24 ),
	.cout		( c5_24 )
	);

full_adder FA5_25 (
	// Inputs
	.a		( p15_10 ),
	.b		( p14_11 ),
	.cin		( p13_12 ),
	// Outputs
	.sum		( s5_25 ),
	.cout		( c5_25 )
	);

half_adder HA4_5 (
	// Inputs
	.a		( p5_0 ),
	.b		( p3_2 ),
	// Outputs
	.sum		( s4_5 ),
	.cout		( c4_5 )
	);

full_adder FA4_6 (
	// Inputs
	.a		( p5_1 ),
	.b		( p3_3 ),
	.cin		( p2_4 ),
	// Outputs
	.sum		( s4_6 ),
	.cout		( c4_6 )
	);

full_adder FA4_7 (
	// Inputs
	.a		( p2_5 ),
	.b		( p1_6 ),
	.cin		( p0_7 ),
	// Outputs
	.sum		( s4_7 ),
	.cout		( c4_7 )
	);

full_adder FA4_8 (
	// Inputs
	.a		( p0_8 ),
	.b		( s7_8 ),
	.cin		( c6_7 ),
	// Outputs
	.sum		( s4_8 ),
	.cout		( c4_8 )
	);

full_adder FA4_9 (
	// Inputs
	.a		( c7_8 ),
	.b		( s7_9 ),
	.cin		( c6_8 ),
	// Outputs
	.sum		( s4_9 ),
	.cout		( c4_9 )
	);

full_adder FA4_10 (
	// Inputs
	.a		( c7_9 ),
	.b		( s7_10 ),
	.cin		( c6_9 ),
	// Outputs
	.sum		( s4_10 ),
	.cout		( c4_10 )
	);

full_adder FA4_11 (
	// Inputs
	.a		( c7_10 ),
	.b		( s7_11 ),
	.cin		( c6_10 ),
	// Outputs
	.sum		( s4_11 ),
	.cout		( c4_11 )
	);

full_adder FA4_12 (
	// Inputs
	.a		( c7_11 ),
	.b		( s7_12 ),
	.cin		( c6_11 ),
	// Outputs
	.sum		( s4_12 ),
	.cout		( c4_12 )
	);

full_adder FA4_13 (
	// Inputs
	.a		( c7_12 ),
	.b		( s7_13 ),
	.cin		( c6_12 ),
	// Outputs
	.sum		( s4_13 ),
	.cout		( c4_13 )
	);

full_adder FA4_14 (
	// Inputs
	.a		( c7_13 ),
	.b		( s7_14 ),
	.cin		( c6_13 ),
	// Outputs
	.sum		( s4_14 ),
	.cout		( c4_14 )
	);

full_adder FA4_15 (
	// Inputs
	.a		( c7_14 ),
	.b		( s7_15 ),
	.cin		( c6_14 ),
	// Outputs
	.sum		( s4_15 ),
	.cout		( c4_15 )
	);

full_adder FA4_16 (
	// Inputs
	.a		( c7_15 ),
	.b		( s7_16 ),
	.cin		( c6_15 ),
	// Outputs
	.sum		( s4_16 ),
	.cout		( c4_16 )
	);

full_adder FA4_17 (
	// Inputs
	.a		( c7_16 ),
	.b		( s7_17 ),
	.cin		( c6_16 ),
	// Outputs
	.sum		( s4_17 ),
	.cout		( c4_17 )
	);

full_adder FA4_18 (
	// Inputs
	.a		( c7_17 ),
	.b		( s7_18 ),
	.cin		( c6_17 ),
	// Outputs
	.sum		( s4_18 ),
	.cout		( c4_18 )
	);

full_adder FA4_19 (
	// Inputs
	.a		( c7_18 ),
	.b		( s7_19 ),
	.cin		( c6_18 ),
	// Outputs
	.sum		( s4_19 ),
	.cout		( c4_19 )
	);

full_adder FA4_20 (
	// Inputs
	.a		( c7_19 ),
	.b		( s7_20 ),
	.cin		( c6_19 ),
	// Outputs
	.sum		( s4_20 ),
	.cout		( c4_20 )
	);

full_adder FA4_21 (
	// Inputs
	.a		( c7_20 ),
	.b		( s7_21 ),
	.cin		( c6_20 ),
	// Outputs
	.sum		( s4_21 ),
	.cout		( c4_21 )
	);

full_adder FA4_22 (
	// Inputs
	.a		( c7_21 ),
	.b		( s7_22 ),
	.cin		( c6_21 ),
	// Outputs
	.sum		( s4_22 ),
	.cout		( c4_22 )
	);

full_adder FA4_23 (
	// Inputs
	.a		( c7_22 ),
	.b		( s7_23 ),
	.cin		( c6_22 ),
	// Outputs
	.sum		( s4_23 ),
	.cout		( c4_23 )
	);

full_adder FA4_24 (
	// Inputs
	.a		( p9_15 ),
	.b		( c7_23 ),
	.cin		( c6_23 ),
	// Outputs
	.sum		( s4_24 ),
	.cout		( c4_24 )
	);

full_adder FA4_25 (
	// Inputs
	.a		( p12_13 ),
	.b		( p11_14 ),
	.cin		( p10_15 ),
	// Outputs
	.sum		( s4_25 ),
	.cout		( c4_25 )
	);

full_adder FA4_26 (
	// Inputs
	.a		( p15_11 ),
	.b		( p14_12 ),
	.cin		( p13_13 ),
	// Outputs
	.sum		( s4_26 ),
	.cout		( c4_26 )
	);

half_adder HA3_4 (
	// Inputs
	.a		( p4_0 ),
	.b		( p2_2 ),
	// Outputs
	.sum		( s3_4 ),
	.cout		( c3_4 )
	);

full_adder FA3_5 (
	// Inputs
	.a		( p4_1 ),
	.b		( p2_3 ),
	.cin		( p1_4 ),
	// Outputs
	.sum		( s3_5 ),
	.cout		( c3_5 )
	);

full_adder FA3_6 (
	// Inputs
	.a		( p1_5 ),
	.b		( p0_6 ),
	.cin		( s5_6 ),
	// Outputs
	.sum		( s3_6 ),
	.cout		( c3_6 )
	);

full_adder FA3_7 (
	// Inputs
	.a		( s6_7 ),
	.b		( c5_6 ),
	.cin		( s5_7 ),
	// Outputs
	.sum		( s3_7 ),
	.cout		( c3_7 )
	);

full_adder FA3_8 (
	// Inputs
	.a		( s6_8 ),
	.b		( c5_7 ),
	.cin		( s5_8 ),
	// Outputs
	.sum		( s3_8 ),
	.cout		( c3_8 )
	);

full_adder FA3_9 (
	// Inputs
	.a		( s6_9 ),
	.b		( c5_8 ),
	.cin		( s5_9 ),
	// Outputs
	.sum		( s3_9 ),
	.cout		( c3_9 )
	);

full_adder FA3_10 (
	// Inputs
	.a		( s6_10 ),
	.b		( c5_9 ),
	.cin		( s5_10 ),
	// Outputs
	.sum		( s3_10 ),
	.cout		( c3_10 )
	);

full_adder FA3_11 (
	// Inputs
	.a		( s6_11 ),
	.b		( c5_10 ),
	.cin		( s5_11 ),
	// Outputs
	.sum		( s3_11 ),
	.cout		( c3_11 )
	);

full_adder FA3_12 (
	// Inputs
	.a		( s6_12 ),
	.b		( c5_11 ),
	.cin		( s5_12 ),
	// Outputs
	.sum		( s3_12 ),
	.cout		( c3_12 )
	);

full_adder FA3_13 (
	// Inputs
	.a		( s6_13 ),
	.b		( c5_12 ),
	.cin		( s5_13 ),
	// Outputs
	.sum		( s3_13 ),
	.cout		( c3_13 )
	);

full_adder FA3_14 (
	// Inputs
	.a		( s6_14 ),
	.b		( c5_13 ),
	.cin		( s5_14 ),
	// Outputs
	.sum		( s3_14 ),
	.cout		( c3_14 )
	);

full_adder FA3_15 (
	// Inputs
	.a		( s6_15 ),
	.b		( c5_14 ),
	.cin		( s5_15 ),
	// Outputs
	.sum		( s3_15 ),
	.cout		( c3_15 )
	);

full_adder FA3_16 (
	// Inputs
	.a		( s6_16 ),
	.b		( c5_15 ),
	.cin		( s5_16 ),
	// Outputs
	.sum		( s3_16 ),
	.cout		( c3_16 )
	);

full_adder FA3_17 (
	// Inputs
	.a		( s6_17 ),
	.b		( c5_16 ),
	.cin		( s5_17 ),
	// Outputs
	.sum		( s3_17 ),
	.cout		( c3_17 )
	);

full_adder FA3_18 (
	// Inputs
	.a		( s6_18 ),
	.b		( c5_17 ),
	.cin		( s5_18 ),
	// Outputs
	.sum		( s3_18 ),
	.cout		( c3_18 )
	);

full_adder FA3_19 (
	// Inputs
	.a		( s6_19 ),
	.b		( c5_18 ),
	.cin		( s5_19 ),
	// Outputs
	.sum		( s3_19 ),
	.cout		( c3_19 )
	);

full_adder FA3_20 (
	// Inputs
	.a		( s6_20 ),
	.b		( c5_19 ),
	.cin		( s5_20 ),
	// Outputs
	.sum		( s3_20 ),
	.cout		( c3_20 )
	);

full_adder FA3_21 (
	// Inputs
	.a		( s6_21 ),
	.b		( c5_20 ),
	.cin		( s5_21 ),
	// Outputs
	.sum		( s3_21 ),
	.cout		( c3_21 )
	);

full_adder FA3_22 (
	// Inputs
	.a		( s6_22 ),
	.b		( c5_21 ),
	.cin		( s5_22 ),
	// Outputs
	.sum		( s3_22 ),
	.cout		( c3_22 )
	);

full_adder FA3_23 (
	// Inputs
	.a		( s6_23 ),
	.b		( c5_22 ),
	.cin		( s5_23 ),
	// Outputs
	.sum		( s3_23 ),
	.cout		( c3_23 )
	);

full_adder FA3_24 (
	// Inputs
	.a		( s6_24 ),
	.b		( c5_23 ),
	.cin		( s5_24 ),
	// Outputs
	.sum		( s3_24 ),
	.cout		( c3_24 )
	);

full_adder FA3_25 (
	// Inputs
	.a		( c6_24 ),
	.b		( c5_24 ),
	.cin		( s5_25 ),
	// Outputs
	.sum		( s3_25 ),
	.cout		( c3_25 )
	);

full_adder FA3_26 (
	// Inputs
	.a		( p12_14 ),
	.b		( p11_15 ),
	.cin		( c5_25 ),
	// Outputs
	.sum		( s3_26 ),
	.cout		( c3_26 )
	);

full_adder FA3_27 (
	// Inputs
	.a		( p15_12 ),
	.b		( p14_13 ),
	.cin		( p13_14 ),
	// Outputs
	.sum		( s3_27 ),
	.cout		( c3_27 )
	);

half_adder HA2_3 (
	// Inputs
	.a		( p3_0 ),
	.b		( p1_2 ),
	// Outputs
	.sum		( s2_3 ),
	.cout		( c2_3 )
	);

full_adder FA2_4 (
	// Inputs
	.a		( p3_1 ),
	.b		( p1_3 ),
	.cin		( p0_4 ),
	// Outputs
	.sum		( s2_4 ),
	.cout		( c2_4 )
	);

full_adder FA2_5 (
	// Inputs
	.a		( p0_5 ),
	.b		( s4_5 ),
	.cin		( c3_4 ),
	// Outputs
	.sum		( s2_5 ),
	.cout		( c2_5 )
	);

full_adder FA2_6 (
	// Inputs
	.a		( c4_5 ),
	.b		( s4_6 ),
	.cin		( c3_5 ),
	// Outputs
	.sum		( s2_6 ),
	.cout		( c2_6 )
	);

full_adder FA2_7 (
	// Inputs
	.a		( c4_6 ),
	.b		( s4_7 ),
	.cin		( c3_6 ),
	// Outputs
	.sum		( s2_7 ),
	.cout		( c2_7 )
	);

full_adder FA2_8 (
	// Inputs
	.a		( c4_7 ),
	.b		( s4_8 ),
	.cin		( c3_7 ),
	// Outputs
	.sum		( s2_8 ),
	.cout		( c2_8 )
	);

full_adder FA2_9 (
	// Inputs
	.a		( c4_8 ),
	.b		( s4_9 ),
	.cin		( c3_8 ),
	// Outputs
	.sum		( s2_9 ),
	.cout		( c2_9 )
	);

full_adder FA2_10 (
	// Inputs
	.a		( c4_9 ),
	.b		( s4_10 ),
	.cin		( c3_9 ),
	// Outputs
	.sum		( s2_10 ),
	.cout		( c2_10 )
	);

full_adder FA2_11 (
	// Inputs
	.a		( c4_10 ),
	.b		( s4_11 ),
	.cin		( c3_10 ),
	// Outputs
	.sum		( s2_11 ),
	.cout		( c2_11 )
	);

full_adder FA2_12 (
	// Inputs
	.a		( c4_11 ),
	.b		( s4_12 ),
	.cin		( c3_11 ),
	// Outputs
	.sum		( s2_12 ),
	.cout		( c2_12 )
	);

full_adder FA2_13 (
	// Inputs
	.a		( c4_12 ),
	.b		( s4_13 ),
	.cin		( c3_12 ),
	// Outputs
	.sum		( s2_13 ),
	.cout		( c2_13 )
	);

full_adder FA2_14 (
	// Inputs
	.a		( c4_13 ),
	.b		( s4_14 ),
	.cin		( c3_13 ),
	// Outputs
	.sum		( s2_14 ),
	.cout		( c2_14 )
	);

full_adder FA2_15 (
	// Inputs
	.a		( c4_14 ),
	.b		( s4_15 ),
	.cin		( c3_14 ),
	// Outputs
	.sum		( s2_15 ),
	.cout		( c2_15 )
	);

full_adder FA2_16 (
	// Inputs
	.a		( c4_15 ),
	.b		( s4_16 ),
	.cin		( c3_15 ),
	// Outputs
	.sum		( s2_16 ),
	.cout		( c2_16 )
	);

full_adder FA2_17 (
	// Inputs
	.a		( c4_16 ),
	.b		( s4_17 ),
	.cin		( c3_16 ),
	// Outputs
	.sum		( s2_17 ),
	.cout		( c2_17 )
	);

full_adder FA2_18 (
	// Inputs
	.a		( c4_17 ),
	.b		( s4_18 ),
	.cin		( c3_17 ),
	// Outputs
	.sum		( s2_18 ),
	.cout		( c2_18 )
	);

full_adder FA2_19 (
	// Inputs
	.a		( c4_18 ),
	.b		( s4_19 ),
	.cin		( c3_18 ),
	// Outputs
	.sum		( s2_19 ),
	.cout		( c2_19 )
	);

full_adder FA2_20 (
	// Inputs
	.a		( c4_19 ),
	.b		( s4_20 ),
	.cin		( c3_19 ),
	// Outputs
	.sum		( s2_20 ),
	.cout		( c2_20 )
	);

full_adder FA2_21 (
	// Inputs
	.a		( c4_20 ),
	.b		( s4_21 ),
	.cin		( c3_20 ),
	// Outputs
	.sum		( s2_21 ),
	.cout		( c2_21 )
	);

full_adder FA2_22 (
	// Inputs
	.a		( c4_21 ),
	.b		( s4_22 ),
	.cin		( c3_21 ),
	// Outputs
	.sum		( s2_22 ),
	.cout		( c2_22 )
	);

full_adder FA2_23 (
	// Inputs
	.a		( c4_22 ),
	.b		( s4_23 ),
	.cin		( c3_22 ),
	// Outputs
	.sum		( s2_23 ),
	.cout		( c2_23 )
	);

full_adder FA2_24 (
	// Inputs
	.a		( c4_23 ),
	.b		( s4_24 ),
	.cin		( c3_23 ),
	// Outputs
	.sum		( s2_24 ),
	.cout		( c2_24 )
	);

full_adder FA2_25 (
	// Inputs
	.a		( c4_24 ),
	.b		( s4_25 ),
	.cin		( c3_24 ),
	// Outputs
	.sum		( s2_25 ),
	.cout		( c2_25 )
	);

full_adder FA2_26 (
	// Inputs
	.a		( c4_25 ),
	.b		( s4_26 ),
	.cin		( c3_25 ),
	// Outputs
	.sum		( s2_26 ),
	.cout		( c2_26 )
	);

full_adder FA2_27 (
	// Inputs
	.a		( p12_15 ),
	.b		( c4_26 ),
	.cin		( c3_26 ),
	// Outputs
	.sum		( s2_27 ),
	.cout		( c2_27 )
	);

full_adder FA2_28 (
	// Inputs
	.a		( p15_13 ),
	.b		( p14_14 ),
	.cin		( p13_15 ),
	// Outputs
	.sum		( s2_28 ),
	.cout		( c2_28 )
	);

half_adder HA1_2 (
	// Inputs
	.a		( p2_0 ),
	.b		( p0_2 ),
	// Outputs
	.sum		( s1_2 ),
	.cout		( c1_2 )
	);

full_adder FA1_3 (
	// Inputs
	.a		( p2_1 ),
	.b		( p0_3 ),
	.cin		( s2_3 ),
	// Outputs
	.sum		( s1_3 ),
	.cout		( c1_3 )
	);

full_adder FA1_4 (
	// Inputs
	.a		( s3_4 ),
	.b		( c2_3 ),
	.cin		( s2_4 ),
	// Outputs
	.sum		( s1_4 ),
	.cout		( c1_4 )
	);

full_adder FA1_5 (
	// Inputs
	.a		( s3_5 ),
	.b		( c2_4 ),
	.cin		( s2_5 ),
	// Outputs
	.sum		( s1_5 ),
	.cout		( c1_5 )
	);

full_adder FA1_6 (
	// Inputs
	.a		( s3_6 ),
	.b		( c2_5 ),
	.cin		( s2_6 ),
	// Outputs
	.sum		( s1_6 ),
	.cout		( c1_6 )
	);

full_adder FA1_7 (
	// Inputs
	.a		( s3_7 ),
	.b		( c2_6 ),
	.cin		( s2_7 ),
	// Outputs
	.sum		( s1_7 ),
	.cout		( c1_7 )
	);

full_adder FA1_8 (
	// Inputs
	.a		( s3_8 ),
	.b		( c2_7 ),
	.cin		( s2_8 ),
	// Outputs
	.sum		( s1_8 ),
	.cout		( c1_8 )
	);

full_adder FA1_9 (
	// Inputs
	.a		( s3_9 ),
	.b		( c2_8 ),
	.cin		( s2_9 ),
	// Outputs
	.sum		( s1_9 ),
	.cout		( c1_9 )
	);

full_adder FA1_10 (
	// Inputs
	.a		( s3_10 ),
	.b		( c2_9 ),
	.cin		( s2_10 ),
	// Outputs
	.sum		( s1_10 ),
	.cout		( c1_10 )
	);

full_adder FA1_11 (
	// Inputs
	.a		( s3_11 ),
	.b		( c2_10 ),
	.cin		( s2_11 ),
	// Outputs
	.sum		( s1_11 ),
	.cout		( c1_11 )
	);

full_adder FA1_12 (
	// Inputs
	.a		( s3_12 ),
	.b		( c2_11 ),
	.cin		( s2_12 ),
	// Outputs
	.sum		( s1_12 ),
	.cout		( c1_12 )
	);

full_adder FA1_13 (
	// Inputs
	.a		( s3_13 ),
	.b		( c2_12 ),
	.cin		( s2_13 ),
	// Outputs
	.sum		( s1_13 ),
	.cout		( c1_13 )
	);

full_adder FA1_14 (
	// Inputs
	.a		( s3_14 ),
	.b		( c2_13 ),
	.cin		( s2_14 ),
	// Outputs
	.sum		( s1_14 ),
	.cout		( c1_14 )
	);

full_adder FA1_15 (
	// Inputs
	.a		( s3_15 ),
	.b		( c2_14 ),
	.cin		( s2_15 ),
	// Outputs
	.sum		( s1_15 ),
	.cout		( c1_15 )
	);

full_adder FA1_16 (
	// Inputs
	.a		( s3_16 ),
	.b		( c2_15 ),
	.cin		( s2_16 ),
	// Outputs
	.sum		( s1_16 ),
	.cout		( c1_16 )
	);

full_adder FA1_17 (
	// Inputs
	.a		( s3_17 ),
	.b		( c2_16 ),
	.cin		( s2_17 ),
	// Outputs
	.sum		( s1_17 ),
	.cout		( c1_17 )
	);

full_adder FA1_18 (
	// Inputs
	.a		( s3_18 ),
	.b		( c2_17 ),
	.cin		( s2_18 ),
	// Outputs
	.sum		( s1_18 ),
	.cout		( c1_18 )
	);

full_adder FA1_19 (
	// Inputs
	.a		( s3_19 ),
	.b		( c2_18 ),
	.cin		( s2_19 ),
	// Outputs
	.sum		( s1_19 ),
	.cout		( c1_19 )
	);

full_adder FA1_20 (
	// Inputs
	.a		( s3_20 ),
	.b		( c2_19 ),
	.cin		( s2_20 ),
	// Outputs
	.sum		( s1_20 ),
	.cout		( c1_20 )
	);

full_adder FA1_21 (
	// Inputs
	.a		( s3_21 ),
	.b		( c2_20 ),
	.cin		( s2_21 ),
	// Outputs
	.sum		( s1_21 ),
	.cout		( c1_21 )
	);

full_adder FA1_22 (
	// Inputs
	.a		( s3_22 ),
	.b		( c2_21 ),
	.cin		( s2_22 ),
	// Outputs
	.sum		( s1_22 ),
	.cout		( c1_22 )
	);

full_adder FA1_23 (
	// Inputs
	.a		( s3_23 ),
	.b		( c2_22 ),
	.cin		( s2_23 ),
	// Outputs
	.sum		( s1_23 ),
	.cout		( c1_23 )
	);

full_adder FA1_24 (
	// Inputs
	.a		( s3_24 ),
	.b		( c2_23 ),
	.cin		( s2_24 ),
	// Outputs
	.sum		( s1_24 ),
	.cout		( c1_24 )
	);

full_adder FA1_25 (
	// Inputs
	.a		( s3_25 ),
	.b		( c2_24 ),
	.cin		( s2_25 ),
	// Outputs
	.sum		( s1_25 ),
	.cout		( c1_25 )
	);

full_adder FA1_26 (
	// Inputs
	.a		( s3_26 ),
	.b		( c2_25 ),
	.cin		( s2_26 ),
	// Outputs
	.sum		( s1_26 ),
	.cout		( c1_26 )
	);

full_adder FA1_27 (
	// Inputs
	.a		( s3_27 ),
	.b		( c2_26 ),
	.cin		( s2_27 ),
	// Outputs
	.sum		( s1_27 ),
	.cout		( c1_27 )
	);

full_adder FA1_28 (
	// Inputs
	.a		( c3_27 ),
	.b		( c2_27 ),
	.cin		( s2_28 ),
	// Outputs
	.sum		( s1_28 ),
	.cout		( c1_28 )
	);

full_adder FA1_29 (
	// Inputs
	.a		( p15_14 ),
	.b		( p14_15 ),
	.cin		( c2_28 ),
	// Outputs
	.sum		( s1_29 ),
	.cout		( c1_29 )
	);

wire [29:0] fadd_a;
wire [29:0] fadd_b;
wire [30:0] fadd_sum;
assign fadd_a[0] = p1_0;
assign fadd_b[0] = p0_1;
assign fadd_a[1] = p1_1;
assign fadd_b[1] = s1_2;
assign fadd_a[2] = c1_2;
assign fadd_b[2] = s1_3;
assign fadd_a[3] = c1_3;
assign fadd_b[3] = s1_4;
assign fadd_a[4] = c1_4;
assign fadd_b[4] = s1_5;
assign fadd_a[5] = c1_5;
assign fadd_b[5] = s1_6;
assign fadd_a[6] = c1_6;
assign fadd_b[6] = s1_7;
assign fadd_a[7] = c1_7;
assign fadd_b[7] = s1_8;
assign fadd_a[8] = c1_8;
assign fadd_b[8] = s1_9;
assign fadd_a[9] = c1_9;
assign fadd_b[9] = s1_10;
assign fadd_a[10] = c1_10;
assign fadd_b[10] = s1_11;
assign fadd_a[11] = c1_11;
assign fadd_b[11] = s1_12;
assign fadd_a[12] = c1_12;
assign fadd_b[12] = s1_13;
assign fadd_a[13] = c1_13;
assign fadd_b[13] = s1_14;
assign fadd_a[14] = c1_14;
assign fadd_b[14] = s1_15;
assign fadd_a[15] = c1_15;
assign fadd_b[15] = s1_16;
assign fadd_a[16] = c1_16;
assign fadd_b[16] = s1_17;
assign fadd_a[17] = c1_17;
assign fadd_b[17] = s1_18;
assign fadd_a[18] = c1_18;
assign fadd_b[18] = s1_19;
assign fadd_a[19] = c1_19;
assign fadd_b[19] = s1_20;
assign fadd_a[20] = c1_20;
assign fadd_b[20] = s1_21;
assign fadd_a[21] = c1_21;
assign fadd_b[21] = s1_22;
assign fadd_a[22] = c1_22;
assign fadd_b[22] = s1_23;
assign fadd_a[23] = c1_23;
assign fadd_b[23] = s1_24;
assign fadd_a[24] = c1_24;
assign fadd_b[24] = s1_25;
assign fadd_a[25] = c1_25;
assign fadd_b[25] = s1_26;
assign fadd_a[26] = c1_26;
assign fadd_b[26] = s1_27;
assign fadd_a[27] = c1_27;
assign fadd_b[27] = s1_28;
assign fadd_a[28] = c1_28;
assign fadd_b[28] = s1_29;
assign fadd_a[29] = p15_15;
assign fadd_b[29] = c1_29;
// Instantiation of final adder
ripple_carry_adder final_adder (
   .a    ( fadd_a[29:0] ),
   .b    ( fadd_b[29:0] ),
   .out  ( fadd_sum[30:0] )
 );



// Invert the MSB before assigning it to the output
assign out[31] = ~fadd_sum[30];
assign out[30:1] = fadd_sum[29:0];
// Assign the LSB of the partial product array directly to the output
assign out[0] = p0_0;

endmodule
