- Open "tb_project" in ModelSim (I have 10.4b Altera Starter Edition)

- From menu bar, click Compile > Compile 

- Locate "mult.sv" (the DUT) and click "Compile"

- If prompted to create the "work" library, click "Yes"

- Locate "mult_tb.sv" (the testbench) and click "Compile"

- Now close this window and find the "work" library in the Library pane

- Double-click on the testbench module. In this case, "mult_tb"

- Now run the simulation. Simulate > Run > Run All