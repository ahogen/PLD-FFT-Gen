////////////////////////////////////////////////////////////////////////////////// 
// 
// ROM Generator written by: Alexander Hogen 
//                           (https://bitbucket.org/ahogen/) 
// 
// Filename:  twiddle_rom_real.sv 
// Generated: Tue Sep 20 14:00:59 2016
// 
// Description: 
//     This module accepts a 11-bit address and returns -2147483648-bit value 
// 
////////////////////////////////////////////////////////////////////////////////// 

 
module twiddle_rom_real (
	input clk, 
	input [