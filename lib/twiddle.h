/******************************************************************************
 * Class:        twiddle
 *
 * Author:       Alexander Hogen
 * Date Created: 09/10/2016
 * Modified:     09/20/2016
 * Filename:     twiddle.h
 *
 * Constructors:
 *
 *    twiddle(int fft_length, int fft_bit_width)
 *
 *         Initializes all data members to their default values. Also sets
 *         the t_length and t_bit_width (private) members equal to the
 *         values passed in, if they are greater than 2.
 *
 * Destructors:
 *
 *	 ~twiddle()
 *
 *         Deletes both the 'r_vals' and 'i_vals' dynamically allocated
 *         arrays. All other data members can clean up after themselves.
 *
 *
 * Methods:
 *
 *	 bool Generate()
 *
 *         Generates and fills the real and imag int arrays with the
 *         twiddle factors needed to compute an FFT of length t_length
 *         and t_bit_width. The elements in these arrays are already
 *         scaled to fit the t_bit_width, but are represented as base-10
 *         integers.
 *
 *         Returns TRUE if memory allocation and number generation were
 *         successful. Otherwise, returns FALSE.
 *
 *    bool Export(string output_directory)
 *
 *         If the Generate() method was not previously called/executed, it
 *         will be called. It then instatiates two svROM objects with the
 *         real and imag twiddle factor arrays and then calls their Export
 *         methods to generate the SystemVerilog ROM files in
 *         'output_directory.'
 *
 *         Returns TRUE if Generate() call and svROM instantiation & Export
 *         executes without any known errors. Otherwise, returns FALSE.
 *
 *****************************************************************************/

#ifndef TWIDDLE_H
#define TWIDDLE_H

#include "sv_rom.h"

#include <string>
using std::string;

// Un-comment to enable debugging messages in terminal
// window. Comment out to disable debugging this class.
//#define DEBUG_TWIDDLE

#ifdef DEBUG_TWIDDLE
#include <iostream>
using std::cout;
using std::endl;
#endif

class twiddle
{

public:
  twiddle(const int &fft_length, const int &fft_bit_width);
  ~twiddle();

  // Generates both the real and imag twiddle factors
  bool Generate();

  // Creates both the real and imag ROM objects
  // and calls their Export functions
  bool Export(const string &output_directory);

protected:
private:
  // Note: The number of elements in the real and imaginary int arrays
  // is half the length of the desired FFT, so (t_length / 2). Because
  // of this, there is no element counter member.
  signed int *r_vals;
  signed int *i_vals;
  int t_length;
  int t_bit_width;
};

#endif // TWIDDLE_H
