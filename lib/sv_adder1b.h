/*!****************************************************************************
 * @file
 * @author   Alexander Hogen
 * @brief    Declares definition and instantiation
 *           functions for SystemVerilog compatible
 *           1-bit adder modules.
 *
 * Use the AdderDefStr() function to first
 * generate an adder definition string. Then
 * use your svAdder1B object's method
 * InstantiateStr() to generate an instantiation
 * string.
 *
 *****************************************************************************/

#ifndef SV_ADDER1B_H
#define SV_ADDER1B_H

#include <string>
using std::string;

/*!****************************************************************************
 * @brief Defining three possible adder types
 *
 * Note the specific FA1 option. Because of the
 * constant 1, the SV module can be optimized,
 * so a different module is instantiated for
 * this adder type.
 *****************************************************************************/
enum TAdd
{
  FA, /*!< Full adder. Inputs A, B, and C_IN will be used */
  HA, /*!< Half adder. Inputs A and B will be used */
  FA1 /*!< Full adder with a constant '1' as one input */
};

//! String constants for the separate adder module names.
const string NAME_HALF_ADD = "half_adder";
const string NAME_FULL_ADD = "full_adder";
const string NAME_FULL_ONE_ADD = "full_one_adder";

/*!****************************************************************************
 * @brief Generates a SystemVerilog adder module
 *        definition as a string.
 *
 * Generates a string based on the adder type
 * specified. This string can be written to a
 * SystemVerilog (.sv) file where the adder
 * will also be instantiated.
 *
 * Useful when small adders like these 1-bit adders
 * need to be explicitly defined but are almost
 * too small to warrant creating an entirely new
 * SystemVerilog file for the module definition.
 *
 * @param adder_type as TAdd
 * @returns string
 *****************************************************************************/
string AdderDefStr(TAdd adder_type);

/*!****************************************************************************
 * \class svAdder1B
 * @brief Class to define SV adder properties
 *
 * The adder object must be instantiated with data
 * which defines the input and output bits of an
 * adder. It also defines the name and type of
 * the adder which should be generated.
 *
 * Note: You must have defined a SystemVerilog
 * adder of the same adder type (TAdd) as the
 * object and that definition must be visible
 * to the SystemVerilog module that will contain
 * an adder object instantiation. Use the
 * AdderDec() function to generate an adder
 * module definition string.
 *****************************************************************************/
class svAdder1B
{

public:
  svAdder1B();
  svAdder1B(const char *name,
            const TAdd type,
            const char *a,
            const char *b,
            const char *c_in,
            const char *sum,
            const char *c_out);
  /*!**************************************************************************
   * @brief Generate SystemVerilog instantiation
   *        code as a string for this adder object.
   *
   * @returns Character string
   ***************************************************************************/
  string InstantiateStr();

  /*!**************************************************************************
   * @brief Generate SystemVerilog dataflow
   *        code for the actual adder as a string.
   *
   * @returns Character string
   ***************************************************************************/
  string InlineDataflow();

  void SetName(const string &s)
  {
    m_name = svStrCk(s) ? s : "";
  }

  void SetType(const TAdd t)
  {
    m_type = t;
  }

  void SetInA(const string &s)
  {
    m_a = svStrCk(s) ? s : "";
  }

  void SetInB(const string &s)
  {
    m_b = svStrCk(s) ? s : "";
  }

  void SetInCarry(const string &s)
  {
    m_c_in = svStrCk(s) ? s : "";
  }

  void SetOutSum(const string &s)
  {
    m_sum = svStrCk(s) ? s : "";
  }

  void SetOutCarry(const string &s)
  {
    m_c_out = svStrCk(s) ? s : "";
  }

  string GetOutSum() const
  {
    return (m_sum);
  }

  string GetOutCarry() const
  {
    return (m_c_out);
  }

private:
  string m_name;  //!< SystemVerilog instantiation name string
  TAdd m_type;    //!< Type of adder to be instantiated
  string m_a;     //!< The first input wire name
  string m_b;     //!< The second input wire name
  string m_c_in;  //!< The third (carry in) wire name. Not used when type is a half-adder.
  string m_sum;   //!< Sum output wire name
  string m_c_out; //!< Carry output wire name

  bool svStrCk(const string &s);
};

#endif //SV_ADDER1B_H
