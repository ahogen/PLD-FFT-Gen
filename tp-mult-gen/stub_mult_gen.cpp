/*!****************************************************************************
 * Project Name: tp-multiplier-generator
 * @file         stub_mult_gen.cpp
 * @author       Alexander Hogen
 * @date         09/28/2016
 * @version      0.1
 *
 * @brief Tests HPM Baugh-Wooley SystemVerilog multiplier
 *        generation in svMult_BW class.
 *
 * This program generates a SystemVerilog compatible
 * multiplier, using the Baugh-Wooley multiplier algorithm
 * paired with an HPM reduction tree [1].
 *
 * Input:
 *       Gets keyboard input from user via the terminal window.
 *
 * Output:
 *       Displays prompts to user in terminal window. Creates
 *       (or overwrites) a '.sv' file in the specified
 *       directory.
 *
 * References:
 *
 * [1] H. Eriksson, P. Larsson-Edefors, M. Sheeran,
 *     M. Sjalander, D. Johansson, and M. Scholin, "Multiplier
 *     Reduction Tree with Logarithmic Logic Depth and Regular
 *     Connectivity," in IEEE International Symposium on
 *     Circuits and Systems, May 2006.
 *     Available: http://bit.ly/2dvN2u3
 *
 * [2] M. Sjalander and P. Larsson-Edefors, "The Case for
 *     HPM-Based Baugh-Wooley Multipliers," Department of
 *     Computer Science and Engineering, Chalmers University
 *     of Technology, Tech. Rep. 08-8, March 2008.
 *     Available: http://bit.ly/2dt7DPP
 *
 *****************************************************************************/

#include <iostream>
using std::cout;
using std::endl;

#include "../lib/sv_mult_bw.h"

void PrintSuccess(bool b);

/*!****************************************************************************
 * @brief Instantiate and test a few svMult_BW
 *        objects.
 *
 * Instantiates various svMult_BW objects and
 * the calls various methods of those objects
 * to test all the functionality.
 *
 * @returns 0
 *
 *****************************************************************************/

int main()
{
  svMult_BW mult1("mult", 8);
  cout << "First export.... ";
  PrintSuccess(mult1.Export());

  svMult_BW mult2("mult2", 16);
  cout << "Second export.... ";
  PrintSuccess(mult2.Export());

  return (0);
}

void PrintSuccess(bool b)
{
  if (b)
  {
    cout << "Success!\n\n";
  }
  else
  {
    cout << "FAIL!!\n\n";
  }
}
