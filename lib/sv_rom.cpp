/******************************************************************************
 * Author:              Alexander Hogen
 * Date Created:        09/05/2016
 * Modified:            09/20/2016
 * Filename:            sv_rom.cpp
 * Parent header file:  sv_rom.h
 *
 * Overview:
 *
 *       This file contains the declarations of all methods
 *       in the svROM class.
 *****************************************************************************/

#include "sv_rom.h"

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ios;
using std::ofstream;

#include <sstream>
using std::hex;
using std::stringstream;

#include <math.h>

#include <time.h> // To insert a timestamp in the generated
                  // SV ROM file.

/******************************************************************************
 * Purpose: Constructor for using an external text file for
 *          the ROM data.
 *
 * Entry:   Object instantiated with (string, string) OR
 *             (string, string, string)               OR
 *             (string, string, string, int)
 *
 * Exit:    All data members are initialized and set either to
 *          empty/default values or to the values passed in.
 *****************************************************************************/
svROM::svROM(const string &rom_name,
             const string &init_file,
             const string &out_dir,
             const int &addr_offset,
             Logger *log_obj) : r_length(0),
                                r_bitwidth(0),
                                r_addr_width(0),
                                r_addr_offset(addr_offset),
                                rom_dat(nullptr),
                                r_name("rom1"),
                                r_init_file(init_file),
                                r_out_dir(out_dir),
                                imported(false)

{
  // Make sure string has characters and that the first
  // character *is* a letter, not a number or symbol.
  if (rom_name != "" && isalpha(rom_name.at(0)))
  {
    r_name = rom_name;
  }
}

/******************************************************************************
 * Purpose: Constructor for using an integer array as the ROM
 *          data.
 *
 * Entry:   Object instantiated with (string, int *, int) OR
 *             (string, int *, int, string)               OR
 *             (string, int *, int, string, int)
 *
 * Exit:    All data members are initialized and set either to
 *          empty/default values or to the values passed in.
 *          The integer array is deep-copied to "rom_dat" and
 *          the dataset sizes are analyzed.
 *****************************************************************************/
svROM::svROM(const string &rom_name,
             const int values[],
             const int &num_vals,
             const string &out_dir,
             const int &addr_offset,
             Logger *log_obj) : r_length(num_vals),
                                r_bitwidth(0),
                                r_addr_width(0),
                                r_addr_offset(addr_offset),
                                rom_dat(nullptr),
                                r_name("rom1"),
                                r_init_file(""),
                                r_out_dir(out_dir),
                                imported(true) // NOTE: Notice that 'imported' is
                                               // set to TRUE. Because this ctor is called
                                               // with an integer array, the Export()
                                               // function must NOT call ImportFile()
{
  int max_num(0);
  int min_num(0);

#if ENABLE_LOG_SV_ROM
  {
    string tmp_log_str = "Entered svROM ctor for " + rom_name;
    m_log->Debug(tmp_log_str.c_str());
  }
#endif // ENABLE_LOG_SV_ROM

  // Make sure string has characters and that the first
  // character *is* a letter, not a number or symbol.
  if (rom_name != "" && isalpha(rom_name.at(0)))
  {
    r_name = rom_name;
  }
#if ENABLE_LOG_SV_ROM
  else
  {
    m_log->Error("Invalid module name");
  }
#endif // ENABLE_LOG_SV_ROM

  ////// Perform a deep copy of "values" in to "rom_dat"
  if (rom_dat == nullptr)
  {
    // Allocate new memory block
    rom_dat = new int[num_vals];

    // Copy to new array, element by element
    for (int i = 0; i < num_vals; i++)
    {
      rom_dat[i] = values[i];

      // Update max value if the current number is larger
      // than the value currently in max_num
      max_num = values[i] > max_num ? values[i] : max_num;
      min_num = values[i] < min_num ? values[i] : min_num;
    }
  }

  // Calculate ROM data bit width
  if (max_num != 0)
  {
    r_bitwidth = ceil(log2(max_num - min_num));

#if ENABLE_LOG_SV_ROM
    {
      string tmp_log_str = "ROM will be " + std::to_string(r_bitwidth) + " bits wide";
      m_log->Info(tmp_log_str.c_str());
    }
#endif // ENABLE_LOG_SV_ROM
  }

  // Calculate ROM address bit width
  if (r_length != 0)
  {
    r_addr_width = ceil(log2(r_length + r_addr_offset));

#if ENABLE_LOG_SV_ROM
    {
      string tmp_log_str = "ROM will be " + std::to_string(r_length) + " bits long";
      m_log->Info(tmp_log_str.c_str());
    }
#endif // ENABLE_LOG_SV_ROM
  }

#if ENABLE_LOG_SV_ROM
  {
    string tmp_log_str = r_name + " ROM will be " + std::to_string(this->GetSizeBytes()) + " bytes";
    m_log->Info(tmp_log_str.c_str());
  }
#endif // ENABLE_LOG_SV_ROM

#if ENABLE_LOG_SV_ROM
  {
    string tmp_log_str = "Exiting svROM ctor for " + rom_name;
    m_log->Debug(tmp_log_str.c_str());
  }
#endif // ENABLE_LOG_SV_ROM
}

/******************************************************************************
 * Purpose: Destructor. Safely deallocates "rom_dat"
 *
 * Entry:   Destruction of the object or "delete" command.
 *
 * Exit:    ROM data memory freed. "rom_dat" set to nullptr
 *****************************************************************************/
svROM::~svROM()
{
// Destructor code here
#if ENABLE_LOG_SV_ROM
//m_log->Debug("svROM dtor for " + r_name + " has been fired");
#endif // ENABLE_LOG_SV_ROM

  // Deallocate dynamic array(s)
  delete[] rom_dat;
  rom_dat = nullptr;
}

/******************************************************************************
 * Purpose: Import numbers from a plain-text file.
 *
 * Entry:   Any svROM object or friend class/function
 *
 * Exit:    "rom_dat" filled with numbers extracted from text
 *          file. In addition, the following data members
 *          are updated based on the properties of the newly
 *          imported data set:
 *
 *             r_length
 *             r_bitwidth
 *             r_addr_width
 *             imported = true
 *
 *          Any errors reported by returning FALSE.
 *
 *****************************************************************************/
bool svROM::ImportFile()
{
  bool success(false);
  bool keep_reading = true;
  int temp_num(0);
  int max_num(0);
  int min_num(0);
  string line("");

#if ENABLE_LOG_SV_ROM
  cout << "ENTERED: ImportFile() for " << r_name << endl;
#endif // ENABLE_LOG_SV_ROM

  // Reset length, width values and 'imported' flag
  r_length = 0;
  r_bitwidth = 0;
  imported = false;

#if ENABLE_LOG_SV_ROM
  cout << "-- INFO: Attempting to open \"" << r_init_file << "\" : ";
#endif // ENABLE_LOG_SV_ROM

  ifstream f(r_init_file, ios::in);

  if (f.is_open())
  {
#if ENABLE_LOG_SV_ROM
    cout << "OPENED" << endl;
#endif // ENABLE_LOG_SV_ROM

    string token("");
    stringstream iss;

    while (getline(f, line))
    {
      iss << line;

      while (getline(iss, token, ',') && keep_reading)
      {
        temp_num = 0;

        // Find consecutive numbers in the string
        for (unsigned int i = 0; (i < token.length() && keep_reading); i++)
        {
          // If the char is not a number, exit with an error
          if (isdigit(token.at(i)) == false)
          {
#if ENABLE_LOG_SV_ROM
            cout << "-- ERROR: Non-numeric character at position ";
            cout << f.tellg() << endl;
#endif // ENABLE_LOG_SV_ROM

            keep_reading = false;
          }

          // Convert ASCII char to number and append to 'temp_num'
          temp_num = (temp_num * 10) + (token.at(i) - 48);
        }

#if ENABLE_LOG_SV_ROM
        cout << "-- INFO: Adding " << temp_num << " to array " << endl;
#endif // ENABLE_LOG_SV_ROM

        // Add the new number to the ROM init array
        AddToArray(temp_num);

        // Update max_num and min_num
        max_num = temp_num > max_num ? temp_num : max_num;
        min_num = temp_num < min_num ? temp_num : min_num;
      }

      iss.clear();
    }

    success = true & keep_reading;

    f.close();

    imported = true;
  }
// Error message if error opening file
#if ENABLE_LOG_SV_ROM
  else
  {
    cout << "FAIL                      <----------<<" << endl;
  }
#endif // ENABLE_LOG_SV_ROM

  // Calculate ROM data bit width
  if (max_num != 0)
  {
    r_bitwidth = ceil(log2(max_num - min_num));

#if ENABLE_LOG_SV_ROM
    cout << "-- INFO: ROM will be " << r_bitwidth << " bits wide" << endl;
#endif // ENABLE_LOG_SV_ROM
  }

  // Calculate ROM address bit width
  if (r_length != 0)
  {
    r_addr_width = ceil(log2(r_length + r_addr_offset));

#if ENABLE_LOG_SV_ROM
    cout << "-- INFO: ROM will be " << r_length << " bits long" << endl;
#endif // ENABLE_LOG_SV_ROM
  }

#if ENABLE_LOG_SV_ROM
  cout << "-- INFO: " << r_name << " ROM will be ";
  cout << this->GetSizeBytes() << " bytes " << endl;
#endif // ENABLE_LOG_SV_ROM

#if ENABLE_LOG_SV_ROM
  cout << "EXITING: ImportFile() for " << r_name << " with status: ";
  cout << (success ? "SUCCESS \n" : "FAIL <----------<< \n") << endl;
#endif // ENABLE_LOG_SV_ROM

  return (success);
}

/******************************************************************************
 * Purpose: Create SystemVerilog ROM file
 *
 * Entry:   Any svROM object or friend class/function
 *
 * Exit:    A SystemVerilog (".sv") file is created in
 *          "r_out_dir" directory with the name "r_name."
 *
 *          No data members are modified unless ImportFile()
 *          must be called.
 *
 *          Any errors reported by returning FALSE.
 *
 *****************************************************************************/
bool svROM::Export()
{
  time_t rawtime;
  struct tm *timeinfo;
  bool success = false;

#if ENABLE_LOG_SV_ROM
  cout << "ENTERED: Export() for " << r_name << endl;
#endif // ENABLE_LOG_SV_ROM

  // Import the init file if it has not already been imported
  if (!imported)
  {
#if ENABLE_LOG_SV_ROM
    cout << "-- INFO: No init file imported for " << r_name << endl;
#endif // ENABLE_LOG_SV_ROM

    ImportFile();
  }

  // Only attempt to Export if 'imported' is true
  if (imported && (rom_dat != nullptr))
  {
    // Set starting address
    int addr(r_addr_offset);

    // Create full filepath string
    string full_output_name(r_out_dir + "/" + r_name + ".sv");

#if ENABLE_LOG_SV_ROM
    cout << "-- INFO: Attempting to open \"" << full_output_name;
    cout << "\" for writing"
         << " : ";
#endif // ENABLE_LOG_SV_ROM

    // Attempt to open the target ouput file
    ofstream f(full_output_name, ios::out | ios::trunc);

    if (f.is_open())
    {

#if ENABLE_LOG_SV_ROM
      cout << "OPENED" << endl;
#endif // ENABLE_LOG_SV_ROM

      f << "////////////////////////////////////////////////////////////////////////////////// \n";
      f << "// \n";
      f << "// ROM Generator written by: Alexander Hogen \n";
      f << "//                           (https://bitbucket.org/ahogen/) \n";
      f << "// \n";
      f << "// Filename:  " << r_name << ".sv \n";

      // Get the local time to timestamp the output file
      time(&rawtime);
      timeinfo = localtime(&rawtime);
      f << "// Generated: " << asctime(timeinfo);
      f << "// \n";
      f << "// Description: \n";
      f << "//     This module accepts a " << r_addr_width << "-bit address and returns ";
      f << r_bitwidth << "-bit value \n";
      f << "// \n";
      f << "////////////////////////////////////////////////////////////////////////////////// \n";

      f << "\n \n";

      f << "module " << r_name << " (\n";
      f << "\tinput clk, \n";
      f << "\tinput [" << r_addr_width - 1 << ":0] addr, \n";
      f << "\toutput logic [" << r_bitwidth - 1 << ":0] out \n";
      f << "); \n\n";

      f << "logic [" << r_addr_width - 1 << ":0] addr_lat = ";
      f << r_addr_width << "\'h0;\n\n";

      f << "always @ (posedge clk) \n";
      f << "begin \n";
      f << "\taddr_lat <= addr;\n\n";

      f << "\tcase(addr_lat) \n";

      for (int i = 0; i < r_length; i++)
      {
        f << "\t\t" << r_addr_width << "\'d" << addr;
        f << ": out <= " << r_bitwidth << "\'h";
        f << IntToHexStr(rom_dat[i]) << ";\n";

        addr++;
      }

      f << "\t\tdefault: out <= " << r_bitwidth << "'h0;\n";
      f << "\tendcase \n";
      f << "end \n";
      f << "endmodule";

      f.close();

      success = true;
    }
// Error message if error opening file
#if ENABLE_LOG_SV_ROM
    else
    {
      cout << "FAIL                      <----------<<" << endl;
    }
#endif // ENABLE_LOG_SV_ROM
  }

#if ENABLE_LOG_SV_ROM
  cout << "EXITING: Export() for " << r_name << " with status: ";
  cout << (success ? "SUCCESS \n" : "FAIL <----------<<\n") << endl;
#endif // ENABLE_LOG_SV_ROM

  return (success);
}

/******************************************************************************
 * Purpose: Return the byte size of the generated ROM module
 *
 * Entry:   Any svROM object or friend class/function
 *
 * Exit:    Returns the number of bytes in the ROM. No data
 *          members are modified.
 *****************************************************************************/
inline int svROM::GetSizeBytes() const
{
  return ((r_length * r_bitwidth) / 8);
}

/******************************************************************************
 * Purpose: Display all ROM data to a terminal window
 *
 * Entry:   Any svROM object or friend class/function
 *
 * Exit:    "rom_dat" elements printed, one per line, to a
 *          terminal window. No data members are modified.
 *****************************************************************************/
void svROM::Print() const
{
  for (int i = 0; i < r_length; i++)
  {
    cout << rom_dat[i] << endl;
  }
}

/******************************************************************************
 * Purpose: Append an integer to "rom_dat" array
 *
 * Entry:   Only another member method in the svROM class can
 *          call this method.
 *
 * Exit:    Value of integer passed in is appended to the
 *          dynamically allocated "rom_dat" array. "r_length"
 *          is incremented.
 *****************************************************************************/
void svROM::AddToArray(int new_value)
{
  int *temp_array = new int[r_length + 1];

  // Copy old ROM data to new array
  for (int i = 0; i < r_length; i++)
  {
    temp_array[i] = rom_dat[i];
  }

  // Move new data to last element in new ROM array
  temp_array[r_length] = new_value;

  // Deallocate old ROM array
  delete[] rom_dat;

  // Copy pointer of new ROM array to old array pointer
  rom_dat = temp_array;

  // Increment ROM array length counter
  r_length++;
}

/******************************************************************************
 * Purpose: Convert an integer to a hexadecimal representation
 *          in a string form.
 *
 * Entry:   Only another member method in the svROM class can
 *          call this method.
 *
 * Exit:    "num" returned as a base-16 number with "r_bitwidth"
 *          bits in a string.
 *****************************************************************************/
string svROM::IntToHexStr(int num) const
{
  string t_str;
  stringstream ss;

  if ((num >> (r_bitwidth - 1)) != 0) // if sign bit is set
  {
    num = ~num - (1 << r_bitwidth); // compute negative value
    num = (-num) - 1;
  }

  // Use stringstream int to hex conversion tool
  ss << hex << num;

  // Move stringstream to string
  t_str = ss.str();

  // To remove extra hexadecimal places, we will extract only the required
  // least-significant hex places from the string.

  // Start by finding the starting position to extract substring from
  int pos = t_str.length() - ceil(r_bitwidth / 4.0);

  // Extract hex places starting at 'pos' until the end of the string
  if (pos >= 0)
  {
    t_str = t_str.substr(pos);
  }

  // Return the final converted hex representation of the int value
  // with r_bitwidth number of bits
  return (t_str);
}
