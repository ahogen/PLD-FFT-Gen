`timescale 1ns / 100ps

///////////////////////////////////////////////////////////
//
// Author:  Alexander Hogen
// Created: 10/8/2016
// Updated: 10/9/2016
// Purpose: Verify the combinational logic of a multi-bit
//          combinational adder module. You must change 
//          the parameter below to match the adder's bit 
//          width for the test to function correctly.
//
///////////////////////////////////////////////////////////

module ripple_carry_adder_tb ();

/**********************************************************
*                    SETUP PARAMETERS
**********************************************************/
// Set to equal the bit width of the DUT adder
parameter BIT_WIDTH = 6;

/**********************************************************
*                    TESTBENCH SIGNALS
**********************************************************/
// (Subtract 1 from bit width to simplify definitions below)
localparam BW = BIT_WIDTH -1;

//localparam NUM_CASES = 2 ** (BIT_WIDTH + BIT_WIDTH);


logic [BW:0] a;
logic [BW:0] b;
wire  [BW+1 : 0] sum;

/**********************************************************
*                    INSTANTIATE DUT(s)
**********************************************************/
ripple_carry_adder DUT (
	.a   ( a ), 
	.b   ( b ), 
	.out ( sum )
); 

/**********************************************************
*                     TESTBENCH LOGIC
**********************************************************/
logic [ BIT_WIDTH : 0] t_sum;
logic [ (2 * BIT_WIDTH)-1 :0] error_cnt;
logic [ (2 * BIT_WIDTH)-1 :0] pass_cnt;
logic [BW:0]  MAX_VAL;
integer report;

// INITIAL BLOCk
initial begin
	$display( "Initializing... " );
	
	a = 0;
	b = 0;
	t_sum = 0;
	error_cnt = 0;
	pass_cnt = 0;
	MAX_VAL = 'h0;
	MAX_VAL = ~MAX_VAL;

	report = $fopen("ripple_carry_adder_tb_REPORT.txt", "w");
	
	// Any other initialization code here
	
	$display( "Starting testbench... " );

	end


initial begin

	while ( (a != MAX_VAL) || (b != MAX_VAL) )
	begin
	
		{a, b} = {a, b} + 1;
		
		// Short pause
		#1;
		
		// Calculate actual sum
		t_sum = a + b;
		
		// Short pause
		#1;
		
		$display( "Expected sum: %d. DUT calculated sum: %d", t_sum, sum );
		
		// Test output
		if (t_sum != sum) 
		begin
		
			$display( "!!!!!!!!  FAIL !!!!!!!!");
			$display( "Input: %d + %d at time %t", a, b, $stime,);
			$display( "Expected sum: %d. DUT calculated sum: %d", t_sum, sum );
			
			$fdisplay( report, "ERROR: Expected output: %d DUT output: %d at time %t", t_sum, sum, $time);
			
			error_cnt = error_cnt + 1;
		end
			
		else 
		begin
			$display( ".... ok" );
			pass_cnt = pass_cnt + 1;
		end

	end
	
	$display( "===== RESULTS =====" );
	$display( "Calculations passed: %d", pass_cnt );
	$display( "Calculations failed: %d", error_cnt );
	
	
	$fdisplay( report, "===== RESULTS =====" );
	$fdisplay( report, "Calculations passed: %d", pass_cnt );
	$fdisplay( report, "Calculations failed: %d", error_cnt);
	$fdisplay( report, "Success rate: %d%%", ((pass_cnt) / (pass_cnt + error_cnt))*100 );
	$fdisplay( report, "=================================================");
	$fclose(report);
end

endmodule 