![picture](img/readme-banner.png)

# README #

Hi there! Thanks for taking interest in this project! Allow me to give you some background information on why this project is what it is and how you can get started using it!

[TOC]

### Status ###

| Component Name         | C++ Code Written | C++ Code Tested | SV Synthesizes | SV ModelSim Simulation Test |
|------------------------|------------------|-----------------|----------------|-----------------------------|
| ROM                    |![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/chk.png)| ![picture](img/chk.png) |
| Twiddle ROM            |![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/chk.png)|
| Multiplier             |![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/chk.png)|
| RAM                    |![picture](img/chk.png)|![picture](img/chk.png)|![picture](img/spinner.gif) |![picture](img/x.png)|
| 2-port RAM             |![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|
| Butterfly Unit (BFU)   |![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|
| Top-level module       |![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|
| (op) Kogge-Stone Adder |![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|![picture](img/x.png)|

### What is this project? ###

This project's goal is to make integrating a fast Fourier transform (FFT) processing core into your project incredibly simple. If you are reading this, then you are most likely well aware of the many uses for an FFT in an FPGA. I was going to use it for my senior project but had a quite a difficult time integrating a proprietary FFT core. The users guide for the core was not clear in the least. Even if I could have gotten it working, I didn't (and still don't) have the money to purchase a license for the core, so it would be severly limited in its functionality.

My senior project research taught me a lot about FFT processing in a hardware platform, which got me thinking about how I could build my own core. However, I didn't want to just make a single core for my own specific application, I wanted the functionality of generating an FFT processor of any length and bit-width. And that is how this FFT generator was born.

With the guidance of [George Slade's paper](https://www.researchgate.net/publication/235995761_The_Fast_Fourier_Transform_in_Hardware_A_Tutorial_Based_on_an_FPGA_Implementation), along with some other internet resources, I have begun working on a **C++** object oriented library to configure and generate **SystemVerilog** compatible modules for use in *any vendor's* FPGA.

The FFT core uses a custom-built multiplier. Specifically, a SystemVerilog implementation of the HPM-based Baugh-Wooley multiplier, as researched by H. Eriksson, P. Larsson-Edefors, M. Sheeran, M. Sjalander, D. Johansson, and M. Scholin. Refer to their paper [*Multiplier Reduction Tree with Logarithmic Logic Depth and Regular Connectivity*](http://www.sjalander.com/research/pdf/sjalander-iscas2006.pdf) for more information. I have contacted Mr. Sjalander and received permission to impliment their multiplier design.

### What is this repository for? ###

This repository is meant to track the progress of all the C++ code used to test and impliment the final FFT core generation program. It also serves as a bug tracker.

### How do I get set up? ###

(something will be added here later)

### Contribution guidelines ###

For now, please just report any and all bugs you may find in the bug tracker! There is a lot of work still to be done, so please don't expect this to work immediatly.

### Who do I talk to? ###

Alexander Hogen

B.S. Computer Engineering Technology, 2016

Oregon Institute of Technology

ahogen@outlook.com