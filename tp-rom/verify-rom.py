# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 19:57:24 2017

@author: Alex Hogen

Used to automatically build a Code::Blocks project in the current directory,
look for any SystemVerilog files created by the resulting executible, and
test the syntax of those executibles with the "quartus_map" utility provided
by the Quartus software package.

"""
import os
#import shutil
import subprocess
from sys import exit
import platform

target_exe_name = "rom"

sys_type = platform.system()

sv_file_list = []
t_filename = []

# Find Code::Blocks project file
print()
print("Looking for Code::Blocks project file...")
print()
for root, dirs, files in os.walk("."):
    for file in files:
        if file.endswith(".cbp"):
            t_filename.append( os.path.join(root, file) )

if ( len(t_filename) > 1):
    exit("Found more than one C::B project file!!")
    
if ( len(t_filename) < 1):
    exit("Could not find a Code::Blocks project file (.cbp) in " + os.getcwd() )
    
# Build project
print( subprocess.check_output("codeblocks --rebuild " + t_filename[0] ))

t_filename = ""

# Find built executible 
print()
print("Looking for newly built executible...")
print()
for root, dirs, files in os.walk("bin"):
    for file in files:
        if target_exe_name in file:
            print( file )
            t_filename =  root + "\\" + file

# Run executable
print()
print( "Running re-built executable...")
print()
print( subprocess.check_output( t_filename ))


# Find all SystemVerilog files
print()
print("Looking for .sv files...")
print()
for root, dirs, files in os.walk("."):
    for file in files:
        if file.endswith(".sv"):
            sv_file_list.append( os.path.join(root, file) )

print()
print( "Found " + str(len(sv_file_list)) + " SystemVerilog files." )
print() 

print("Attempting to verify syntax by compiling each file individually with \"quartus_map\"")
print()

for i in range(0, len(sv_file_list)):

    if subprocess.call("quartus_map temp_test_1947 --family=max10 --analyze_file=" + sv_file_list[i] ) == 0:
        print( sv_file_list[i] + " SUCCESSFULLY compiles with Quartus")
    else:
        print( sv_file_list[i] + " has errors!")
        
print()
print("Cleaning up...")
print()
for root, dirs, files in os.walk("."):
    for file in files:
        if "temp_test_1947.qsf" in file or "temp_test_1947.qpf" in file:
            os.remove(os.path.join(root, file))
            
shutil.rmtree("db")
#shutil.rmtree("bin")
#shutil.rmtree("obj")

print("All done...")