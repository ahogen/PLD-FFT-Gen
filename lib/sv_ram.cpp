/*!****************************************************************************
 * @file
 * @brief  svRAM class definitions
 *
 * @author Alexander Hogen
 * @date   01/06/2017
 *
 * Definitions for all the ctors, dtor, and
 * methods of the svRAM class.
 *
 *****************************************************************************/
#include "sv_ram.h"

#include <cmath>
using std::ceil;
using std::log2;

#include <fstream>
using std::ios;
using std::ofstream;

#include <stdexcept>
#include <time.h> // To insert a timestamp in the generated
                  // SystemVerilog RAM file.

/*!****************************************************************************
 * @brief Default constructor with no parameters
 *
 * Initializes the svRAM object with some default information
 * and initializes internal variables to their default values.
 *
 *****************************************************************************/
svRAM::svRAM() : svRAM("rom0", 0, 0)
{
}

/*!****************************************************************************
 * @brief Constructor with name
 *
 *
 * @param name is used to set m_name
 *
 * @param width is the number of bits in each line or word of
 *        the RAM block
 *
 * @param length is the number of lines or words in the RAM
 *        block
 *
 * @param log_obj is an optional parameter. Pass in the addres
 *        of a logger object. If ENABLE_LOG_SV_RAM is defined,
 *        then log messages will be sent to log_obj for
 *        processing. Yay!
 *
 *****************************************************************************/
svRAM::svRAM(
    const char *name,
    const unsigned int width,
    const unsigned int length,
    logger *log_obj)
    : m_name("rom0"),
      m_bitwidth(width),
      m_length(length),
      m_addr_width(0),
      m_log(log_obj)
{
  // Copy name only if the first character is an actual letter
  // (not a number or symbol)
  if (isalpha(name[0]))
  {
    m_name = name;
  }

#ifdef ENABLE_LOG_SV_RAM
  if (m_log == nullptr)
  {
    throw std::runtime_error(std::string("You have defined ENABLE_LOG_SV_RAM but have not attached a log class!"));
  }

#endif // ENABLE_LOG_SV_RAM

// Warn if total bit size is larger than 4K bits
#ifdef ENABLE_LOG_SV_RAM
  if (length * width > 4096)
  {
    string tmp_log_str = "Large RAM block size. ";
    tmp_log_str += m_name + " will be at least ";
    tmp_log_str += std::to_string(static_cast<int>(length * width) / 8) + " bytes ";
    m_log->warning(tmp_log_str.c_str());
  }
#endif

  // Calculate the RAM address bit width
  UpdateAddrWidth();
}

void svRAM::SetName(const string &name)
{
  if (isalpha(name.at(0)))
  {
    // Copy name into private data member
    m_name = name;
  }
}



void svRAM::SetLength(const unsigned int length)
{

// If the RAM bit length is large, display a warning
#ifdef ENABLE_LOG_SV_RAM
  if (length > 4000)
  {
    m_log->warning("Large RAM length. Length =", length);
  }
#endif

  // Copy length into private data member
  m_length = length;

  // Re-calculate the RAM address bit width
  UpdateAddrWidth();
}



void svRAM::SetWidth(const unsigned int width)
{
// If the RAM bit width is large
#ifdef ENABLE_LOG_SV_RAM
  if (width > 32)
  {
    m_log->warning("Large RAM bitwidth. Width greater than 32 bits.");
  }
#endif

  // Copy width into private data member
  m_bitwidth = width;
}

//

string svRAM::GetName() const
{
  return (m_name);
}



unsigned int svRAM::GetLength() const
{
  return (m_length);
}



unsigned int svRAM::GetWidth() const
{
  return (m_bitwidth);
}


void svRAM::UpdateAddrWidth()
{
  m_addr_width = ceil(log2(m_length));

#ifdef ENABLE_LOG_SV_RAM
  m_log->info("RAM address bit width updated and is now", m_addr_width);
#endif
}


bool svRAM::Export(const char *out_dir) const
{
  time_t rawtime;
  struct tm *timeinfo;
  bool success = false;

  string filepath = out_dir;
  filepath += "/";
  filepath += m_name + ".sv";

#ifdef ENABLE_LOG_SV_RAM
  m_log->info("Exporting RAM module");
#endif

  // Attempt to open the target output file
  ofstream f(filepath, ios::out | ios::trunc);

  if (f.is_open())
  {

#ifdef ENABLE_LOG_SV_RAM
    m_log->info("Successfully opened file to write RAM module");
#endif

    f << "////////////////////////////////////////////////////////////////////////////////// \n";
    f << "// \n";
    f << "// RAM Generator written by: Alexander Hogen \n";
    f << "//                           (https://bitbucket.org/ahogen/) \n";
    f << "// \n";
    f << "// Filename:  " << m_name << ".sv \n";

    // Get the local time to timestamp the output file
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    f << "// Generated: " << asctime(timeinfo);
    f << "// \n";
    f << "// Description: \n";
    f << "//     This RAM module is " << m_bitwidth << "-bits wide and " << m_length << "-bits long.\n";
    f << "//     It accepts a " << m_addr_width << "-bit address and returns a " << m_bitwidth << "-bit value \n";
    f << "// \n";
    f << "////////////////////////////////////////////////////////////////////////////////// \n";

    f << "\n\n";

    f << "module " << m_name << " (\n";
    f << "\tinput [" << m_addr_width - 1 << ":0] addr, \n";
    f << "\tinput cs, we, oe, \n";
    f << "\tinout [" << m_bitwidth - 1 << ":0] data \n";
    f << "); \n\n";

    f << "// Declare the memory block as an unpacked array \n";
    f << "logic [" << m_bitwidth - 1 << ":0] ram [0:" << m_length << "] = ";
    f << "'{default:" << m_bitwidth << "'h0}; \n\n";

    //*****************************************
    // READ functionality + bi-dir port control
    f << "// Bi-directional data port direction control \n";
    f << "// including READ functionality \n";
    f << "assign data = (~cs && ~oe) ? ram(addr) : {" << m_bitwidth << "{1'bz}}; \n\n";

    //*****************************************
    // Write functinality
    f << "// WRITE functionality \n";
    f << "always @ (cs or we) \n";
    f << "begin\n";
    f << "\tif (!cs && !we) begin \n";
    f << "\t\tram[addr] = data;\n";
    f << "\tend \n";
    f << "end \n\n";

    //*****************************************
    // Error case
    f << "// Display error in simulation if control signal conflict \n";
    f << "always @ (we or oe) \n";
    f << "begin\n";
    f << "\tif (!we && !oe) begin \n";
    f << "\t\t$display(\"Operational error in RAM module " << m_name << ": OE and WE are both active!!\"); \n";
    f << "\tend \n";
    f << "end \n\n";

    f << "endmodule\n";

#ifdef ENABLE_LOG_SV_RAM
    string tmp_log_str = "Wrote ";
    tmp_log_str += to_string(f.tellp());
    tmp_log_str += " bytes to file \"" + m_name;
    tmp_log_str += ".sv\"";
    m_log->info(tmp_log_str.c_str());
#endif

    f.close();

#ifdef ENABLE_LOG_SV_RAM
    m_log->info("RAM module export successful.");
#endif // ENABLE_LOG_SV_RAM

    success = true;
  }
#ifdef ENABLE_LOG_SV_RAM
  else
  {
    string tmp_log_str = "Could not open target file to Export \"";
    tmp_log_str += filepath;
    m_log->error(tmp_log_str.c_str());
  }
#endif

  return (success);
}
